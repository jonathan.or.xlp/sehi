<?php
include('autentificacion.php');
require_once('config.php');

if (isset($_GET["id"])) {
    $sucursalID = $_GET["id"];
    $sql = mysqli_query($link, "SELECT S.*, Z.zona, R.region,TS.tipoSucursal FROM sucursales AS S INNER JOIN zonas AS Z ON S.zonaID = Z.zonaID INNER JOIN regiones AS R ON Z.regionID = R.regionID INNER JOIN tipossucursal AS TS ON S.tipoSucursalID = TS.tipoSucursalID WHERE sucursalID = $sucursalID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: sucursales.php"); 
    } else {
        $row = mysqli_fetch_assoc($sql);

        $sucursalID = $row['sucursalID'];
        $sucursal = $row["sucursal"];
        $clave = $row["clave"];
        $region =$row["region"];
        $zona = $row["zona"];
        $tipoSucursal = $row["tipoSucursal"];
        $direccion = $row["direccion"];
        $colonia = $row["colonia"];
        $cp = $row["cp"];
        $municipio = $row["municipio"];
        $estado = $row["estado"];
    }
} else {
    header("location: sucursales.php");
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page linktent  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> Sucursal: <?php echo $clave; ?></h3>
                <div class="line"></div>
                <br/>
                <div class="">
                    <div class="ml-4">
                        <div class="form-group row">
                            <label for="clave" class="col-sm-1 col-form-label">Clave</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="clave" value="<?php echo $clave; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="sucursal" class="col-sm-1 col-form-label">Sucursal</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="sucursal" value="<?php echo $sucursal; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="region" class="col-sm-1 col-form-label">Región</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="region" value="<?php echo $region; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="zona" class="col-sm-1 col-form-label">Zona</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="zona" value="<?php echo $zona; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tipoSucursal" class="col-sm-1 col-form-label">Tipo de Sucursal</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="tipoSucursal" value="<?php echo $tipoSucursal; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="direccion" class="col-sm-1 col-form-label">Dirección</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="direccion" value="<?php echo $direccion; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colonia" class="col-sm-1 col-form-label">Colonia</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="colonia" value="<?php echo $colonia; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cp" class="col-sm-1 col-form-label">C.P.</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="cp" value="<?php echo $cp; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="municipio" class="col-sm-1 col-form-label">Ciudad</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="municipio" value="<?php echo $municipio; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="estado" class="col-sm-1 col-form-label">Estado</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="estado" value="<?php echo $estado; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <input type="hidden" name="sucursalID" value=<?php echo $sucursalID; ?>>
                            <label class="col-sm-1">&nbsp;</label>
                            <div class="col-sm-4 text-right">
                                <a href="sucursales.php" class="btn btn-sm btn-light">Regresar</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="line"></div>
            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
</body>

</html>