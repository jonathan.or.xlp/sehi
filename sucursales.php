<?php include('autentificacion.php'); ?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page Content  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> Sucursales</h3>
                <?php

                require_once('config.php'); //conexión a la base de datos con variable $link

                if (isset($_GET['aksi']) == 'delete') {
                    // escaping, additionally removing everything that could be (html/javascript-) code
                    $id = mysqli_real_escape_string($link, (strip_tags($_GET["id"], ENT_QUOTES)));
                    $data = mysqli_query($link, "SELECT * FROM sucursales WHERE sucursalID='$id'");
                    if (mysqli_num_rows($data) == 0) {
                        echo '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> No se encontraron datos.</div>';
                    } else {
                        $delete = mysqli_query($link, "DELETE FROM sucursales WHERE sucursalID='$id'");
                        if ($delete) {
                            echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Datos eliminado correctamente.</div>';
                        } else {
                            echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error, no se pudo eliminar los datos.<br>'.mysqli_error($link).'</div>';
                        }
                    }
                }
                ?>
                <div class="row">
                <form class="form-inline col-10" method="get">
                        <div class="form-group">
                            <select name="filter" class="form-control" onchange="form.submit()">
                                <option value=0>Región</option>
                                <?php
                                $filter = (isset($_GET['filter']) ? strtolower($_GET['filter']) : NULL);
                                $data = mysqli_query($link, "SELECT * FROM regiones");
                                while ($row = mysqli_fetch_assoc($data)) { ?>
                                    <option value=<?php echo $row['regionID'];
                                                    if ($filter == $row['regionID']) echo ' selected'; ?>><?php echo $row['region']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="filter2" class="form-control" onchange="form.submit()">
                                <option value=0>Zona</option>
                                <?php
                                $filter2 = (isset($_GET['filter2'])? strtolower($_GET['filter2']) : NULL);
                                $regionFilter = (isset($_GET['filter'])  && $_GET['filter']>0 ? ' WHERE regionID='.$_GET['filter'] : '');
                                $data = mysqli_query($link, 'SELECT * FROM zonas'.$regionFilter);
                                while ($row = mysqli_fetch_assoc($data)) { ?>
                                    <option value=<?php echo $row['zonaID'];
                                                    if ($filter2 == $row['zonaID']) echo ' selected'; ?>><?php echo $row['zona']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="filter3" class="form-control" onchange="form.submit()">
                                <option value=0>Tipo de Sucursal</option>
                                <?php
                                $filter3 = (isset($_GET['filter3']) ? strtolower($_GET['filter3']) : NULL);
                                $data = mysqli_query($link, "SELECT * FROM tipossucursal");
                                while ($row = mysqli_fetch_assoc($data)) { ?>
                                    <option value=<?php echo $row['tipoSucursalID'];
                                                    if ($filter3 == $row['tipoSucursalID']) echo ' selected'; ?>><?php echo $row['tipoSucursal']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </form>
                    <div class="col text-right">
                        <a class="btn btn-secondary" href="sucursalesEdit.php">Nueva</a>
                    </div>
                </div>
                <div class="line"></div>

                <div class="">
                    <table class="table table-striped table-hover table-sm">
                        <thead>
                            <tr class="bg-primary text-light">
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">Clave</th>
                                <th>Sucursal</th>
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">Zona</th>
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">Región</th>
                                <th>Tipo de Sucursal</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            
                            if ($filter || $filter2 || $filter3) {
                                $clauses=array();
                                if($filter) $clauses[] = "R.regionID = $filter";
                                if($filter2) $clauses[] = "Z.zonaID = $filter2";
                                if($filter3) $clauses[] = "TS.tipoSucursalID = $filter3";
                                $where = !empty( $clauses ) ? ' WHERE '.implode(' AND ',$clauses ) : '';
                                $sql = mysqli_query($link, "SELECT S.sucursalID,S.clave,S.sucursal,Z.zona,R.region,TS.tipoSucursal FROM sucursales AS S JOIN zonas AS Z ON Z.zonaID= S.zonaID JOIN regiones AS R ON Z.regionID = R.regionID JOIN tipossucursal AS TS ON TS.tipoSucursalID = S.tipoSucursalID $where ORDER BY ABS(S.clave)");
                            } else {
                                $sql = mysqli_query($link, "SELECT S.sucursalID,S.clave,S.sucursal,Z.zona,R.region,TS.tipoSucursal FROM sucursales AS S JOIN zonas AS Z ON Z.zonaID= S.zonaID JOIN regiones AS R ON Z.regionID = R.regionID JOIN tipossucursal AS TS ON TS.tipoSucursalID = S.tipoSucursalID ORDER BY ABS(S.clave)");
                            }
                            if (mysqli_num_rows($sql) == 0) {
                                echo '<tr><td colspan="8">No hay datos.</td></tr>';
                            } else {
                                while ($row = mysqli_fetch_assoc($sql)) {
                                    echo '
                                            <tr>
                                                <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">'.$row['clave'].'</td>
                                                <td><a href="sucursalView.php?id=' . $row['sucursalID'] . '">' . $row['sucursal'] . '</a></td>
                                                <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">'.$row['zona'].'</td>
                                                <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">' . $row['region'] . '</td>
                                                <td>' . $row['tipoSucursal'] . '</td>
                                                <td>
                                                    <a href="sucursalesEdit.php?sucursalID=' . $row['sucursalID'] . '" title="Editar datos" class="btn btn-outline-success btn-sm"><span class="fa fa-edit" aria-hidden="true"></span></a>
                                                    <a href="" title="Eliminar" class="btn btn-outline-danger btn-sm deleteButton" data-toggle="modal" data-target="#exampleModal" data-sucursal="' . $row['sucursalID'] . '"><span class="fa fa-trash" aria-hidden="true"></span></a>
                                                </td>
                                            </tr>
                                            ';
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Confirmar</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                ¿Está seguro que desea eliminar la sucursal?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                                <a class='btn btn-outline-danger' href=''> Eliminar</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
    <script type="text/javascript">
        $(document).on("click", ".deleteButton", function(e) {

            //get data-id attribute of the clicked element
            var sucursalID = $(this).data('sucursal')
            //populate the textbox
            $(".modal-footer a").attr('href', 'sucursales.php?aksi=delete&id=' + sucursalID);
        });
    </script>
</body>

</html>