<?php
// Initialize the session
session_start();
require_once('login_utils.php');
$url = $_SERVER['REQUEST_URI'];

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    header("location: login.php?url=$url");
    exit;
}

if (auto_logout()) {
    session_unset();
    session_destroy();
    header("location: login.php?url=$url");
    exit;
}
$page = basename($_SERVER['PHP_SELF']);
if(!isset($_SESSION["rolID"]) || !hasPermission($page,$_SESSION["rolID"])){
    header("location: index.php");
    exit;
}
?>