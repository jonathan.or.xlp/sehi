<div class="sidebar">
    <nav id="sidebar" class="sticky-top">
        <a class="navbar-brand sidebar-header text-center" href="index.php">
            <img src="img/logo-sm.png">
        </a>
        
        <ul class="list-unstyled components">
            <?php if (isset($_SESSION["rolID"]) && $_SESSION["rolID"] === 2) { ?>
                <li>
                    <a href="home_tec.php">
                        <i class="fas fa-home"></i>
                        Asignaciones
                    </a>
                </li>
            <?php } ?>
            <?php if (isset($_SESSION["rolID"]) && $_SESSION["rolID"] === 1) { ?>
                <li>
                    <a href="home_admin.php">
                        <i class="fas fa-home"></i>
                        Administrador
                    </a>
                </li>
                <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="fa fa-list-alt"></i>
                        Catálogos
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="usuarios.php">
                                <i class="fa fa-user"></i>
                                Usuarios
                            </a>
                        </li>
                        <li>
                            <a href="regiones.php">
                                <i class="fa fa-map"></i>
                                Regiones
                            </a>
                        </li>
                        <li>
                            <a href="zonas.php">
                                <i class="fa fa-map-marker"></i>
                                Zonas
                            </a>
                        </li>
                        <li>
                            <a href="razonesSociales.php">
                                <i class="fas fa-user-tie"></i>
                                Razones Sociales
                            </a>
                        </li>
                        <li>
                            <a href="tiposSucursal.php">
                                <i class="fa fa-cubes"></i>
                                Tipos Sucursal
                            </a>
                        </li>
                        <li>
                            <a href="sucursales.php">
                                <i class="fa fa-map-marker-alt"></i>
                                Sucursales
                            </a>
                        </li>
                        <li>
                            <a href="certificados.php">
                                <i class="fas fa-stamp"></i>
                                Equipos
                            </a>
                        </li>
                        <li>
                            <a href="normas.php">
                                <i class="fas fa-gavel"></i>
                                Normas
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="ejercicios.php">
                        <i class="fas fa-briefcase"></i>
                        Ejercicios
                    </a>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                        <i class="fas fa-copy"></i>
                        Operaciones
                    </a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li>
                            <a href="#">Asignar Certificado</a>
                        </li>
                        <li>
                            <a href="#">Asignar Equipo</a>
                        </li>
                        <li>
                            <a href="asignarTecnico.php">Asignar Técnico</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="nom002.php">
                        <i class="fas fa-image"></i>
                        Estudios
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-question"></i>
                        Reportes
                    </a>
                </li>
            <?php } ?>
            <li>
                <a href="#">
                    <i class="fas fa-paper-plane"></i>
                    Contacto
                </a>
            </li>
        </ul>
    </nav>
</div>