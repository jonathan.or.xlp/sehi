<?php include('autentificacion.php'); ?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page Content  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> Ejercicios</h3>
                <?php

                require_once('config.php'); //conexión a la base de datos con variable $link

                if (isset($_GET['aksi']) == 'delete') {
                    // escaping, additionally removing everything that could be (html/javascript-) code
                    $id = mysqli_real_escape_string($link, (strip_tags($_GET["id"], ENT_QUOTES)));
                    $data = mysqli_query($link, "SELECT * FROM ejercicios WHERE ejercicioID='$id'");
                    if (mysqli_num_rows($data) == 0) {
                        echo '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> No se encontraron datos.</div>';
                    } else {
                        $delete = mysqli_query($link, "DELETE FROM ejercicios WHERE ejercicioID='$id'");
                        if ($delete) {
                            echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Datos eliminado correctamente.</div>';
                        } else {
                            echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error, no se pudo eliminar los datos.<br>' . mysqli_error($link) . '</div>';
                        }
                    }
                }
                ?>
                <div class="row">
                    <div class="col text-right">
                        <a class="btn btn-secondary" href="ejercicioAperturar.php">Aperturar</a>
                    </div>
                </div>
                <div class="line"></div>

                <div class="">
                    <table class="table table-striped table-hover table-sm">
                        <thead>
                            <tr class="bg-primary text-light">
                                <th>Ejercicio</th>
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">Estatus</th>
                                <th>Fecha Inicio</th>
                                <th>Fecha Término</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            $sql = mysqli_query($link, "SELECT * FROM ejercicios");

                            if (mysqli_num_rows($sql) == 0) {
                                echo '<tr><td colspan="8">No hay datos.</td></tr>';
                            } else {
                                while ($row = mysqli_fetch_assoc($sql)) {
                                    echo '
                                            <tr>
                                                <td>' . $row['ejercicioID'] . '</td>
                                                <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">' . $row['estatus'] . '</td>
                                                <td>' . $row['fechaInicio'] . '</td>
                                                <td>' . $row['fechaTermino'] . '</td>
                                                <td>
                                                    <a href="ejercicioCerrar.php?ejercicioID=' . $row['ejercicioID'] . '" title="Cerrar ejercicio" class="btn btn-outline-success btn-sm"><span class="fa fa-lock" aria-hidden="true"></span></a>
                                                    <a href="" title="Eliminar" class="btn btn-outline-danger btn-sm deleteButton" data-toggle="modal" data-target="#exampleModal" data-ejercicio="' . $row['ejercicioID'] . '"><span class="fa fa-trash" aria-hidden="true"></span></a>
                                                </td>
                                            </tr>
                                            ';
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Confirmar</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                ¿Está seguro que desea eliminar el ejercicio?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                                <a class='btn btn-outline-danger' href=''> Eliminar</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
    <script type="text/javascript">
        $(document).on("click", ".deleteButton", function(e) {

            //get data-id attribute of the clicked element
            var ejercicioID = $(this).data('ejercicio')
            //populate the textbox
            $(".modal-footer a").attr('href', 'ejercicios.php?aksi=delete&id=' + ejercicioID);
        });
    </script>
</body>

</html>