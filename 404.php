<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Sistema Protección Seguridad e Higiene | 404</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
  <!-- Our Custom CSS -->
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/my-login.css">
  <!-- Font Awesome JS -->
  <script defer src="vendor/fontawesome/js/solid.js"></script>
  <script defer src="vendor/fontawesome/js/fontawesome.min.js"></script>
  <!-- Favicon-->
  <link rel="apple-touch-icon" sizes="57x57" href="icons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="icons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="icons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="icons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="icons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="icons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="icons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="icons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="icons/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="icons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="icons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="icons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="icons/favicon-16x16.png">
  <link rel="manifest" href="icons/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="icons/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

</head>

<body class="my-login-page">
  <section class="h-100">
    <div class="card-wrapper h-100">
      <div class="brand">
        <img src="img/logo.png">
      </div>
      <div class="card fat">
        <div class="card-body">
          <h1 class="card-title">Error 404, página no encontrada</h1>
            <p>La página solicitada <span class="text-danger">"<?php echo $_SERVER['REQUEST_URI'];?>"</span>, no fue encontrada, ver <a class="text-primary" href="index.php">Inicio</a></p>
        </div>
      </div>
      <div class="footer">
        Copyright &copy; TIENDAS CHEDRAUI S.A. DE C.V. 2019
      </div>
    </div>
  </section>
  <script src="vendor/bootstrap/jquery-3.3.1.slim.min.js"></script>
  <script src="vendor/bootstrap/popper.min.js"></script>
  <script src="vendor/bootstrap/bootstrap.min.js"></script>
</body>

</html>