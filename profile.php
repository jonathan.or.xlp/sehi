<?php
include('autentificacion.php');
require_once('config.php');

if (isset($_GET["id"])) {
    $usuarioID = $_GET["id"];
    $sql = mysqli_query($link, "SELECT U.*, R.rol FROM usuarios AS U INNER JOIN roles AS R ON U.rolID = R.rolID WHERE usuarioID = $usuarioID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: usuarios.php");
    } else {
        $row = mysqli_fetch_assoc($sql);

        $usuarioID = $row['usuarioID'];
        $usuario = $row['usuario'];
        $nombreCompleto = $row["nombreCompleto"];
        $email = $row["email"];
        $telefono =$row["telefono"];
        $rol = $row["rol"];
    }
} else {
    header("location: usuarios.php");
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page linktent  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> Usuario: <?php echo $usuario; ?></h3>
                <div class="line"></div>
                <br/>
                <div class="">
                    <div class="ml-4">
                        <div class="form-group row">
                            <label for="usuario" class="col-sm-1 col-form-label">Usuario</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="usuario" value="<?php echo $usuario; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nombreCompleto" class="col-sm-1 col-form-label">Nombre Completo</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="nombreCompleto" value="<?php echo $nombreCompleto; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-1 col-form-label">E-mail</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="email" value="<?php echo $email; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="telefono" class="col-sm-1 col-form-label">Telefono</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="telefono" value="<?php echo $telefono; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="rol" class="col-sm-1 col-form-label">Rol</label>
                            <div class="col-sm-4">
                                <input type="text" readonly name="rol" value="<?php echo $rol; ?>" class="form-control">
                            </div>
                        </div>
                       
                        <div class="form-group row">
                            <input type="hidden" name="usuarioID" value=<?php echo $usuarioID; ?>>
                            <label class="col-sm-1">&nbsp;</label>
                            <div class="col-sm-4 text-right">
                                <a href="usuarios.php" class="btn btn-sm btn-light">Regresar</a>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="line"></div>
            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
</body>

</html>