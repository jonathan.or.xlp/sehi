<?php
include('autentificacion.php');
setlocale(LC_ALL, "es_MX");
date_default_timezone_set("America/Mexico_City");

if (isset($_GET["sucursalID"])) {
    require_once('config.php');
    require_once('functionsDB.php');
    include_once('nom-helpers.php');

    $ejercicio = getEjercicioAbierto($link);
    $userID = $_SESSION["id"];

    $table = "nom030"; // nombre de la tabla principal de la norma
    $tableID = $table."ID"; // nombre de la columna ID de la norma, debrá seguir el estandar <nombreTabla><ID>
    $sucursalID = $_GET["sucursalID"];

    $sql = mysqli_query($link, "SELECT S.sucursal,Z.zona,R.region, A.* FROM asignaciones AS A INNER JOIN sucursales AS S ON A.sucursalID = S.sucursalID INNER JOIN zonas AS Z ON S.zonaID = Z.zonaID INNER JOIN regiones AS R ON Z.regionID = R.regionID WHERE ejercicioID = $ejercicio AND usuarioID = $userID AND A.sucursalID = $sucursalID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: index.php");
    } else {
        $row = mysqli_fetch_assoc($sql);

        $sucursal = $row["sucursal"];
        $zona = $row["zona"];
        $region = $row["region"];
        $fecha = $row["fecha"];
        $estatus = $row["estatus"];

        $exist = mysqli_query($link, "SELECT $tableID, sucursalID FROM $table WHERE ejercicioID = '$ejercicio' AND sucursalID = $sucursalID");

        if (mysqli_num_rows($exist) == 0) {
            // FALTA OBTENER DATOS DEL AÑO PASADO EN TABLA PRINCIPAL Y SECUNDARIAS
            // SI ES EL PRIMER AÑO SE INSERTA UN REGISTRO SOLO CON LOS CAMPOS PRINCIPALES
            $result = mysqli_query($link, "INSERT INTO $table (ejercicioID, sucursalID) VALUES ('$ejercicio',$sucursalID)");
            $nomID = mysqli_insert_id($link);
        }else{
            $row = mysqli_fetch_assoc($exist);
            $nomID = $row[$tableID];
        }
        $where = "WHERE $tableID = $nomID";
    }
}else{
    header("location: index.php");
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page Content  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-gavel" aria-hidden="true"></span> NOM-030-STPS-2009</h3>
                <h4>Servicios preventivos de seguridad y salud en el trabajo - Funciones y actividades</h4>
                <?php require('nom-cards.php'); ?>
                <div class="line"></div>
                <div class="accordion" id="accordionExample">
                    <?php
                    // Procesamos todas las secciones
                    $d1 = proccessFormSection("1");

                    // Obtenemos banderas de captura de secciones
                    $c1 = sectionCompleted("1");

                    // obtenemos datos almacenados en BD
                    $data = getDataFromDB();
                    //print("<pre>".print_r($data,true)."</pre>");
                    
                    initSection("accordionExample", "1", "Actividades de Salud y Seguridad en el trabajo", $d1, $c1);
                        questionRadio("q1", "Cuentan con un diagnóstico integral o por área de trabajo de las condiciones de seguridad y salud ", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q2", "Cuenta con un programa de seguridad y salud en el trabajo actualizado al menos una vez al año ", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q3", "Cuenta con una relación de acciones preventivas y correctivas de seguridad y salud en el trabajo actualizada al menos una vez al año ", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q4", "Se tiene designado por parte del patrón un responsable de seguridad y salud en el trabajo interno o externo ", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q5", "Se cuenta con un documento que acredite que se comunica a la comisión de seguridad e higiene y/o a los trabajadores el diagnóstico integral o por área de trabajo ", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q6", "Cuenta con constancias de habilidades laborales del personal de la empresa que forma parte de los servicios preventivos de seguridad y salud en el trabajo ", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q7", "Cuentan con registros de los reportes de seguimiento de los avances en la instauración del programa de seguridad y salud en el trabajo o de la relación de acciones  ", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q8", "Cuentan con mecanismos de respuesta inmediata cuando se detecte un riesgo grave o inminente ", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q9", "Cuenta con procedimientos, instructivos, guías o registros necesarios para cumplir con el programa de seguridad y salud ", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q10", "Tienen establecido un programa para todo el personal ( planta y terceros) sobre temas de prevención de enfermedades generales y de trabajo que causen daño físico o influyan en el ausentismo ", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q11", "Informan a todo el personal del programa por medio de folletos, periódicos murales, trípticos, carteles, películas, etc. ", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q12", "Capacitan al personal de nuevo ingreso, mediante información sobre seguridad y salud en el trabajo. ", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);    
                    endSection($sucursalID, "1");
                    ?>
                </div>
            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
    <?php require('nom-helpers-script.php') ?>
</body>

</html>