<?php
include('autentificacion.php');
require_once('config.php');
require_once('certificadoUpload.php');

if (isset($_POST['saveNew'])) {
    $action = 'new';
    $nombre = $_POST["nombre"];
    $descripcion = $_POST["descripcion"];
    $fechaInicial = $_POST["fechainicial"];
    $fechaFinal = $_POST["fechafinal"];
    $fileEquipo = basename($_FILES["fileEquipo"]["name"]);
    $uploadEquipo = uploadFile($_FILES, $_POST,"fileEquipo");
    $_MYFILES = array();
    foreach (array_keys($_FILES["fileToUpload"]['name']) as $i) { // loop over 0,1,2,3 etc...
        foreach (array_keys($_FILES["fileToUpload"]) as $j) { // loop over 'name', 'size', 'error', etc...
            $_MYFILES[$i][$j] = $_FILES["fileToUpload"][$j][$i]; // "swap" keys and copy over original array values
        }
    }
    if (array_sum($_FILES["fileToUpload"]["error"]) == UPLOAD_ERR_OK) {
        $upload = uploadFiles("new", $_MYFILES);
        if (isset($upload) && isset($upload["0"]) && $upload["0"] == 1) {
            $file = serialize($_FILES["fileToUpload"]["name"]);
        }
    }
    $update = mysqli_query($link, "INSERT INTO certificados (nombre, descripcion, fechainicial, fechafinal,file, fileEquipo) VALUES(''$nombre,'$descripcion','$fechaInicial','$fechaFinal','$file','$fileEquipo')");
    $certificadoID = mysqli_insert_id($link);
    rename("certificados/new/", "certificados/{$certificadoID}/");
} elseif (isset($_POST['save'])) {
    $action = 'update';
    $certificadoID = $_POST['certificadoID'];
    $nombre = $_POST["nombre"];
    $descripcion = $_POST["descripcion"];
    $fechaInicial = $_POST["fechainicial"];
    $fechaFinal = $_POST["fechafinal"];
    $fileEquipo = basename($_FILES["fileEquipo"]["name"]);
    if (isset($_FILES["fileEquipo"]) && $_FILES["fileEquipo"]["error"] == UPLOAD_ERR_OK) {
        $uploadEquipo = uploadFile($_FILES, $_POST,"fileEquipo");
        $setfileEquipo = ", fileEquipo='$fileEquipo'";
        //Borrar anterior archivo
        $id = mysqli_real_escape_string($link, (strip_tags($_POST['certificadoID'], ENT_QUOTES)));
        $data = mysqli_query($link, "SELECT * FROM certificados WHERE certificadoID='$id'");
        if ($row = mysqli_fetch_assoc($data)) {
            unlink("equipos/" . $row["fileEquipo"]);
        }
    } else {
        $setfileEquipo = "";
    }
    
    $_MYFILES = array();
    foreach (array_keys($_FILES["fileToUpload"]['name']) as $i) { // loop over 0,1,2,3 etc...
        foreach (array_keys($_FILES["fileToUpload"]) as $j) { // loop over 'name', 'size', 'error', etc...
            $_MYFILES[$i][$j] = $_FILES["fileToUpload"][$j][$i]; // "swap" keys and copy over original array values
        }
    }
    if (array_sum($_FILES["fileToUpload"]["error"]) == UPLOAD_ERR_OK) {
        $upload = uploadFiles($certificadoID, $_MYFILES);
        if (isset($upload) && isset($upload["0"]) && $upload["0"] == 1) {
            $file = serialize($_FILES["fileToUpload"]["name"]);
            $setfile = ", file='$file'";
            //Borrar anterior archivo
            // $id = mysqli_real_escape_string($link, (strip_tags($_POST['certificadoID'], ENT_QUOTES)));
            // $data = mysqli_query($link, "SELECT * FROM certificados WHERE certificadoID='$id'");
            // if ($row = mysqli_fetch_assoc($data)) {
            //     $valueFileNames = isset($row) && isset($row["file"]) ? unserialize($row["file"]) : [];
            //     foreach ($valueFileNames as $fileName) {
            //         unlink("certificados/" . $fileName);
            //     }
            // }
        } else {
            $setfile = "";
        }
    } else {
        $setfile = "";
    }
    $update = mysqli_query($link, "UPDATE certificados SET nombre='$nombre', descripcion='$descripcion', fechainicial='$fechaInicial', fechafinal='$fechaFinal' $setfile $setfileEquipo WHERE certificadoID=$certificadoID");
}
if (isset($_GET["certificadoID"])) {
    $action = 'edit';
    $certificadoID = $_GET["certificadoID"];
    $sql = mysqli_query($link, "SELECT * FROM certificados WHERE certificadoID=$certificadoID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: certificados.php");
    } else {
        $row = mysqli_fetch_assoc($sql);

        $certificadoID = $row['certificadoID'];
        $nombre = $row["nombre"];
        $descripcion = $row["descripcion"];
        $fechaInicial = $row["fechainicial"];
        $fechaFinal = $row["fechafinal"];
        $file = $row["file"];
        $fileEquipo = $row["fileEquipo"];
    }
} else {
    //header("location: usuarios.php");
    $certificadoID = 0;
    $action = 'new';
    $nombre = '';
    $descripcion = '';
    $fechaInicial = '';
    $fechaFinal = '';
    $file = '';
    $fileEquipo = "";
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page linktent  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> <?php echo $action == 'new' ? 'Agregar Certificado' : 'Editar Certificado'; ?></h3>
                <div class="line"></div>
                <br />
                <div class="">
                    <?php
                    if (isset($update) && $update) {
                        echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
                    } elseif (isset($update) && !$update) {
                        echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.<br>' . mysqli_error($link) . '</div>';
                    }
                    if (isset($uploadEquipo) && isset($uploadEquipo[0]) && $uploadEquipo[0] == 0) {
                        echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo cargar la imagen del Equipo.<br>' . $uploadEquipo[1] . '</div>';
                    }
                    if (isset($upload) && isset($upload[0]) && $upload[0] == 0) {
                        echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo cargar la imagen del Certificado.<br>' . $upload[1] . '</div>';
                    }
                    ?>
                    <form action="" method="post" class="ml-4" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label for="nombre" class="col-sm-1 col-form-label">Nombre</label>
                            <div class="col-sm-4">
                                <input type="text" name="nombre" class="form-control" placeholder="nombre" value="<?php echo $nombre; ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="descripcion" class="col-sm-1 col-form-label">Descripción</label>
                            <div class="col-sm-4">
                                <textarea name="descripcion" class="form-control" placeholder="descripción" rows="4" required><?php echo $descripcion; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fechainicial" class="col-sm-1 col-form-label">Fecha Inicio</label>
                            <div class="col-sm-4">
                                <input type="date" name="fechainicial" value="<?php echo $fechaInicial; ?>" class="form-control" placeholder="Fecha Inicio" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="fechafinal" class="col-sm-1 col-form-label">Fecha Término</label>
                            <div class="col-sm-4">
                                <input type="date" name="fechafinal" value="<?php echo $fechaFinal; ?>" class="form-control" placeholder="Fecha Término" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="file" class="col-sm-1 col-form-label">Equipo</label>
                            <div class="col-sm-4">
                                <?php if($fileEquipo !== "") echo '<a href="" data-toggle="modal" data-target="#imageModal" data-file="' . 'equipos/' . $fileEquipo . '" title="Ver" class="btn btn-outline-success btn-sm verButton"><img src="' . 'equipos/' . $fileEquipo . '" class="img-thumbnail rounded float-right" width="100px" alt=""></a>'; ?>
                                <input type="file" name="fileEquipo" id="fileEquipo" value="<?php echo $file; ?>" class=".form-control-file" accept="image/png, image/jpeg" <?php if ($action == 'new') echo 'required' ?>>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="file" class="col-sm-1 col-form-label">Certificado</label>
                            <div class="col-sm-4">
                                <?php $valueFileNames = isset($file) && $file !=="" ? unserialize($file) : [];
                                foreach ($valueFileNames as $fileName) {
                                    echo '<a href="" data-toggle="modal" data-target="#imageModal" data-file="' . 'certificados/' . $row['certificadoID'] . '/' . $fileName . '" title="Ver" class="btn btn-outline-success btn-sm verButton"><img src="' . 'certificados/' . $row['certificadoID'] . '/' . $fileName . '" class="img-thumbnail rounded float-right" width="60px" alt=""></a>';
                                }
                                ?>
                                <input type="file" name="fileToUpload[]" id="fileToUpload[]" multiple value="<?php echo $file; ?>" class=".form-control-file" accept="image/png, image/jpeg" <?php if ($action == 'new') echo 'required' ?>>
                            </div>
                        </div>
                        <div class="form-group row">
                            <input type="hidden" name="certificadoID" value=<?php echo $certificadoID; ?>>
                            <label class="col-sm-1">&nbsp;</label>
                            <div class="col-sm-4 text-right">
                                <a href="certificados.php" class="btn btn-sm btn-light">Regresar</a>
                                <input type="submit" name="<?php echo $action == 'edit' ? 'save' : 'saveNew'; ?>" class="btn btn-sm btn-secondary" value="Guardar">
                            </div>
                        </div>
                    </form>
                </div>

                <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Ver</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <img src='' class="img-fluid" />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
    <script type="text/javascript">
        $(document).on("click", ".verButton", function(e) {
            var src = $(this).data('file');
            $(".modal-body img").attr('src', decodeURIComponent(src));
        });
    </script>
</body>

</html>