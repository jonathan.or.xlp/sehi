<?php include('autentificacion.php');
require_once('functions.php');
 ?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page Content  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> Usuarios</h3>
                <?php

                require_once('config.php'); //conexión a la base de datos con variable $link

                if (isset($_GET['aksi']) == 'delete') {
                    // escaping, additionally removing everything that could be (html/javascript-) code
                    $id = mysqli_real_escape_string($link, (strip_tags($_GET["id"], ENT_QUOTES)));
                    $data = mysqli_query($link, "SELECT * FROM usuarios WHERE usuarioID='$id'");
                    if (mysqli_num_rows($data) == 0) {
                        echo '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> No se encontraron datos.</div>';
                    } else {
                        $delete = mysqli_query($link, "DELETE FROM usuarios WHERE usuarioID='$id'");
                        if ($delete) {
                            echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Datos eliminado correctamente.</div>';
                        } else {
                            echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error, no se pudo eliminar los datos.<br>'.mysqli_error($link).'</div>';
                        }
                    }
                }
                ?>
                <div class="row">
                    <form class="form-inline col" method="get">
                        <div class="form-group">
                            <select name="filter" class="form-control" onchange="form.submit()">
                                <option value=0>Rol</option>
                                <?php
                                $filter = (isset($_GET['filter']) ? strtolower($_GET['filter']) : NULL);
                                $data = mysqli_query($link, "SELECT * FROM roles");
                                while ($row = mysqli_fetch_assoc($data)) { ?>
                                    <option value=<?php echo $row['rolID'];
                                                    if ($filter == $row['rolID']) echo ' selected'; ?>><?php echo $row['rol']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </form>
                    <div class="col text-right">
                        <a class="btn btn-secondary" href="usuariosEdit.php">Nuevo</a>
                    </div>
                </div>
                <div class="line"></div>

                <div class="">
                    <table class="table table-striped table-hover table-sm">
                        <thead>
                            <tr class="bg-primary text-light">
                                <th>Usuario</th>
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">Nombre</th>
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">E-mail</th>
                                <th class="d-none d-lg-table-cell d-xl-table-cell">Teléfono</th>
                                <th>Rol</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($filter) {
                                $sql = mysqli_query($link, "SELECT * FROM usuarios WHERE rolID='$filter' ORDER BY usuario ASC");
                            } else {
                                $sql = mysqli_query($link, "SELECT * FROM usuarios ORDER BY usuario ASC");
                            }
                            if (mysqli_num_rows($sql) == 0) {
                                echo '<tr><td colspan="8">No hay datos.</td></tr>';
                            } else {
                                while ($row = mysqli_fetch_assoc($sql)) {
                                    echo '
                                            <tr>
                                                <td><a href="profile.php?id=' . $row['usuarioID'] . '">' . $row['usuario'] . '</a></td>
                                                <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">' . $row['nombreCompleto'] . '</td>
                                                <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">' . $row['email'] . '</td>
                                                <td class="d-none d-lg-table-cell d-xl-table-cell">' .phone_number_format($row['telefono']) . '</td>
                                                <td>';
                                    if ($row['rolID'] == 1) {
                                        echo '<span class="badge badge-primary">Administrador</span>';
                                    } else if ($row['rolID'] == 2) {
                                        echo '<span class="badge badge-secondary">Técnico</span>';
                                    } else if ($row['rolID'] == 3) {
                                        echo '<span class="badge badge-success">Tienda</span>';
                                    } else {
                                        echo '<span class="badge badge-danger">SIN ROL</span>';
                                    }
                                    echo '
                                                </td>
                                                <td>
                                                    <a href="usuariosEdit.php?usuarioID=' . $row['usuarioID'] . '" title="Editar datos" class="btn btn-outline-success btn-sm"><span class="fa fa-edit" aria-hidden="true"></span></a>
                                                    <a href="" title="Eliminar" class="btn btn-outline-danger btn-sm deleteButton" data-toggle="modal" data-target="#exampleModal" data-usuario="' . $row['usuarioID'] . '"><span class="fa fa-trash" aria-hidden="true"></span></a>
                                                </td>
                                            </tr>
                                            ';
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Confirmar</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                ¿Está seguro que desea eliminar al usuario?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                                <a class='btn btn-outline-danger' href=''> Eliminar</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
    <script type="text/javascript">
        $(document).on("click", ".deleteButton", function(e) {

            //get data-id attribute of the clicked element
            var usuarioID = $(this).data('usuario')
            //populate the textbox
            $(".modal-footer a").attr('href', 'usuarios.php?aksi=delete&id=' + usuarioID);
        });
    </script>
</body>

</html>