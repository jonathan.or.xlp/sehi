<?php
if (isset($_POST['generar'])) {
	setlocale(LC_ALL, "es_MX");
	// Include classes
	include_once('estudiosTemplates/tbs_class.php'); // Load the TinyButStrong template engine
	include_once('opentbs/tbs_plugin_opentbs.php'); // Load the OpenTBS plugin

	// prevent from a PHP configuration problem when using mktime() and date()
	if (version_compare(PHP_VERSION, '5.1.0') >= 0) {
		if (ini_get('date.timezone') == '') {
			date_default_timezone_set('UTC');
		}
	}

	// Initialize the TBS instance
	$TBS = new clsTinyButStrong; // new instance of TBS
	$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN); // load the OpenTBS plugin

	// Retrieve the user name to display
	// $yourname = (isset($_POST['yourname'])) ? $_POST['yourname'] : '';
	// $yourname = trim(''.$yourname);
	// if ($yourname=='') $yourname = "(no name)";


	$now = ucfirst(strftime("%B, %Y"));
	$claveSuc = $_POST['claveSuc'];
	$nombreSuc = $_POST['nombreSuc'];
	$direccionSuc = $_POST['direccionSuc'];
	$coloniaSuc = $_POST['coloniaSuc'];
	$ciudadSuc = $_POST['ciudadSuc'];
	$estadoSuc = $_POST['estadoSuc'];

	$razonSocial = $_POST['razonSocial'];

	$elaboroTecnico = $_POST['elaboroTecnico'];
	$elaboroFecha = $_POST['elaboroFecha'];

	$levantamientoFecha = $_POST['levantamientoFecha'];
	$levantamientoFecha2 = $_POST['levantamientoFecha2'];

	$colaboradores = $_POST['colaboradores'];
	$clientes = $_POST['clientes'];
	$promotores = $_POST['promotores'];
	$visitantes = $_POST['visitantes'];
	$m2 = $_POST['m2'];

	$numRociadores = $_POST['numRociadores'];
	$alarmaIncendio = $_POST['alarmaIncendio'];
	$hidrantes = $_POST['hidrantes'];
	$volumenAguaIncendio = $_POST['volumenAguaIncendio'];
	$volumenAguaIncendioLts = $_POST['volumenAguaIncendioLts'];

	$extintoresSeco = $_POST['extintoresSeco'];
	$extintoresCO2 = $_POST['extintoresCO2'];
	$extintoresK = $_POST['extintoresK'];

	$detectoresHumo = $_POST['detectoresHumo'];
	$brigadistas = $_POST['brigadistas'];

	// -----------------
	// Load the template
	// -----------------

	$template = 'estudiosTemplates/Nom-002.docx';
	$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).

	// -----------------
	// Output the result
	// -----------------

	// Define the name of the output file
	$save_as = (isset($_POST['save_as']) && (trim($_POST['save_as']) !== '') && ($_SERVER['SERVER_NAME'] == 'localhost')) ? trim($_POST['save_as']) : '';
	$output_file_name = str_replace('.', '_' . date('Y-m-d') . $save_as . '.', $template);
	if ($save_as === '') {
		// Output the result as a downloadable file (only streaming, no data saved in the server)
		$TBS->Show(OPENTBS_DOWNLOAD, $output_file_name); // Also merges all [onshow] automatic fields.
		// Be sure that no more output is done, otherwise the download file is corrupted with extra data.
		exit();
	} else {
		// Output the result as a file on the server.
		$TBS->Show(OPENTBS_FILE, $output_file_name); // Also merges all [onshow] automatic fields.
		// The script can continue.
		exit("File [$output_file_name] has been created.");
	}
} else {
	include('autentificacion.php'); ?>

	<!DOCTYPE html>
	<html>

	<head>
		<?php include('head.php'); ?>
	</head>

	<body>
		<div class="wrapper">
			<!-- Sidebar  -->
			<?php include('sidebar.php'); ?>
			<!-- Page Content  -->
			<div id="content">
				<?php include('navbar.php'); ?>
				<div class="content">
					<h3><span class="fa fa-user" aria-hidden="true"></span> Norm 002</h3>
					<div class="line"></div>
					<div class="">
						<form action="" method="post" class="ml-4">
							<div class="form-group row">
								<label for="claveSuc" class="col-3 col-form-label">Clave Sucursal</label>
								<div class="col-9">
									<input id="claveSuc" name="claveSuc" value="015" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="nombreSuc" class="col-3 col-form-label">Sucursal</label>
								<div class="col-9">
									<input id="nombreSuc" name="nombreSuc" value="PUEBLA CRYSTAL" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="direccionSuc" class="col-3 col-form-label">Dirección</label>
								<div class="col-9">
									<textarea id="direccionSuc" name="direccionSuc" cols="40" rows="3" class="form-control">BLVD. VALSEQUILLO NO. 115 ESQ. AV 51 PONIENTE</textarea>
								</div>
							</div>
							<div class="form-group row">
								<label for="coloniaSuc" class="col-3 col-form-label">Colonia</label>
								<div class="col-9">
									<input id="coloniaSuc" name="coloniaSuc" value="COL. BOULEVARES" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="ciudadSuc" class="col-3 col-form-label">Ciudad</label>
								<div class="col-9">
									<input id="ciudadSuc" name="ciudadSuc" value="PUEBLA" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="estadoSuc" class="col-3 col-form-label">Estado</label>
								<div class="col-9">
									<input id="estadoSuc" name="estadoSuc" value="PUE" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="razonSocial" class="col-3 col-form-label">Razón Social</label>
								<div class="col-9">
									<input id="razonSocial" name="razonSocial" value="TIENDAS CHEDRAUI S.A. DE C.V." type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="elaboroTecnico" class="col-3 col-form-label">Técnico</label>
								<div class="col-9">
									<input id="elaboroTecnico" name="elaboroTecnico" value="Norberto Sánchez Dionisio" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="elaboroFecha" class="col-3 col-form-label">Fecha de elaboración</label>
								<div class="col-9">
									<input id="elaboroFecha" name="elaboroFecha" value="07/05/2019" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="levantamientoFecha" class="col-3 col-form-label">Fecha levantamiento</label>
								<div class="col-9">
									<input id="levantamientoFecha" name="levantamientoFecha" value="30 de Abril de 2018" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="levantamientoFecha2" class="col-3 col-form-label">Levantamiento Fecha Corta</label>
								<div class="col-9">
									<input id="levantamientoFecha2" name="levantamientoFecha2" value="30/04/2018" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="colaboradores" class="col-3 col-form-label">Coaboradores</label>
								<div class="col-9">
									<input id="colaboradores" name="colaboradores" value="120" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="clientes" class="col-3 col-form-label">Clientes</label>
								<div class="col-9">
									<input id="clientes" name="clientes" value="2,513" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="promotores" class="col-3 col-form-label">Promotores</label>
								<div class="col-9">
									<input id="promotores" name="promotores" value="94" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="visitantes" class="col-3 col-form-label">Visitantes</label>
								<div class="col-9">
									<input id="visitantes" name="visitantes" value="169" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="m2" class="col-3 col-form-label">M2</label>
								<div class="col-9">
									<input id="m2" name="m2" value="11,341.00" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="numRociadores" class="col-3 col-form-label">Número de rociadores</label>
								<div class="col-9">
									<input id="numRociadores" name="numRociadores" value="0" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="alarmaIncendio" class="col-3 col-form-label">Alarmas de incendio</label>
								<div class="col-9">
									<input id="alarmaIncendio" name="alarmaIncendio" value="26" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="hidrantes" class="col-3 col-form-label">Hidrantes</label>
								<div class="col-9">
									<input id="hidrantes" name="hidrantes" value="26" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="volumenAguaIncendio" class="col-3 col-form-label">Volumen de Agua contra Incendio</label>
								<div class="col-9">
									<input id="volumenAguaIncendio" name="volumenAguaIncendio" value="145.00" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="volumenAguaIncendioLts" class="col-3 col-form-label">Volumen de agua contra incendio litros</label>
								<div class="col-9">
									<input id="volumenAguaIncendioLts" name="volumenAguaIncendioLts" value="145,000.00" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="extintoresSeco" class="col-3 col-form-label">Extintores seco</label>
								<div class="col-9">
									<input id="extintoresSeco" name="extintoresSeco" value="42" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="extintoresCO2" class="col-3 col-form-label">Extintores seco</label>
								<div class="col-9">
									<input id="extintoresCO2" name="extintoresCO2" value="5" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="extintoresK" class="col-3 col-form-label">Extintores seco</label>
								<div class="col-9">
									<input id="extintoresK" name="extintoresK" value="1" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="detectoresHumo" class="col-3 col-form-label">Extintores seco</label>
								<div class="col-9">
									<input id="detectoresHumo" name="detectoresHumo" value="33" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<label for="brigadistas" class="col-3 col-form-label">Extintores seco</label>
								<div class="col-9">
									<input id="brigadistas" name="brigadistas" value="58" type="text" class="form-control">
								</div>
							</div>
							<div class="form-group row">
								<div class="offset-3 col-9">
									<button name="generar" type="submit" class="btn btn-primary">Generar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>

	</html>
<?php
}
?>