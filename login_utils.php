<?php

$expire_time = 600;

function console_log($data)
{
    echo '<script>';
    echo 'console.log(' . json_encode($data) . ')';
    echo '</script>';
}

function auto_logout()
{
    global $expire_time;

    $t = time();
    $t0 = $_SESSION["USER_TIME"];
    $diff = $t - $t0;

    if ($diff > $expire_time || !isset($t0)) {
        return true;
    } else {
        $_SESSION["USER_TIME"] = time();
    }
    return false;
}

function auto_logout_check()
{
    global $expire_time;

    $t = time();
    $t0 = $_SESSION["USER_TIME"];

    $diff = $t - $t0;

    if ($diff > $expire_time || !isset($t0)) {
        return true;
    }
    return false;
}
function hasPermission($page, $rolID)
{
    if ($page == 'index.php') return true; //todos
    if ($page == 'myprofile.php') return true; //todos
    if ($page == 'certificadosAsignados.php') return true; //todos
    if ($rolID == 1) { //Administrador
        switch ($page) {
            case "home_admin.php":
            case 'usuarios.php':
            case 'usuariosEdit.php':
            case 'profile.php':
            case 'regiones.php':
            case 'regionesEdit.php':
            case 'zonas.php':
            case 'zonasEdit.php':
            case 'tiposSucursal.php':
            case 'tiposSucursalEdit.php':
            case 'sucursales.php':
            case 'sucursalesEdit.php':
            case 'sucursalView.php':
            case 'ejercicios.php':
            case 'ejercicioAperturar.php':
            case 'ejercicioCerrar.php':
            case "razonesSociales.php":
            case "razonesSocialesEdit.php":
            case "certificados.php":
            case "certificadosEdit.php":
            case "asignarTecnico.php":
            case "normas.php":
            case "normasEdit.php":
                return true;
        }
    }
    if ($rolID == 2) { //Tecnico
        switch ($page) {
            case 'home_tec.php':
            case 'visita.php':
            case 'nom-002.php':
            case 'nom-006.php':
            case 'nom-022.php':
            case 'nom-029.php':
            case 'nom-030.php':
            case 'nom-033.php':
            case 'nom-002-word.php':
                return true;
        }
    }
    return false;
}
