<?php include('autentificacion.php');
setlocale(LC_ALL, "es_MX");
date_default_timezone_set("America/Mexico_City");

require_once('config.php');
require_once('functionsDB.php');
$ejercicio = getEjercicioAbierto($link);
$userID = $_SESSION["id"];

if (isset($_POST['generales'])) {
    $sucursalID = $_POST["sucursalID"];
    $m2 = $_POST["m2"];
    $trabajadores = $_POST["trabajadores"];
    $clientes = $_POST["clientes"];
    $promotores = $_POST["promotores"];
    $visitantes = $_POST["visitantes"];

    $update = mysqli_query($link, "UPDATE  sucursalesgenerales SET m2 = $m2, trabajadores = $trabajadores, clientes = $clientes, promotores = $promotores, visitantes = $visitantes WHERE  ejercicioID = $ejercicio AND sucursalID = $sucursalID");
}
if (isset($_GET["sucursalID"])) {
    $sucursalID = $_GET["sucursalID"];
    $sql = mysqli_query($link, "SELECT S.sucursal,Z.zona,R.region, A.* FROM asignaciones AS A INNER JOIN sucursales AS S ON A.sucursalID = S.sucursalID INNER JOIN zonas AS Z ON S.zonaID = Z.zonaID INNER JOIN regiones AS R ON Z.regionID = R.regionID WHERE ejercicioID = $ejercicio AND usuarioID = $userID AND A.sucursalID = $sucursalID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: index.php");
    } else {
        $row = mysqli_fetch_assoc($sql);

        $sucursal = $row["sucursal"];
        $zona = $row["zona"];
        $region = $row["region"];
        $fecha = $row["fecha"];
        $estatus = $row["estatus"];

        $sql2 = mysqli_query($link, "SELECT * FROM sucursalesgenerales WHERE ejercicioID = $ejercicio AND sucursalID = $sucursalID");
        if (mysqli_num_rows($sql2) == 0) {
            // buscar datos del ejercicio pasado
            $sql2 = mysqli_query($link, "SELECT * FROM sucursalesgenerales WHERE ejercicioID = $ejercicio-1 AND sucursalID = $sucursalID");
            if (mysqli_num_rows($sql2) == 0) {
                $m2 = 0;
                $trabajadores = 0;
                $clientes = 0;
                $promotores = 0;
                $visitantes = 0;
                //INSERT en ceros
                $update = mysqli_query($link, "INSERT INTO sucursalesgenerales (ejercicioID, sucursalID, m2, trabajadores, clientes, promotores, visitantes) VALUES('$ejercicio',$sucursalID, $m2, $trabajadores, $clientes, $promotores, $visitantes)");
            } else {
                $row = mysqli_fetch_assoc($sql2);
                $m2 = $row["m2"];
                $trabajadores = $row["trabajadores"];
                $clientes = $row["clientes"];
                $promotores = $row["promotores"];
                $visitantes = $row["visitantes"];
            }
        } else {
            $row = mysqli_fetch_assoc($sql2);
            $m2 = $row["m2"];
            $trabajadores = $row["trabajadores"];
            $clientes = $row["clientes"];
            $promotores = $row["promotores"];
            $visitantes = $row["visitantes"];
        }
    }
}

?>


<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page Content  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <div class="row justify-content-between">
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-header">
                                <h5>Sucursal</h5>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title"><?php echo $sucursal ?></h5>
                                <h6 class="card-subtitle mb-2 text-muted"><?php echo $zona ?></h6>
                                <h6 class="card-subtitle mb-2 text-muted"><?php echo $region ?></h6>
                                <p class="card-text">Fecha programada: <?php echo $fecha ?></p>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-header">
                                <h5>Generales</h5>
                            </div>
                            <div class="card-body">
                                <form action="" method="post">
                                    <div class="form-group-sm row">
                                        <label for="m2" class="col-sm-6 col-form-label-sm">M2</label>
                                        <div class="col-sm-6">
                                            <input type="number" min="0" step=".01" name="m2" value="<?php echo $m2; ?>" class="form-control form-control-sm" placeholder="M2" required>
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label for="trabajadores" class="col-sm-6 col-form-label-sm">Trabajadores</label>
                                        <div class="col-sm-6">
                                            <input type="number" min="0" name="trabajadores" value="<?php echo $trabajadores; ?>" class="form-control form-control-sm" placeholder="Trabajadores" required>
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label for="clientes" class="col-sm-6 col-form-label-sm">Clientes</label>
                                        <div class="col-sm-6">
                                            <input type="number" min="0" name="clientes" value="<?php echo $clientes; ?>" class="form-control form-control-sm" placeholder="Clientes" required>
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label for="promotores" class="col-sm-6 col-form-label-sm">Promotores</label>
                                        <div class="col-sm-6">
                                            <input type="number" min="0" name="promotores" value="<?php echo $promotores; ?>" class="form-control form-control-sm" placeholder="Promotores" required>
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <label for="visitantes" class="col-sm-6 col-form-label-sm">Visitantes</label>
                                        <div class="col-sm-6">
                                            <input type="number" min="0" name="visitantes" value="<?php echo $visitantes; ?>" class="form-control form-control-sm" placeholder="Visitantes" required>
                                        </div>
                                    </div>
                                    <div class="form-group-sm row">
                                        <input type="hidden" name="sucursalID" value=<?php echo $sucursalID; ?>>
                                        <input type="hidden" name="ejercicioID" value=<?php echo $ejercicio; ?>>
                                        <label class="col-sm-6">&nbsp;</label>
                                        <div class="col-sm-6 text-right">
                                            <input type="submit" name="generales" class="btn btn-sm btn-secondary" value="Guardar">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card">
                            <div class="card-header">
                                <h5>Certificados</h5>
                            </div>
                            <div class="card-body">
                                <div id="certificados"></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="line"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-sm">
                        <thead>
                            <tr class="bg-primary text-light">
                                <th>Norma</th>
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">Descripción</th>
                                <th>Estado</th>
                                <th>NA</th>
                                <th colspan="3">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql = mysqli_query($link, "SELECT N.*, L.estatus FROM normas AS N LEFT JOIN levantamientos AS L ON N.normaID = L.normaID AND L.ejercicioID = $ejercicio AND L.sucursalID = $sucursalID");
                            if (mysqli_num_rows($sql) == 0) {
                                echo '<tr><td colspan="6">No hay datos.</td></tr>';
                            } else {
                                $ahora = new DateTime("today");
                                while ($row = mysqli_fetch_assoc($sql)) {
                            ?>
                                    <tr>
                                        <td><?php echo $row['clave'] ?></td>
                                        <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell"><?php echo $row['descripcion'] ?></td>
                                        <td><?php echo $row['estatus'] ?></td>
                                        <td>
                                            <!-- Default unchecked 
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="defaultUnchecked<?php echo $row["normaID"] ?>">
                                                <label class="custom-control-label" for="defaultUnchecked<?php echo $row["normaID"] ?>"></label>
                                            </div>-->
                                            <div class="custom-control custom-switch">
                                                <input type="checkbox" <?php if ($row["estatus"] == "AP") echo "disabled" ?> class="custom-control-input" id="customSwitch<?php echo $row["normaID"] ?>">
                                                <label class="custom-control-label" for="customSwitch<?php echo $row["normaID"] ?>"></label>
                                            </div>
                                        </td>
                                        <td>
                                            <?php
                                            if ($row['estatus'] == "AP") echo '<a role="button" class="btn btn-sm btn-outline-success" title="Iniciar Levantamiento" href="' . $row['script'] . '.php?sucursalID=' . $sucursalID . '"><span class="fa fa-play" aria-hidden="true"></span></a>';
                                            elseif ($row['estatus'] == "NA") echo '<span class="text-warning"> NA</span>';
                                            if ($row['estatus'] == "AP") echo '<a role="button" class="btn btn-sm btn-outline-info" title="Generar Estudio Word" href="' . $row['script'] . '-word.php?sucursalID=' . $sucursalID . '"><span class="fas fa-file-word" aria-hidden="true"></span></a>';
                                            if ($row["estatus"] == "AP") echo '<a href="" title="Eliminar" class="btn btn-outline-danger btn-sm deleteButton" data-toggle="modal" data-target="#exampleModal" data-norma="' . $row['normaID'] . '"><span class="fa fa-trash" aria-hidden="true"></span></a>';
                                            ?>
                                        </td>
                                    </tr>
                            <?php }
                            } ?>
                        </tbody>
                    </table>
                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Confirmar</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                ¿Está seguro que desea eliminar el levantamiento?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                                <a class='btn btn-outline-danger' href=''> Eliminar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="line"></div>
                <h2>Lorem Ipsum Dolor</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                    et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                    dolore eu
                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum.</p>

                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
    <script type="text/javascript">
        $(document).on("click", ".deleteButton", function(e) {
            //get data-id attribute of the clicked element
            var normaID = $(this).data('norma')
            //populate the textbox
            $(".modal-footer a").attr('href', 'visita.php?sucursalID=' + $sucursalID + '&aksi=delete&id=' + normaID);
        });

        $("input[type='checkbox']").on('change', function() {
            alert(this.id.substring(12));
            /* $.ajax({
                method: 'POST',
                url: '/uri/of/your/page',
                data: {
                    'my_checkbox_value': e.target.checked
                },
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                }
            }); */
        });
        function getCertificadosAsignados(asignacionID) {
            $.ajax({
                url: "certificadosAsignados.php",
                method: "POST",
                data: {
                    'asignacionID': asignacionID
                },
                dataType: 'json',
                success: function(data) {
                    let eCertificados = document.getElementById('certificados');
                    while (eCertificados.firstChild) {
                        eCertificados.firstChild.remove()
                    }
                    let now = new Date();
                    data.forEach(function(certificado) {
                        let checked = certificado.asignado == "1"?"checked":"";
                        let noVigente = new Date(certificado.fechafinal) < now?'&nbsp;&nbsp;<span class="badge badge-danger">NO VIGENTE<span>':'';
                        let radio = '<div class="custom-control custom-switch"> \
                                        <input type="checkbox" class="custom-control-input" name="switch[]" value="' + certificado.certificadoID + '" id="switch-' + certificado.certificadoID + '"' + checked +'> \
                                        <label class="custom-control-label" for="switch-'+ certificado.certificadoID + '" value="' + certificado.certificadoID + '">' + certificado.nombre + '</label>' + noVigente + ' \
                                    </div>'
                        //console.log(certificado);
                        eCertificados.insertAdjacentHTML('beforeend', radio);
                    });
                }
            })
        }
    </script>
</body>

</html>