<?php
include('autentificacion.php');
setlocale(LC_ALL, "es_MX");
date_default_timezone_set("America/Mexico_City");
// Include classes
include_once('estudiosTemplates/tbs_class.php'); // Load the TinyButStrong template engine
include_once('opentbs/tbs_plugin_opentbs.php'); // Load the OpenTBS plugin
// prevent from a PHP configuration problem when using mktime() and date()
if (version_compare(PHP_VERSION, '5.1.0') >= 0) {
    if (ini_get('date.timezone') == '') {
        date_default_timezone_set('UTC');
    }
}
// Initialize the TBS instance
$TBS = new clsTinyButStrong; // new instance of TBS
$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN); // load the OpenTBS plugin

if (isset($_GET["sucursalID"])) {
    require_once('config.php');
    require_once('functionsDB.php');
    include_once('nom-helpers.php');
    $ejercicio = getEjercicioAbierto($link);
    $userID = $_SESSION["id"];

    $table = "nom002"; // nombre de la tabla principal de la norma
    $tableID = $table . "ID"; // nombre de la columna ID de la norma, debrá seguir el estandar <nombreTabla><ID>
    $sucursalID = $_GET["sucursalID"];

    $sql = mysqli_query($link, "SELECT S.sucursal,Z.zona,R.region, A.* FROM asignaciones AS A INNER JOIN sucursales AS S ON A.sucursalID = S.sucursalID INNER JOIN zonas AS Z ON S.zonaID = Z.zonaID INNER JOIN regiones AS R ON Z.regionID = R.regionID WHERE ejercicioID = $ejercicio AND usuarioID = $userID AND A.sucursalID = $sucursalID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: index.php");
    } else {
        $exist = mysqli_query($link, "SELECT $tableID, sucursalID FROM $table WHERE ejercicioID = '$ejercicio' AND sucursalID = $sucursalID");
        if (mysqli_num_rows($exist) == 0) {
            header("location: index.php");
        } else {
            $row = mysqli_fetch_assoc($exist);
            $nomID = $row[$tableID];
        }
    }
} else {
    header("location: index.php");
}
// obtenemos datos almacenados en BD
$data = getDataFromDB();
$generales = getGeneralesFromDB();
//print("<pre>".print_r($data,true)."</pre>");
extract($generales, EXTR_OVERWRITE);
extract($data, EXTR_OVERWRITE);
$VolAguaIncendios = $litrosAguaIncendios / 1000;
$maxLiquidos = $maxAceiteHi + $maxDiesel; 
$nombreCompleto = getAsignadoFromDB();
extract($nombreCompleto, EXTR_OVERWRITE);
$fecha = getFechaFromDB();
extract($fecha, EXTR_OVERWRITE);
$sucursalData = getSucursalFromDB();
extract($sucursalData, EXTR_OVERWRITE);
$razon = getRazonDefaultFromDB();
extract($razon, EXTR_OVERWRITE);


$gradoRiesgo = ($maxGas / 3000) + ($maxGasolina / 1400) + ($maxLiquidos / 2000) + ($maxKg / 15000);
if (($gradoRiesgo < 1) && ($m2 < 3000)) {
    $leyendaGrado = "Grado de Riesgo de Incendio Ordinario";
    $leyendaResultado = "Menor a 1 en la evaluación de la ecuación 1";
    $leyendaEcuacion = "bajo, puesto que la sumatoria es menor a 1.";
} else {
    $leyendaGrado = "Grado de Riesgo de Incendio Alto";
    if ($gradoRiesgo < 1) {
        $leyendaResultado = "Menor a 1 en la evaluación de la ecuación 1";
        $leyendaEcuacion = "bajo, puesto que la sumatoria es menor a 1.";
    } else {
        $leyendaResultado = "Mayor a 1 en la evaluación de la ecuación 1";
        $leyendaEcuacion = "alto, puesto que la sumatoria es mayor a 1.";
    }
}
if ($m2 < 3000) {
    $r1 = 0;
} else {
    $r1 = 1;
}
if ($maxGas < 3000) {
    $r2 = 0;
} else {
    $r2 = 1;
}
if ($maxGasolina < 1400) {
    $r3 = 0;
} else {
    $r3 = 1;
}
if ( $maxLiquidos < 2000) {
    $r4 = 0;
} else {
    $r4 = 1;
}
if ($maxKg < 15000) {
    $r5 = 0;
} else {
    $r5 = 1;
}

$riesgos = $r1 + $r2 + $r3 + $r4 + $r5;


$tanquesgas = $DT["tanquesgas"]["rows"];
$tanquesgasTexto = "";
foreach($tanquesgas as $r){
    $tanquesgasTexto .= $r["Volumen"] . " " . $r["Ubicacion"];
    if(next($tanquesgas)==true) $tanquesgasTexto .= PHP_EOL;
}

$distaceitehi = $DT["distaceitehi"]["rows"];
$distaceitehiTexto = "";
foreach($distaceitehi as $r){
    $distaceitehiTexto .= $r["Cantidad"] . " " . $r["Descripcion"];
    if(next($distaceitehi)==true) $distaceitehiTexto .= PHP_EOL;
}

$distdiesel = $DT["distdiesel"]["rows"];
$distdieselTexto = "";
foreach($distdiesel as $r){
    $distdieselTexto .= $r["Cantidad"] . " " . $r["Descripcion"];
    if(next($distdiesel)==true) $distdieselTexto .= PHP_EOL;
}

$distgasolina = $DT["distgasolina"]["rows"];
$distgasolinaTexto = "";
foreach($distgasolina as $r){
    $distgasolinaTexto .= $r["Cantidad"] . " " . $r["Descripcion"];
    if(next($distgasolina)==true) $distgasolinaTexto .= PHP_EOL;
}

$distkg = $DT["distkg"]["rows"];
$distkgTexto = "";
foreach($distkg as $r){
    $distkgTexto .= $r["Cantidad"] . " " . $r["Descripcion"];
    if(next($distkg)==true) $distkgTexto .= PHP_EOL;
}

// Load the template
$template = 'noms/nom-002-cp.docx';
$TBS->LoadTemplate($template, OPENTBS_ALREADY_UTF8); // Also merge some [onload] automatic fields (depends of the type of document).


// Output the result
// Define the name of the output file
$save_as = (isset($_POST['save_as']) && (trim($_POST['save_as']) !== '') && ($_SERVER['SERVER_NAME'] == 'localhost')) ? trim($_POST['save_as']) : '';
$output_file_name = str_replace('.', '_' . date('Y-m-d') . $save_as . '.', $template);
if ($save_as === '') {
    // Output the result as a downloadable file (only streaming, no data saved in the server)
    $TBS->Show(OPENTBS_DOWNLOAD, $output_file_name); // Also merges all [onshow] automatic fields.
    // Be sure that no more output is done, otherwise the download file is corrupted with extra data.
    exit();
} else {
    // Output the result as a file on the server.
    $TBS->Show(OPENTBS_FILE, $output_file_name); // Also merges all [onshow] automatic fields.
    // The script can continue.
    exit("File [$output_file_name] has been created.");
}
