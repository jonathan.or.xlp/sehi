$(document).ready(function() {
    function check_session() {
        $.ajax({
            url: "check_session.php",
            method: "POST",
            success: function(data) {
                if (data == '1') {
                    alert('Tu sesión ha EXPIRADO!');
                    window.location.href = "login.php";
                }
            }
        })
    }
    setInterval(function() {
        check_session();
    }, 30000); //10000 means 10 seconds
});