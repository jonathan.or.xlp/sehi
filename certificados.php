<?php include('autentificacion.php'); ?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page Content  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> Certificados</h3>
                <?php

                require_once('config.php'); //conexión a la base de datos con variable $link

                if (isset($_GET['aksi']) == 'delete') {
                    // escaping, additionally removing everything that could be (html/javascript-) code
                    $id = mysqli_real_escape_string($link, (strip_tags($_GET["id"], ENT_QUOTES)));
                    $data = mysqli_query($link, "SELECT * FROM certificados WHERE certificadoID='$id'");
                    if (mysqli_num_rows($data) == 0) {
                        echo '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> No se encontraron datos.</div>';
                    } else {
                        $delete = mysqli_query($link, "DELETE FROM certificados WHERE certificadoID='$id'");
                        if ($row = mysqli_fetch_assoc($data)) {
                            unlink("equipos/" . $row["fileEquipo"]);
                            $files = glob("certificados/" . $row["certificadoID"]."/*");
                            foreach ($files as $file) {
                                if (is_file($file))
                                    unlink($file);
                            }
                            rmdir("certificados/" . $row["certificadoID"]);
                        }
                        if ($delete) {
                            echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Datos eliminado correctamente.</div>';
                        } else {
                            echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error, no se pudo eliminar los datos.<br>' . mysqli_error($link) . '</div>';
                        }
                    }
                }
                ?>
                <div class="row">
                    <div class="col text-right">
                        <a class="btn btn-secondary" href="certificadosEdit.php">Nuevo</a>
                    </div>
                </div>
                <div class="line"></div>

                <div class="">
                    <table class="table table-striped table-hover table-sm">
                        <thead>
                            <tr class="bg-primary text-light">
                                <th>Equipo</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Fecha Inicio</th>
                                <th>Fecha Término</th>
                                <th>Certificado</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql = mysqli_query($link, "SELECT certificadoID, nombre, descripcion, fechainicial, fechafinal, file, fileEquipo FROM certificados");

                            if (mysqli_num_rows($sql) == 0) {
                                echo '<tr><td colspan="8">No hay datos.</td></tr>';
                            } else {
                                $now = date("y.m.d");
                                while ($row = mysqli_fetch_assoc($sql)) {
                                    $noVigente = strtotime($row['fechafinal']) < strtotime($now)?'<br><span class="badge badge-danger">NO VIGENTE<span>':'';
                                    echo '
                                            <tr>
                                                <td><a href="" data-toggle="modal" data-target="#imageModal" data-file="' . 'equipos/' . $row['fileEquipo'] . '" title="Ver" class="verButton"><img src="' . 'equipos/' . $row['fileEquipo'] . '" class="img-thumbnail rounded float-right" width="100px" alt=""></a></td>
                                                <td width="20%">' . $row['nombre'] . $noVigente .'</td>
                                                <td width="30%">' . $row['descripcion'] . '</td>
                                                <td>' . $row['fechainicial'] . '</td>
                                                <td>' . $row['fechafinal'] . '</td>
                                                <td>';
                                                $valueFileNames = isset($row) && isset($row["file"]) ? unserialize($row["file"]) : [];
                                                foreach ($valueFileNames as $fileName) {
                                                    echo '<a href="" data-toggle="modal" data-target="#imageModal" data-file="' . 'certificados/' . $row['certificadoID'] . '/' . $fileName . '" title="Ver" class="verButton"><img src="' . 'certificados/' . $row['certificadoID'] . '/' . $fileName . '" class="img-thumbnail rounded float-right" width="60px" alt=""></a>';
                                                }
                                    echo        '</td>';
                                    echo        '<td>
                                                    <a href="certificadosEdit.php?certificadoID=' . $row['certificadoID'] . '" title="Editar datos" class="btn btn-outline-success btn-sm"><span class="fa fa-edit" aria-hidden="true"></span></a>
                                                    <a href="" title="Eliminar" class="btn btn-outline-danger btn-sm deleteButton" data-toggle="modal" data-target="#exampleModal" data-certificado="' . $row['certificadoID'] . '"><span class="fa fa-trash" aria-hidden="true"></span></a>
                                                </td>
                                            </tr>
                                            ';
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Confirmar</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                ¿Está seguro que desea eliminar el certificado?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                                <a class='btn btn-outline-danger' href=''> Eliminar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Ver</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <img src='' class="img-fluid" />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
    <script type="text/javascript">
        $(document).on("click", ".deleteButton", function(e) {
            var certificadoID = $(this).data('certificado');
            $(".modal-footer a").attr('href', 'certificados.php?aksi=delete&id=' + certificadoID);
        });
        $(document).on("click", ".verButton", function(e) {
            var src = $(this).data('file');
            $(".modal-body img").attr('src', decodeURIComponent(src));
        });
    </script>
</body>

</html>