<?php
function uploadFile($_FILES2, $_POST2, $inputFile)
{
    $target_dir = "equipos/";
    $target_file = $target_dir . basename($_FILES2[$inputFile]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    if (isset($_POST2["save"]) || isset($_POST2["saveNew"]) ) {
        $check = getimagesize($_FILES2[$inputFile]["tmp_name"]);
        if ($check !== false) {
            $message = "Imagen tipo - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            $message = "El archivo no es una imagen";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        $message = "El archivo ya existe, renombrelo";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES2[$inputFile]["size"] > 5000000) {
        $message = "El archivo es muy grande, debe ser menor a 5 MB";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if (
        $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif"
    ) {
        $message = "Solo se admiten formatos JPG, JPEG, PNG & GIF.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        //$message = "Error, el archivo no fue cargado.";
        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES2[$inputFile]["tmp_name"], $target_file)) {
            //echo "The file " . basename($_FILES2[$inputFile]["name"]) . " has been uploaded.";
            $uploadOk = 1;
        } else {
            $message = "Sorry, there was an error uploading your file.";
            $uploadOk = 0;
        }
    }
    return array($uploadOk,$message);
}

function uploadFiles($id, $files)
{
    $target_dir = "certificados/{$id}/";
    $status = 1;
    $message = "";
    $uploaded = [];

    if (!is_dir($target_dir)) {
        mkdir($target_dir, 0777, true);
        $scanned_directory = array_diff(scandir($target_dir), ['..', '.']);
    } else {
        $scanned_directory = array_diff(scandir($target_dir), ['..', '.']);
    }
    foreach ($files as $i => $fileArray) {
        $target_file = $target_dir . basename($fileArray["name"]);
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        $check = getimagesize($fileArray["tmp_name"]);
        if ($check == false) {
            $message = "El archivo no es una imagen";
            $status = 0;
            break;
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            $message = "El archivo ya existe, renombrelo";
            $status = 0;
            break;
        }
        // Check file size
        if ($fileArray["size"] > 5000000) {
            $message = "El archivo es muy grande, debe ser menor a 5 MB";
            $status = 0;
            break;
        }
        // Allow certain file formats
        if (
            $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif"
        ) {
            $message = "Solo se admiten formatos JPG, JPEG, PNG & GIF.";
            $status = 0;
            break;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($status == 1) {
            if (!move_uploaded_file($fileArray["tmp_name"], $target_file)) {
                $message = "ERROR AL CARGAR EL ARCHIVO";
                $status = 0;
                break;
            } else {
                array_push($uploaded, $fileArray["name"]);
            }
        }
    }
    if ($status == 0) { // checar si status es 0 borrar cualquier archivo subido en esta funcion
        //print("Borrar:<br><pre>".print_r($uploaded,true)."</pre>");
        foreach ($uploaded as $key => $value) {
            unlink($target_dir . basename($value));
        }
    } else { // caso contrario borrar todos los que no se subieron en esta funcion
        //print("Borrar:<br><pre>".print_r($scanned_directory,true)."</pre>");
        foreach ($scanned_directory as $key => $value) {
            unlink($target_dir . basename($value));
        }
    }
    return array($status, $message);
}
