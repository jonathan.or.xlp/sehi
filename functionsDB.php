<?php

function getEjercicioAbierto($link){
    $data = mysqli_query($link, "SELECT * FROM ejercicios WHERE estatus='abierto'");
    if (mysqli_num_rows($data) == 0) {
        return -1;
    }
    $row = mysqli_fetch_assoc($data);
    return $row["ejercicioID"];
}
?>