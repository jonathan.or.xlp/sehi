<?php
include('autentificacion.php');
setlocale(LC_ALL, "es_MX");
date_default_timezone_set("America/Mexico_City");

if (isset($_GET["sucursalID"])) {
    require_once('config.php');
    require_once('functionsDB.php');
    include_once('nom-helpers.php');

    $ejercicio = getEjercicioAbierto($link);
    $userID = $_SESSION["id"];

    $table = "nom002"; // nombre de la tabla principal de la norma
    $tableID = $table."ID"; // nombre de la columna ID de la norma, debrá seguir el estandar <nombreTabla><ID>
    $sucursalID = $_GET["sucursalID"];

    $sql = mysqli_query($link, "SELECT S.sucursal,Z.zona,R.region, A.* FROM asignaciones AS A INNER JOIN sucursales AS S ON A.sucursalID = S.sucursalID INNER JOIN zonas AS Z ON S.zonaID = Z.zonaID INNER JOIN regiones AS R ON Z.regionID = R.regionID WHERE ejercicioID = $ejercicio AND usuarioID = $userID AND A.sucursalID = $sucursalID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: index.php");
    } else {
        $row = mysqli_fetch_assoc($sql);

        $sucursal = $row["sucursal"];
        $zona = $row["zona"];
        $region = $row["region"];
        $fecha = $row["fecha"];
        $estatus = $row["estatus"];

        $exist = mysqli_query($link, "SELECT $tableID, sucursalID FROM $table WHERE ejercicioID = '$ejercicio' AND sucursalID = $sucursalID");

        if (mysqli_num_rows($exist) == 0) {
            // FALTA OBTENER DATOS DEL AÑO PASADO EN TABLA PRINCIPAL Y SECUNDARIAS
            // SI ES EL PRIMER AÑO SE INSERTA UN REGISTRO SOLO CON LOS CAMPOS PRINCIPALES
            $result = mysqli_query($link, "INSERT INTO $table (ejercicioID, sucursalID) VALUES ('$ejercicio',$sucursalID)");
            $nomID = mysqli_insert_id($link);
        }else{
            $row = mysqli_fetch_assoc($exist);
            $nomID = $row[$tableID];
        }
        $where = "WHERE $tableID = $nomID";
    }
}else{
    header("location: index.php");
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page Content  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-gavel" aria-hidden="true"></span> NOM-002-STPS-2010</h3>
                <h4>Prevención y protección contra incendios</h4>
                <?php require('nom-cards.php'); ?>
                <div class="line"></div>
                <div class="accordion" id="accordionExample">
                    <?php
                    // Procesamos todas las secciones
                    $d1 = proccessFormSection("1");
                    $d2 = proccessFormSection("2");
                    $d3 = proccessFormSection("3");
                    $d4 = proccessFormSection("4");
                    $d5 = proccessFormSection("5");
                    // Obtenemos banderas de captura de secciones
                    $c1 = sectionCompleted("1");
                    $c2 = sectionCompleted("2");
                    $c3 = sectionCompleted("3");
                    $c4 = sectionCompleted("4");
                    $c5 = sectionCompleted("5");
                    // obtenemos datos almacenados en BD
                    $data = getDataFromDB();
                    //print("<pre>".print_r($data,true)."</pre>");
                    
                    initSection("accordionExample", "1", "Equipos contra incendios", $d1, $c1);
                        questionBoolInt("rociadores", "¿Cuenta con rociadores?", $data);
                        questionBoolInt("alarmaIncendios", "¿Cuenta con alarma contra incendios?", $data);
                        questionInt("extintoresPQS", "Cantidad de extintores de PQS", $data);
                        questionInt("extintoresCO2", "Cantidad de extintores de CO2", $data);
                        questionInt("extintoresK", "Cantidad de extintores tipo K", $data);
                        questionInt("hidrantes", "Cantidad de hidrantes", $data);
                        questionInt("litrosAguaIncendios", "Cantidad de litros de agua almacenada para el sistema contra incendios", $data);
                        questionBoolInt("detecStoresHumo", "¿Cuenta con detectores de humo?", $data);
                        questionInt("brigadistas", "Cantidad de brigadistas capacitados", $data);
                    endSection($sucursalID, "1");
                    initSection("accordionExample", "2", "Materiales Combustibles, Gas, Líquidos y Sólidos", $d2, $c2);
                        questionRadio("tipoGas", "Tipo de Gas", array("LP" => 'LP', "Natural" => 'Natural'), $data, "LP");
                        questionInt("maxGas", "Cantidad máxima en litros de gas", $data);
                        questionTableDynamic("tanquesgas", "Datos de Taques de Gas", ["Serial" => ["Serial", "text"], "Volumen" => ["Volumen", "int"], "Fecha" => ["Fecha Fabricación", "date"], "Ubicacion" => ["Ubicación", "text"]], isset($data["DT"]["tanquesgas"]["rows"]) ? $data["DT"]["tanquesgas"]["rows"] : []);
                        questionInt("maxGasolina", "Cantidad máxima en litros de gasolina almacenada en tienda", $data);
                        questionTableDynamic("distgasolina", "Distribución Gasolina", ["Cantidad" => ["Cantidad", "int"], "Descripcion" => ["Descripción", "text"]], isset($data["DT"]["distgasolina"]["rows"]) ? $data["DT"]["distgasolina"]["rows"] : []);
                        questionInt("maxDiesel", "Cantidad máxima en litros de diesel almacenados en tienda", $data);
                        questionTableDynamic("distdiesel", "Distribución Diesel", ["Cantidad" => ["Cantidad", "int"], "Descripcion" => ["Descripción", "text"]], isset($data["DT"]["distdiesel"]["rows"]) ? $data["DT"]["distdiesel"]["rows"] : []);
                        questionInt("maxAceiteHi", "Cantidad máxima en litros de aceite hidráulico almacenado en tienda", $data);
                        questionTableDynamic("distaceitehi", "Distribución Aceite Hidraúlico", ["Cantidad" => ["Cantidad", "int"], "Descripcion" => ["Descripción", "text"]], isset($data["DT"]["distaceitehi"]["rows"]) ? $data["DT"]["distaceitehi"]["rows"] : []);
                        questionInt("maxKg", "Cantidad máxima en kg almacenada en tienda", $data);
                        questionTableDynamic("distkg", "Distribución por Kg", ["Cantidad" => ["Cantidad", "int"], "Descripcion" => ["Descripción", "text"]], isset($data["DT"]["distkg"]["rows"]) ? $data["DT"]["distkg"]["rows"] : []);
                    endSection($sucursalID, "2");
                    initSection("accordionExample", "3", "  Condiciones de Prevención y Protección Contra Incendios", $d3, $c3);
                        questionRadio("q15", "¿Cuentan con instrucciones de seguridad relativas a la ejecucion de trabajos en caliente en las áreas en las que se puedan presentar incendios?, y supervisar que éstas se cumplan", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q16", "¿Hay un programa anual de revisión mensual de los extintores, con el cual se vigile el cumplimiento de las condiciones mínimas de seguridad con las que deben cumplir dichos equipos?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q17", "¿Cuentan con el registro mensual de la revisión a los extintores?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q18", "¿Existe un programa anual de revisión y pruebas a los equipos contra incendio, a los medios de detección, a las alarmas de incendio y sistemas fijos contra incendio?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q19", "¿Existe un programa anual de revisión a las instalaciones eléctricas, a fin de identificar y corregir condiciones inseguras que puedan existir?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q20", "¿Existe un programa anual de revisión a las instalaciones de gas licuado de petróleo y/o natural, a fin de identificar y corregir condiciones inseguras que puedan existir?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q21", "¿Cuentan, con la señalización que prohíba fumar, generar flama abierta o chispas e introducir objetos incandescentes, cerillos, cigarrillos o, en su caso, utilizar teléfonos celulares, aparatos de radiocomunicación, u otros que puedan provocar ignición por no ser intrínsecamente seguros, en las áreas en donde se produzcan, almacenen o manejen materiales inflamables o explosivos. Dicha señalización deberá cumplir con lo establecido por la NOM-026-STPS-2008 o la NOM-003-SEGOB-2002?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q22", "¿Cuentan con señalización en la proximidad de los elevadores, que prohíba su uso en caso de incendio, de conformidad con lo establecido en la NOM-003-SEGOB-2002, o las que la sustituyan?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q23", "¿Se prohíbe y evita el bloqueo, daño, inutilización o uso inadecuado de los equipos y sistemas contra incendio, los equipos de protección personal, así como los señalamientos de evacuación, prevención y de equipos y sistemas contra incendio?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q24", "¿Tienen establecidos controles de acceso para los trabajadores y demás personas que ingresen a las áreas donde se almacenen, procesen o manejen materiales inflamables o explosivos?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q25", "¿Adoptan las medidas de seguridad para prevenir la generación y acumulación de electricidad estática en las áreas donde se manejan materiales inflamables o explosivos, de conformidad con lo establecido en la NOM-022-STPS-2008, o. Asimismo, controlar en dichas áreas el uso de herramientas, ropa, zapatos y objetos personales que puedan generar chispa, flama abierta o altas temperaturas?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q26", "¿Cuentan con las medidas o procedimientos de seguridad, para el uso de equipos de calefacción, calentadores, hornos, parrillas u otras fuentes de calor, en las áreas donde existan materiales inflamables o explosivos, y supervisar que se cumplan?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q27", "¿Se prohíbe y evita que se almacenen materiales o coloquen objetos que obstruyan e interfieran el acceso al equipo contra incendio o a los dispositivos de alarma de incendio o activación manual de los sistemas fijos contra incendio?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q28", "¿Cuentan con brigadas contra incendio, de primeros auxilios, de comunicación y de evacuación, deberán estar capacitadas y contar con los procedimientos y los recursos para desempeñar sus actividades?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q29", "¿Desarrollan simulacros de emergencias de incendio al menos dos veces al año?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q30", "¿Cuentan con medios de detección (calor, humo, flama o productos de la combustión), equipos contra incendio, sistemas fijos de protección contra incendio y alarmas de incendio?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                    endSection($sucursalID, "3");
                    initSection("accordionExample", "4", "   Rutas de Evacuación",$d4, $c4);
                        questionRadio("q31", "¿Están señalizadas en lugares visibles, de conformidad a la NOM-026-STPS-2008 o la NOM-003-SEGOB-2002?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q32", "¿Se encuentran libres de obstáculos que impidan la circulación de los trabajadores y demás ocupantes?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q33", "¿Disponen de dispositivos de iluminación de emergencia cuando se interrumpa la energía eléctrica o falte iluminación natural?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q34", "¿Que la distancia por recorrer desde el punto más alejado del interior de una edificación, hacia cualquier punto de la ruta de evacuación, no sea mayor de 40 m. En caso contrario, el tiempo máximo de evacuación de los ocupantes a un lugar seguro deberá ser de tres minutos?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q35", "¿Las escaleras eléctricas serán consideradas parte de una ruta de evacuación, previo bloqueo de la energía que las alimenta y de su movimiento?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q36", "¿Los elevadores no son considerados parte de una ruta de evacuación y no se usen en caso de incendio?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q37", "¿Los desniveles o escalones en los pasillos y corredores de las rutas de evacuación están señalizados, de conformidad con la NOM-026-STPS-2008 o la NOM-003-SEGOB-2002?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                    endSection($sucursalID, "4");
                    initSection("accordionExample", "5", "   Salidas normales o de emergencia",$d5, $c5);
                        questionRadio("q38", "¿Están identificadas conforme a lo señalado en la NOM-026-STPS-2008 o la NOM-003-SEGOB-2002, o las que las sustituyan?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q39", "¿Comunican a un descanso, en caso de acceder a una escalera?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q40", "¿Las puertas de las salidas de emergencia, abren en el sentido del flujo, salvo que sean automáticas y corredizas?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q41", "¿Las puertas son de materiales resistentes al fuego y capaces de impedir el paso del humo entre áreas de trabajo, en caso de quedar clasificados el área o centro de trabajo como de riesgo de incendio alto, y se requiera impedir la propagación de un incendio hacia una ruta de evacuación o áreas contiguas por presencia de materiales inflamables o explosivos?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q42", "¿Las puertas de emergencia cuentan con un mecanismo que permite abrirlas desde el interior, mediante una operación simple de empuje?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q43", "¿Las puertas consideradas como salidas de emergencia están libres de obstáculos, candados, picaportes o cerraduras con seguros puestos durante las horas laborales, que impidan su utilización en casos de emergencia?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                        questionRadio("q44", "¿Que cuando sus puertas sean consideradas como salidas de emergencia, y funcionen en forma automática, o mediante dispositivos eléctricos o electrónicos, permitan la apertura manual, si llegara a interrumpirse la energía eléctrica en situaciones de emergencia?", array("S" => 'Sí', "N" => 'No', "NA" => "No Aplica"), $data, "NA", 8);
                    endSection($sucursalID, "5");
                    ?>
                </div>
            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
    <?php require('nom-helpers-script.php') ?>
</body>

</html>