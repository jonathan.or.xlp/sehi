<?php include('autentificacion.php');
setlocale(LC_ALL, "es_MX");
date_default_timezone_set("America/Mexico_City"); ?>
<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page Content  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> Sucursales Asignadas</h3>
                <div class="line"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-sm">
                        <caption>
                            <div class="text-right">
                                <h6>Fecha:
                                    <span class=" badge badge-danger">Vencida</span>
                                    <span class=" badge badge-warning">Próxima (&lt;5 días)</span>
                                </h6>
                            </div>
                        </caption>
                        <thead>
                            <tr class="bg-primary text-light">
                                <th>Sucursal</th>
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">Zona</th>
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">Region</th>
                                <th>Fecha</th>
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">Estado</th>
                                <th class="text-center">ESTD</th>
                                <th class="text-center">STPS</th>
                                <th class="text-center">AUDI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $ejercicio = getEjercicioAbierto($link);
                            $userID = $_SESSION["id"];
                            $sql = mysqli_query($link, "SELECT S.sucursalID, S.sucursal,Z.zona,R.region, A.* FROM asignaciones AS A INNER JOIN sucursales AS S ON A.sucursalID = S.sucursalID INNER JOIN zonas AS Z ON S.zonaID = Z.zonaID INNER JOIN regiones AS R ON Z.regionID = R.regionID WHERE ejercicioID = $ejercicio and usuarioID = $userID ORDER BY FIELD(A.estatus,'PENDIENTE','EN PROGRESO','COMPLETADA'), A.fecha ASC");
                            if (mysqli_num_rows($sql) == 0) {
                                echo '<tr><td colspan="6">No hay datos.</td></tr>';
                            } else {
                                $ahora = new DateTime("today");
                                while ($row = mysqli_fetch_assoc($sql)) {
                                    $fecha = (new DateTime($row['fecha']));
                                    $diff = date_diff($fecha, $ahora);
                                    if ($diff->format("%r%a") >= 0) $daysdiff = 1;
                                    elseif ($diff->format("%r%a") >= -5 && $diff->format("%r%a") <= -1) $daysdiff = 2;
                                    else $daysdiff = 3;
                            ?>
                                    <tr>
                                        <td><?php echo $row['sucursal'] ?></td>
                                        <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell"><?php echo $row['zona'] ?></td>
                                        <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell"><?php echo $row['region'] ?></td>
                                        <td class="<?php if ($row['estatus'] == "COMPLETADA") echo 'text-success';
                                                    elseif ($row['estatus'] == "EN PROGRESO") echo 'text-primary';
                                                    elseif ($row['estatus'] == "PENDIENTE" && $daysdiff == 1) echo 'text-danger';
                                                    elseif ($row['estatus'] == "PENDIENTE" && $daysdiff == 2) echo 'text-warning'; ?>"><?php echo $row['fecha'] ?></td>
                                        <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell <?php if ($row['estatus'] == "PENDIENTE") echo 'text-warning';
                                            else if ($row['estatus'] == "EN PROGRESO") echo 'text-primary';
                                            else if ($row['estatus'] == "COMPLETADA") echo 'text-success'; ?>"><?php echo $row['estatus'] ?></td>
                                        <td class="text-center">
                                            <?php
                                            if ($row['estatus'] == "PENDIENTE") echo '<a role="button" class="btn btn-sm btn-outline-warning" title="Iniciar Estudios" href="visita.php?sucursalID=' . $row['sucursalID'] . '"><span class="fa fa-play" aria-hidden="true"></span></a>';
                                            elseif ($row['estatus'] == "EN PROGRESO") echo '<a role="button" class="btn btn-sm btn-outline-primary" title="Continuar Estudios" href="visita.php?sucursalID=' . $row['sucursalID'] . '"><span class="fa fa-pause" aria-hidden="true"></span></a>';
                                            elseif ($row['estatus'] == "COMPLETADA") echo '<a role="button" class="btn btn-sm btn-outline-success" title="Ver Estudios" href="visita.php?sucursalID=' . $row['sucursalID'] . '"><span class="fas fa-eye" aria-hidden="true"></span></a>';
                                            ?>
                                        </td>
                                        <td class="text-center">
                                            <?php
                                            if ($row['estatus'] == "PENDIENTE") echo '<a role="button" class="btn btn-sm btn-outline-warning" title="Iniciar STPS" href="visita.php?sucursalID=' . $row['sucursalID'] . '"><span class="fa fa-play" aria-hidden="true"></span></a>';
                                            elseif ($row['estatus'] == "EN PROGRESO") echo '<a role="button" class="btn btn-sm btn-outline-primary" title="Continuar STPS" href="visita.php?sucursalID=' . $row['sucursalID'] . '"><span class="fa fa-pause" aria-hidden="true"></span></a>';
                                            elseif ($row['estatus'] == "COMPLETADA") echo '<a role="button" class="btn btn-sm btn-outline-success" title="Ver STPS" href="visita.php?sucursalID=' . $row['sucursalID'] . '"><span class="fas fa-eye" aria-hidden="true"></span></a>';
                                            ?>
                                        </td>
                                        <td class="text-center">
                                            <?php
                                            if ($row['estatus'] == "PENDIENTE") echo '<a role="button" class="btn btn-sm btn-outline-warning" title="Iniciar Auditoría" href="visita.php?sucursalID=' . $row['sucursalID'] . '"><span class="fa fa-play" aria-hidden="true"></span></a>';
                                            elseif ($row['estatus'] == "EN PROGRESO") echo '<a role="button" class="btn btn-sm btn-outline-primary" title="Continuar Auditoría" href="visita.php?sucursalID=' . $row['sucursalID'] . '"><span class="fa fa-pause" aria-hidden="true"></span></a>';
                                            elseif ($row['estatus'] == "COMPLETADA") echo '<a role="button" class="btn btn-sm btn-outline-success" title="Ver Auditoría" href="visita.php?sucursalID=' . $row['sucursalID'] . '"><span class="fas fa-eye" aria-hidden="true"></span></a>';
                                            ?>
                                        </td>
                                    </tr>
                            <?php }
                            } ?>
                        </tbody>
                    </table>
                </div>              
                <div class="line"></div>
                <h2>Lorem Ipsum Dolor</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                    et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                    dolore eu
                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum.</p>

                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
</body>

</html>