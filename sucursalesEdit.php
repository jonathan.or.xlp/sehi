<?php
include('autentificacion.php');
require_once('config.php');

if (isset($_POST['saveNew'])) {
    $action = 'new';
    $clave = $_POST["clave"];
    $sucursal = $_POST["sucursal"];
    $zonaID = $_POST["zonaID"];
    $tipoSucursalID = $_POST["tipoSucursalID"];
    $direccion = $_POST["direccion"];
    $colonia = $_POST["colonia"];
    $cp = $_POST["cp"];
    $municipio = $_POST["municipio"];
    $estado = $_POST["estado"];

    $update = mysqli_query($link, "INSERT INTO sucursales (clave, sucursal, zonaID, tipoSucursalID, direccion, colonia, cp, municipio, estado) VALUES('$clave','$sucursal', $zonaID, $tipoSucursalID, $direccion, $colonia, $cp, $municipio, $estado)");
    $sucursalID = mysqli_insert_id($link);
} elseif (isset($_POST['save'])) {
    $action = 'update';
    $sucursalID = $_POST['sucursalID'];
    $clave = $_POST["clave"];
    $sucursal = $_POST["sucursal"];
    $zonaID = $_POST["zonaID"];
    $tipoSucursalID = $_POST["tipoSucursalID"];
    $direccion = $_POST["direccion"];
    $colonia = $_POST["colonia"];
    $cp = $_POST["cp"];
    $municipio = $_POST["municipio"];
    $estado = $_POST["estado"];

    $update = mysqli_query($link, "UPDATE sucursales SET clave='$clave', sucursal='$sucursal', zonaID=$zonaID, tipoSucursalID=$tipoSucursalID, direccion=$direccion, coonia=$colonia, cp=$cp, municipio=$municipio, estado=$estado WHERE sucursalID=$sucursalID");
}if (isset($_GET["sucursalID"])) {
    $action='edit';
    $sucursalID = $_GET["sucursalID"];
    $sql = mysqli_query($link, "SELECT * FROM sucursales WHERE sucursalID=$sucursalID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: sucursales.php");
    } else {
        $row = mysqli_fetch_assoc($sql);

        $sucursalID = $row['sucursalID'];
        $sucursal = $row["sucursal"];
        $clave = $row["clave"];
        $zonaID = $row["zonaID"];
        $tipoSucursalID = $row["tipoSucursalID"];
        $direccion = $row["direccion"];
        $colonia = $row["colonia"];
        $cp = $row["cp"];
        $municipio = $row["municipio"];
        $estado = $row["estado"];
    }
} else {
    //header("location: usuarios.php");
    $sucursalID = 0;
    $action='new';
    $sucursal = '';
    $clave = '';
    $zonaID=0;
    $tipoSucursalID=0;
    $direccion = "";
    $colonia = "";
    $cp = "";
    $municipio = "";
    $estado = "";
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page linktent  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> <?php echo $action=='new'? 'Agregar Sucursal':'Editar Sucursal'; ?></h3>
                <div class="line"></div>
                <br/>
                <div class="">
                    <?php
                    if (isset($update) && $update) {
                        echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
                    } elseif (isset($update) && !$update) {
                        echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.<br>'.mysqli_error($link).'</div>';
                    }
                    ?>
                    <form action="" method="post" class="ml-4">
                        <div class="form-group row">
                            <label for="clave" class="col-sm-1 col-form-label">Clave</label>
                            <div class="col-sm-4">
                                <input type="text" name="clave" value="<?php echo $clave; ?>" class="form-control" placeholder="clave" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="sucursal" class="col-sm-1 col-form-label">Sucursal</label>
                            <div class="col-sm-4">
                                <input type="text" name="sucursal" value="<?php echo $sucursal; ?>" class="form-control" placeholder="sucursal" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="zonaID" class="col-sm-1 col-form-label">Zona</label>
                            <div class="col-sm-4">
                                <select name="zonaID" class="form-control">
                                    <?php
                                    $data = mysqli_query($link, "SELECT * FROM zonas");
                                    while ($row = mysqli_fetch_assoc($data)) { ?>
                                        <option value=<?php echo $row['zonaID'];
                                                        if ($zonaID == $row['zonaID']) echo ' selected'; ?>><?php echo $row['zona']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tipoSucursalID" class="col-sm-1 col-form-label">Tipo</label>
                            <div class="col-sm-4">
                                <select name="tipoSucursalID" class="form-control">
                                    <?php
                                    $data = mysqli_query($link, "SELECT * FROM tipossucursal");
                                    while ($row = mysqli_fetch_assoc($data)) { ?>
                                        <option value=<?php echo $row['tipoSucursalID'];
                                                        if ($tipoSucursalID == $row['tipoSucursalID']) echo ' selected'; ?>><?php echo $row['tipoSucursal']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="direccion" class="col-sm-1 col-form-label">Dirección</label>
                            <div class="col-sm-4">
                                <input type="text" name="direccion" value="<?php echo $direccion; ?>" class="form-control" placeholder="dirección" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="colonia" class="col-sm-1 col-form-label">Colonia</label>
                            <div class="col-sm-4">
                                <input type="text" name="colonia" value="<?php echo $colonia; ?>" class="form-control" placeholder="colonia" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="cp" class="col-sm-1 col-form-label">C.P.</label>
                            <div class="col-sm-4">
                                <input type="text" name="cp" value="<?php echo $cp; ?>" class="form-control" placeholder="c.p." required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="municipio" class="col-sm-1 col-form-label">Ciudad</label>
                            <div class="col-sm-4">
                                <input type="text" name="municipio" value="<?php echo $municipio; ?>" class="form-control" placeholder="municipio" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="estado" class="col-sm-1 col-form-label">Estado</label>
                            <div class="col-sm-4">
                                <input type="text" name="estado" value="<?php echo $estado; ?>" class="form-control" placeholder="estado" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <input type="hidden" name="sucursalID" value=<?php echo $sucursalID; ?>>
                            <label class="col-sm-1">&nbsp;</label>
                            <div class="col-sm-4 text-right">
                                <a href="sucursales.php" class="btn btn-sm btn-light">Regresar</a>
                                <input type="submit" name="<?php echo $action=='edit'? 'save' : 'saveNew';?>" class="btn btn-sm btn-secondary" value="Guardar">
                            </div>
                        </div>
                    </form>
                </div>


                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
</body>

</html>