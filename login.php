<?php
// Initialize the session
session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
  header("location: index.php");
  exit;
}

// Include config file
require_once "config.php";

// Define variables and initialize with empty values
$username = $password = $fullname = "";
$username_err = $password_err = "";

// Processing form data when form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {

  // Check if username is empty
  if (empty(trim($_POST["username"]))) {
    $username_err = "Escribe tu nombre de usuario";
  } else {
    $username = trim($_POST["username"]);
  }

  // Check if password is empty
  if (empty(trim($_POST["password"]))) {
    $password_err = "Escribe tu contraseña";
  } else {
    $password = trim($_POST["password"]);
  }

  // Validate credentials
  if (empty($username_err) && empty($password_err)) {
    // Prepare a select statement
    $sql = "SELECT usuarioID, usuario, pass, nombreCompleto, rolID FROM usuarios WHERE usuario = ?";

    if ($stmt = mysqli_prepare($link, $sql)) {
      // Bind variables to the prepared statement as parameters
      mysqli_stmt_bind_param($stmt, "s", $param_username);

      // Set parameters
      $param_username = $username;

      // Attempt to execute the prepared statement
      if (mysqli_stmt_execute($stmt)) {
        // Store result
        mysqli_stmt_store_result($stmt);

        // Check if username exists, if yes then verify password
        if (mysqli_stmt_num_rows($stmt) == 1) {
          // Bind result variables
          mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password, $fullname, $rolID);
          if (mysqli_stmt_fetch($stmt)) {
            if ($password == $hashed_password) {
              //if(password_verify($password, $hashed_password)){
              // Password is correct, so start a new session
              session_start();

              // Store data in session variables
              $_SESSION["loggedin"] = true;
              $_SESSION["id"] = $id;
              $_SESSION["username"] = $username;
              $_SESSION["fullname"] = $fullname;
              $_SESSION["rolID"] = $rolID;
              $_SESSION["USER_TIME"] = time();

              // Redirect user to welcome page
              switch ($rolID) {
                case 1:
                  header("location: home_admin.php");
                  break;
                case 2:
                  header("location: home_tec.php");
                  break;
                default:
                  header("location: index.php");
              }
            } else {
              // Display an error message if password is not valid
              $password_err = "Contraseña incorrecta";
            }
          }
        } else {
          // Display an error message if username doesn't exist
          $username_err = "Usuario no existe";
        }
      } else {
        echo "Oops! algo salio mal, contactar a soporte";
      }
    }

    // Close statement
    mysqli_stmt_close($stmt);
  }

  // Close connection
  mysqli_close($link);
}
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Sistema Protección Seguridad e Higiene | Login</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="vendor/bootstrap/bootstrap.min.css">
  <!-- Our Custom CSS -->
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/my-login.css">
  <!-- Font Awesome JS -->
  <script defer src="vendor/fontawesome/js/solid.js"></script>
  <script defer src="vendor/fontawesome/js/fontawesome.min.js"></script>
  <!-- Favicon-->
  <link rel="apple-touch-icon" sizes="57x57" href="icons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="icons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="icons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="icons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="icons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="icons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="icons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="icons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="icons/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="icons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="icons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="icons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="icons/favicon-16x16.png">
  <link rel="manifest" href="icons/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="icons/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

</head>

<body class="my-login-page">
  <section class="h-100 content">
    <div class="card-wrapper h-100">
      <div class="brand">
        <img src="img/logo.png">
      </div>
      <div class="card fat">
        <div class="card-body">
          <h4 class="card-title">Iniciar sesión</h4>
          <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

            <div class="form-group">
              <label for="usuario" class="sr-only">Usuario</label>
              <input type="text" id="username" name="username" class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>" placeholder="Usuario" required autofocus>
              <div class="form-text invalid-feedback"><?php echo $username_err; ?></div>
            </div>
            <div class="form-group">
              <label for="inputPassword" class="sr-only">Contraseña</label>
              <input type="password" id="password" name="password" class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>" placeholder="Contraseña" required>
              <div class="form-text invalid-feedback"><?php echo $password_err; ?></div>
            </div>

            <div class="form-group no-margin">
              <button type="submit" class="btn btn-primary btn-block">
                Entrar
              </button>
            </div>

          </form>
        </div>
      </div>
      <div class="footer">
        Copyright &copy; TIENDAS CHEDRAUI S.A. DE C.V. 2019
      </div>
    </div>
  </section>
  <script src="vendor/bootstrap/jquery-3.3.1.slim.min.js"></script>
  <script src="vendor/bootstrap/popper.min.js"></script>
  <script src="vendor/bootstrap/bootstrap.min.js"></script>
  <script src="js/my-login.js"></script>
</body>

</html>