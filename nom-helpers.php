<?php
setlocale(LC_ALL, "es_MX");
date_default_timezone_set("America/Mexico_City");

if (!function_exists('array_key_first')) {
    function array_key_first(array $arr)
    {
        foreach ($arr as $key => $unused) {
            return $key;
        }
        return NULL;
    }
}

function sectionCompleted($sectionName)
{
    $table =  $GLOBALS["table"];
    $nomID = $GLOBALS["nomID"];
    $select = mysqli_query($GLOBALS["link"], "SELECT capturada FROM nom_section WHERE nomName = '$table' AND nomID = $nomID AND seccion = '$sectionName'");
    if (mysqli_num_rows($select) == 0) return false;
    return true;
}

function getGeneralesFromDB()
{
    $ejercicio = $GLOBALS["ejercicio"];
    $sucursalID = $GLOBALS["sucursalID"];
    $select = mysqli_query($GLOBALS["link"], "SELECT * FROM sucursalesgenerales WHERE ejercicioID = $ejercicio AND sucursalID = $sucursalID");
    $row = mysqli_fetch_assoc($select);
    return $row;
}

function getAsignadoFromDB()
{
    $ejercicio = $GLOBALS["ejercicio"];
    $sucursalID = $GLOBALS["sucursalID"];
    $select = mysqli_query($GLOBALS["link"], "SELECT usuarios.nombreCompleto FROM asignaciones INNER JOIN usuarios USING (usuarioID) WHERE asignaciones.sucursalID = $sucursalID AND ejercicioID = '$ejercicio'");
    $row = mysqli_fetch_assoc($select);
    return $row;
}

function getFechaFromDB()
{
    $ejercicio = $GLOBALS["ejercicio"];
    $sucursalID = $GLOBALS["sucursalID"];
    $select = mysqli_query($GLOBALS["link"], "SELECT fecha FROM asignaciones WHERE sucursalID = $sucursalID AND ejercicioID = '$ejercicio'");
    $row = mysqli_fetch_assoc($select);
    return $row;
}

function getSucursalFromDB()
{
    $sucursalID = $GLOBALS["sucursalID"];
    $select = mysqli_query($GLOBALS["link"], "SELECT sucursal,clave,direccion,colonia,municipio,estado FROM sucursales WHERE sucursalID = $sucursalID");
    $row = mysqli_fetch_assoc($select);
    return $row;
}

function getRazonDefaultFromDB()
{
    $select = mysqli_query($GLOBALS["link"], "SELECT razon FROM razonessociales INNER JOIN config USING (razonID)");
    $row = mysqli_fetch_assoc($select);
    return $row;
}

function getDataFromDB()
{
    $tableID = $GLOBALS["tableID"];
    $table =  $GLOBALS["table"];
    $nomID = $GLOBALS["nomID"];
    $select = mysqli_query($GLOBALS["link"], "SELECT * FROM $table WHERE $tableID = $nomID");
    $row = mysqli_fetch_assoc($select);

    $dynamicTables = mysqli_query($GLOBALS["link"], "SELECT TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_SCHEMA = '" . DB_NAME . "' AND REFERENCED_TABLE_NAME = '$table'");
    while ($r = mysqli_fetch_assoc($dynamicTables)) {
        $tableName = $r["TABLE_NAME"];
        $columnName = $r["COLUMN_NAME"];
        $select2 = mysqli_query($GLOBALS["link"], "SELECT * FROM $tableName WHERE $columnName = $nomID");
        //$rows = mysqli_fetch_all($select2,MYSQLI_ASSOC);
        $rows = array();
        while ($rowx = mysqli_fetch_array($select2, MYSQLI_ASSOC)) {
            $rows[] = $rowx;
        }
        $rows = array_map(function ($a) {
            return array_slice($a, 1);
        }, $rows);
        $row["DT"][explode("_", $r["TABLE_NAME"])[1]]["rows"] = $rows;
    }
    return $row;
    /* SELECT TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_SCHEMA = 'sehi' AND REFERENCED_TABLE_NAME = 'nom002'; */
}

function proccessFormSection($sectionName)
{
    if (!empty($_POST["section" . $sectionName])) {

        $DATA = $_POST; // Copia de $_POST
        if (!empty($_FILES)) {
            foreach ($_FILES as $key => $value) {
                $_MYFILES = array();
                foreach (array_keys($_FILES[$key]['name']) as $i) { // loop over 0,1,2,3 etc...
                    foreach (array_keys($_FILES[$key]) as $j) { // loop over 'name', 'size', 'error', etc...
                        $_MYFILES[$i][$j] = $_FILES[$key][$j][$i]; // "swap" keys and copy over original array values
                    }
                }
                if (array_sum($_FILES[$key]["error"]) == UPLOAD_ERR_OK) {
                    $upload = uploadFiles($key, $_MYFILES);
                    if (isset($upload) && isset($upload["0"]) && $upload["0"] == 1) {
                        $DATA[$key] = serialize($_FILES[$key]["name"]);
                    }
                }
            }
        }
        $tableNames = []; // Lista con los nombres de las tablas dinamicas en la sección
        $tableValues = []; // Lista con los valores recibidos de las tablas dinámicas de la sección
        $gTable =  $GLOBALS["table"]; // Obtiene el nombre de la tabla principal de la norma declarada en el archivo que llama a estas funciones
        $gwhere =  $GLOBALS["where"]; // Obtiene la sentencia WHERE de la norma declarada en el archivo que llama a estas funciones
        $ejercicio = $GLOBALS["ejercicio"];
        $sucursalID = $GLOBALS["sucursalID"];
        $nomID = $GLOBALS["nomID"];

        $updateSets = "";
        $insertKeys = "";
        $insertValues = "";

        //
        if (!sectionCompleted($sectionName)) {
            $select = mysqli_query($GLOBALS["link"], "INSERT INTO nom_section (nomName, nomID, seccion, capturada) VALUES ('$gTable',$nomID,'$sectionName',true)");
        }

        unset($DATA["section" . $sectionName]);
        unset($DATA["sucursalID"]);
        // VALIDAR QUE $DATA Tenga valores a procesar
        if (!empty($DATA)) {
            foreach ($DATA as $key => $value) {
                if (substr($key, 0, strlen("chk")) === "chk")
                    continue;
                if (substr($key, 0, strlen("bool")) === "bool")
                    continue;
                if (is_numeric($value) || $value === "true" || $value === "false") {
                    $updateSets .= ", $key = $value";
                    $insertKeys .= ", $key";
                    $insertValues .= ", $value";
                } elseif (is_array($value)) {
                    $ex  = explode("-", $key);
                    if (!in_array($ex[0], $tableNames, false)) {
                        array_push($tableNames, $ex[0]);
                    }
                    $tableValues[$ex[0]][$ex[1]] = $value;
                } else {
                    $updateSets .= ", $key = '$value'";
                    $insertKeys .= ", $key";
                    $insertValues .= ", '$value'";
                }
            }
            // Tablas dinamicas
            // Agrupamos registros
            foreach ($tableValues as $t => $cs) {
                foreach ($cs as $cn => $c) {
                    $xd = 0;
                    foreach ($c as $v) {
                        $tableValues[$t]["rows"][$xd++][$cn] = $v;
                    }
                }
            }
            // Eliminamos datos ya agrupados
            foreach ($tableValues as $t => $cs) {
                foreach ($cs as $cn => $c) {
                    $xd = 0;
                    if ($cn != "rows") {
                        unset($tableValues[$t][$cn]);
                    }
                }
            }
            //
            $update = "UPDATE $gTable SET " . substr($updateSets, 1, strlen($updateSets) - 1) . " $gwhere";

            if ($insertKeys != "") {
                $insert = "INSERT INTO $gTable (ejercicioID, sucursalID $insertKeys) VALUES ($ejercicio, $sucursalID $insertValues)";

                $updateResult = mysqli_query($GLOBALS["link"], $update);
                if (isset($updateResult) && $updateResult) {
                    $div = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
                } elseif (isset($updateResult) && !$updateResult) {
                    $div = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.<br>' . mysqli_error($GLOBALS["link"]) . '</div>';
                }
            }
            $div = proccessDynamicTables($tableValues);

            $_POST["DT"] = $tableValues;
            //print("<pre>".print_r($tableValues,true)."</pre>");
            //echo $update . "<br>";
            //echo $insert . "<br>";
            return $div;
        }
        if (isset($upload) && isset($upload["0"]) && $upload["0"] == 0) {
            return '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo cargar la imagen.<br>' . $upload["1"] . '</div>';
        }
    }
    return NULL;
}

function proccessDynamicTables($tableValues)
{
    $gTable =  $GLOBALS["table"]; // Obtiene el nombre de la tabla principal de la norma declarada en el archivo que llama a estas funciones
    $nomID = $GLOBALS["nomID"];


    foreach ($tableValues as $table => $rows) {
        try {
            mysqli_begin_transaction($GLOBALS["link"], MYSQLI_TRANS_START_READ_WRITE);
            $delete = mysqli_query($GLOBALS["link"], "DELETE FROM $gTable" . "_" . "$table WHERE $gTable" . "ID = $nomID");
            foreach ($rows["rows"] as $row) {
                $updateSets = "";
                $insertKeys = "";
                $insertValues = "";
                foreach ($row as $key => $value) {
                    if (is_numeric($value)) {
                        $updateSets .= ", $key = $value";
                        $insertKeys .= ", $key";
                        $insertValues .= ", $value";
                    } else {
                        $updateSets .= ", $key = '$value'";
                        $insertKeys .= ", $key";
                        $insertValues .= ", '$value'";
                    }
                }
                //$update = "UPDATE $gTable-$table SET " . substr($updateSets,1,strlen($updateSets)-1) . " $gwhere";
                $insert = mysqli_query($GLOBALS["link"], "INSERT INTO $gTable" . "_" . "$table ($gTable" . "ID $insertKeys) VALUES ($nomID $insertValues)");
                //echo "INSERT INTO $gTable"."_"."$table ($gTable"."ID $insertKeys) VALUES ($nomID $insertValues)";
                //echo $update . "<br>";
                //echo $delete . "<br>";
                //echo $insert . "<br>";
            }
            mysqli_commit($GLOBALS["link"]);
        } catch (Exception $th) {
            mysqli_rollback($GLOBALS["link"]);
            return '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.<br>' .$th . '</div>';
        }
    }
    return '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
}

function initSection($parent, $sectionName, $sectionTittle, $divresult, $completed = false, $enctype = "")
{
    //Markup
    $percent = $completed ? 100 : 50;
    $status = $completed ? "CAPTURADA" : "PENDIENTE";
    $bgStatusBar = $completed ? "bg-success" : "bg-warning";
    $show = !empty($_POST["section" . $sectionName]) ? "show" : "";
    echo "<div class='card'>
        <div class='card-header' id='heading$sectionName'>
            <h5 class='mb-0 row justify-content-between' data-toggle='collapse' data-target='#collapse$sectionName'>
                <div class='col-sm-10'>
                    <button type='button' class='btn btn-primary' style='margin-right:15px;' >
                    <i class='fa fa-plus'></i></button>
                    <span class='col-sm-11'>$sectionTittle</span>
                </div>
                <div class='col-sm-2'>
                    <div class='progress'>
                        <div class='progress-bar $bgStatusBar' role='progressbar' style='width: $percent%;' aria-valuenow='$percent' aria-valuemin='0' aria-valuemax='100'>$status</div>
                    </div>
                </div>
            </h5>
        </div>
        <div id='collapse$sectionName' class='collapse $show' aria-labelledby='heading$sectionName' data-parent='#$parent'>
            <div class='card-body'>
            $divresult
            <form action='' method = 'post' id='$sectionName' $enctype>";
}

function endSection($sucursalID, $sectionName)
{
    echo "
    <div class='form-group row'>
                            <input type='hidden' name='sucursalID' value=$sucursalID>
                            <label class='col-sm-3'>&nbsp;</label>
                            <div class='col-sm-9 text-right'>
                                <input type='submit' name='section$sectionName' class='btn btn-sm btn-secondary' value='Guardar' form='$sectionName'/>
                            </div>
                        </div>
    </form></div></div></div>";
}

function questionBool($id, $title, $data)
{
    $value = isset($data) && isset($data[$id]) ? $data[$id] : false;
    if ($value === "true") $cheked = "checked";
    else $cheked = "";
    echo "
    <div class='form-check'>
        <input class='form-check-input' type='checkbox' name='bool$id' id='bool$id' value='true' $cheked/>
        <label class='form-check-label col-form-label-sm' for='bool$id'>$title</label>
    </div>
    <input type = 'hidden' name='$id' id='$id' value = 'true'/>
    ";
}

function questionBoolInt($id, $title, $data, $cols = 3)
{
    $valueInt = isset($data) && isset($data[$id]) ? $data[$id] : 0;
    if ($valueInt > 0) $cheked = "checked";
    else $cheked = "";
    echo "
    <div class='form-group row'>
        <div class='col-sm-$cols'>
            <div class='form-check'>
                <input class='form-check-input' type='checkbox' name='chk$id' id='chk$id' value=true $cheked/>
                <label class='form-check-label col-form-label-sm' for='chk$id'>$title</label>
            </div>
        </div>
        <div class='col-auto'>
            <div class='row'>
                <div class='col-auto'>
                    <input class='form-control form-control-sm' name='$id' id='$id' value=$valueInt type='number' min='0' step='1' required/>
                </div>
                <label class='col-auto col-form-label col-form-label-sm' for='$id'>Cantidad</label>
            </div>
        </div>
    </div>
    ";
}

function questionInt($id, $title, $data, $cols = 3)
{
    $valueInt = isset($data) && isset($data[$id]) ? $data[$id] : 0;
    echo "
    <div class='form-group row'>
        <label class='col-sm-$cols col-form-label col-form-label-sm' for='$id'>$title</label>
        <div class='col-auto'>
            <input class='form-control form-control-sm' name='$id' id='$id' value=$valueInt type='number' min='0' step='1' required/>
        </div>
    </div>
    ";
}

function questionIntString($id, $title, $data, $cols = 3)
{
    $valueInt = isset($data) && isset($data[$id]) ? $data[$id] : 0;
    $valueString = isset($data) && isset($data["S" . $id]) ? $data["S" . $id] : "";
    echo "
    <div class='form-group row'>
            <label class='col-sm-$cols col-form-label col-form-label-sm' for='$id'>$title</label>
            <div class='col-2'>
                <input class='form-control form-control-sm' name='$id' id='$id' value=$valueInt type='number' min='0' step='1' required/>
            </div>
            <label class='col-sm-1 col-form-label col-form-label-sm' for='S$id'>Comentario</label>
            <div class='col'>
                <input class='form-control form-control-sm' name='S$id' id='S$id' value='$valueString' type='text' required/>
            </div>
    </div>
    ";
}

function questionRadio($id, $title, $radios, $data, $default, $cols = 3)
{
    $optionCheked = isset($data) && isset($data[$id]) ? $data[$id] : $default;
    echo "
    <div class='form-group row'>
        <label class='col-sm-$cols col-form-label col-form-label-sm'>$title</label>
        <div class='col-auto'>";
    foreach ($radios as $key => $value) {
        if ($key === $optionCheked) $cheked = "checked";
        else $cheked = "";
        echo "
        <div class='form-check form-check-inline'>
            <input class='form-check-input' type='radio' name='$id' id='inlineRadio$id$key' value='$key' $cheked>
            <label class='form-check-label' for='inlineRadio$id$key'>$value</label>
        </div>
        ";
    }
    echo "  
        </div>
    </div>
    ";
}

function questionRadioString($id, $title, $radios, $data, $default, $cols = 3)
{
    $optionCheked = isset($data) && isset($data[$id]) ? $data[$id] : $default;
    $valueString = isset($data) && isset($data["S" . $id]) ? $data["S" . $id] : "";
    echo "
    <div class='form-group row'>
        <label class='col-sm-$cols col-form-label col-form-label-sm'>$title</label>
        <div class='col-auto'>";
    foreach ($radios as $key => $value) {
        if ($key === $optionCheked) $cheked = "checked";
        else $cheked = "";
        echo "
        <div class='form-check form-check-inline'>
            <input class='form-check-input' type='radio' name='$id' id='inlineRadio$id$key' value='$key' $cheked>
            <label class='form-check-label' for='inlineRadio$id$key'>$value</label>
        </div>
        ";
    }
    echo "  
        </div>
        <label class='col-sm-1 col-form-label col-form-label-sm' for='S$id'>Comentario</label>
            <div class='col'>
                <input class='form-control form-control-sm' name='S$id' id='S$id' value='$valueString' type='text' required/>
            </div>
    </div>
    ";
}

function questionCombo($id, $title, $combos, $data, $default, $cols = 3)
{
    $optionCheked = isset($data) && isset($data[$id]) ? $data[$id] : $default;
    echo "
    <div class='form-group row'>
        <label for='combo$id' class='col-sm-$cols col-form-label col-form-label-sm'>$title</label>
        <div class='col-sm-auto'>
        <select class='form-control form-control-sm' name='$id' id='combo$id'>";
    foreach ($combos as $key => $value) {
        if ($key === $optionCheked) $cheked = "selected";
        else $cheked = "";
        echo "<option value='$key' $cheked>$value</option>";
    }
    echo "  
        </select>
        </div>
    </div>
    ";
}

function questionImagesFile($id, $title, $data, $fileext = 'image/png, image/jpeg', $cols = 9)
{
    $valueFileNames = isset($data) && isset($data[$id]) ? unserialize($data[$id]) : [];
    $colsLabel = 12 - $cols;
    $ejercicio = $GLOBALS["ejercicio"];
    $sucursalID = $GLOBALS["sucursalID"];
    $nomID = $GLOBALS["table"];
    $target_dir = "upload/{$ejercicio}/{$sucursalID}/{$nomID}/{$id}/";
    foreach ($valueFileNames as $fileName) {
        $src = $target_dir . $fileName;
        echo "<div class='form-group row'><img src='$src' class='img-fluid mx-auto d-block border border-primary' alt='$fileName'></div>";
    }
    echo "<div class='form-group row'>
            <label class='col-sm-$colsLabel text-right' for='{$id}[]'>$title</label>  
            <div class='col-sm-$cols'>
                <input type='file' name='{$id}[]' id='${id}[]' multiple class='form-control-file' accept='$fileext'>
            </div>
        </div>";
}

function questionTableDynamic($id, $title, $ths, $trs, $cols = 3)
{
    echo "
    <div class='row'>
        <label class='col-sm-$cols col-form-label col-form-label-sm'>$title</label>
        <div class='col-auto table-responsive'>
            <table class='table table-striped table-hover table-sm'>
                <thead>
                    <tr class='bg-primary text-light'>";
    foreach ($ths as $idh => $th) {
        echo "<th>$th[0]</th>";
    }
    echo "<th class='text-right'><button type='button' class='btn btn-success btn-sm' onclick=\"addRow$id('$id')\"><i class='fas fa-plus-circle'></i></button></th></tr>
                </thead>
                <tbody id='tbody$id'>";
    $types = array_values($ths);
    $keys = array_keys($ths);
    $i = 0;
    foreach ($trs as $tds) {
        echo "<tr>";
        $j = 0;
        foreach ($tds as $td) {
            echo "<td>";
            $idInput = $id . "-" . $keys[$j] . "[]";
            switch ($types[$j][1]) {
                case "text":
                    echo "<input class='form-control form-control-sm' name='$idInput' value='$td' type='text' required/>";
                    break;
                case "int":
                    echo "<input class='form-control form-control-sm' name='$idInput' value=$td type='number' min='0' step='1' required/>";
                    break;
                case "decimal":
                    echo "<input class='form-control form-control-sm' name='$idInput' value=$td type='number' min='0' step='0.01' required/>";
                    break;
                case "date":
                    echo "<input class='form-control form-control-sm' name='$idInput' value='$td' type='date' required/>";
                    break;
                case "select":
                    $selectItem = "<select class='form-control form-control-sm' name='$idInput'>";
                    foreach ($types[$j][2] as $k => $v) {
                        $selected = $k == $td ? "selected": "";
                        $selectItem .= "<option value='$k' $selected>$v</option>";
                    }
                    $selectItem .= "</select>";
                    echo $selectItem;
                    break;
            }
            echo "</td>";
            $j++;
        }
        $i++;
        echo "<td class='text-right'><a class='btn btn-outline-danger btn-sm' onclick='removeRow(this)'><i class='fas fa-minus-circle'></i></a></td></tr>";
    }
    echo "</tbody>
            </table>
        </div>
    </div>";
    echo "
    <script>
    function addRow$id(id) {
        var table = document.getElementById('tbody'+id);
        var row = table.insertRow();";
    $itr = 0;
    foreach ($ths as $idx => $th) {
        echo "
        var cell$itr = row.insertCell($itr);
        cell$itr.innerHTML = '";
        $idInput = $id . "-" . $keys[$itr] . "[]";
        switch ($types[$itr][1]) {
            case "text":
                echo "<input class=\"form-control form-control-sm\" name=\"$idInput\" type=\"text\" required/>";
                break;
            case "int":
                echo "<input class=\"form-control form-control-sm\" name=\"$idInput\" type=\"number\" min=\"0\" step=\"1\" required/>";
                break;
            case "decimal":
                echo "<input class=\"form-control form-control-sm\" name=\"$idInput\" type=\"number\" min=\"0\" step=\"0.01\" required/>";
                break;
            case "date":
                echo "<input class=\"form-control form-control-sm\" name=\"$idInput\" type=\"date\" required/>";
                break;
            case "select":
                $selectItem = "<select class=\"form-control form-control-sm\" name=\"$idInput\">";
                foreach ($types[$itr][2] as $k => $v) {
                    $selectItem .= "<option value=\"$k\">$v</option>";
                }
                $selectItem .= "</select>";
                echo $selectItem;
                break;
        }
        echo "'";
        $itr++;
    }
    echo "
    var cellDelete = row.insertCell($itr);cellDelete.className='text-right';cellDelete.innerHTML = '<a class=\"btn btn-outline-danger btn-sm\" onclick=\"removeRow(this)\"><i class=\"fas fa-minus-circle\"></i></a>';
    }
    function removeRow(btn){
        var row = btn.parentNode.parentNode;
        row.parentNode.removeChild(row);
    }
    </script>";
}

function uploadFiles($id, $files)
{
    $ejercicio = $GLOBALS["ejercicio"];
    $sucursalID = $GLOBALS["sucursalID"];
    $nomID = $GLOBALS["table"];
    $target_dir = "upload/{$ejercicio}/{$sucursalID}/{$nomID}/{$id}/";
    $status = 1;
    $message = "";
    $uploaded = [];

    if (!is_dir($target_dir)) {
        mkdir($target_dir, 0777, true);
        $scanned_directory = array_diff(scandir($target_dir), ['..', '.']);
    } else {
        $scanned_directory = array_diff(scandir($target_dir), ['..', '.']);
    }
    foreach ($files as $i => $fileArray) {
        $target_file = $target_dir . basename($fileArray["name"]);
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        $check = getimagesize($fileArray["tmp_name"]);
        if ($check == false) {
            $message = "El archivo no es una imagen";
            $status = 0;
            break;
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            $message = "El archivo ya existe, renombrelo";
            $status = 0;
            break;
        }
        // Check file size
        if ($fileArray["size"] > 5000000) {
            $message = "El archivo es muy grande, debe ser menor a 5 MB";
            $status = 0;
            break;
        }
        // Allow certain file formats
        if (
            $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif"
        ) {
            $message = "Solo se admiten formatos JPG, JPEG, PNG & GIF.";
            $status = 0;
            break;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($status == 1) {
            if (!move_uploaded_file($fileArray["tmp_name"], $target_file)) {
                $message = "ERROR AL CARGAR EL ARCHIVO";
                $status = 0;
                break;
            } else {
                array_push($uploaded, $fileArray["name"]);
            }
        }
    }
    if ($status == 0) { // checar si status es 0 borrar cualquier archivo subido en esta funcion
        //print("Borrar:<br><pre>".print_r($uploaded,true)."</pre>");
        foreach ($uploaded as $key => $value) {
            unlink($target_dir . basename($value));
        }
    } else { // caso contrario borrar todos los que no se subieron en esta funcion
        //print("Borrar:<br><pre>".print_r($scanned_directory,true)."</pre>");
        foreach ($scanned_directory as $key => $value) {
            unlink($target_dir . basename($value));
        }
    }
    return array($status, $message);
}
