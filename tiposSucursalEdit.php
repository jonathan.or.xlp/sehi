<?php
include('autentificacion.php');
require_once('config.php');

if (isset($_POST['saveNew'])) {
    $action = 'new';
    $tipoSucursal = $_POST["tipoSucursal"];

    $update = mysqli_query($link, "INSERT INTO tipossucursal (tipoSucursal) VALUES('$tipoSucursal')");
    $tipoSucursalID = mysqli_insert_id($link);
} elseif (isset($_POST['save'])) {
    $action = 'update';
    $tipoSucursalID = $_POST['tipoSucursalID'];
    $tipoSucursal = $_POST["tipoSucursal"];

    $update = mysqli_query($link, "UPDATE tipossucursal SET tipoSucursal='$tipoSucursal' WHERE tipoSucursalID=$tipoSucursalID");
}if (isset($_GET["tipoSucursalID"])) {
    $action='edit';
    $tipoSucursalID = $_GET["tipoSucursalID"];
    $sql = mysqli_query($link, "SELECT * FROM tipossucursal WHERE tipoSucursalID=$tipoSucursalID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: tiposSucursal.php");
    } else {
        $row = mysqli_fetch_assoc($sql);

        $tipoSucursalID = $row['tipoSucursalID'];
        $tipoSucursal = $row["tipoSucursal"];
    }
} else {
    //header("location: usuarios.php");
    $tipoSucursalID = 0;
    $action='new';
    $tipoSucursal = '';
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page linktent  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> <?php echo $action=='new'? 'Agregar Tipo de Sucursal':'Editar Tipo de Sucursal'; ?></h3>
                <div class="line"></div>
                <br/>
                <div class="">
                    <?php
                    if (isset($update) && $update) {
                        echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
                    } elseif (isset($update) && !$update) {
                        echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.<br>'.mysqli_error($link).'</div>';
                    }
                    ?>
                    <form action="" method="post" class="ml-4">
                        <div class="form-group row">
                            <label for="tipoSucursal" class="col-sm-1 col-form-label">Tipo de Sucursal</label>
                            <div class="col-sm-4">
                                <input type="text" name="tipoSucursal" value="<?php echo $tipoSucursal; ?>" class="form-control" placeholder="tipo de sucursal" required>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <input type="hidden" name="tipoSucursalID" value=<?php echo $tipoSucursalID; ?>>
                            <label class="col-sm-1">&nbsp;</label>
                            <div class="col-sm-4 text-right">
                                <a href="tiposSucursal.php" class="btn btn-sm btn-light">Regresar</a>
                                <input type="submit" name="<?php echo $action=='edit'? 'save' : 'saveNew';?>" class="btn btn-sm btn-secondary" value="Guardar">
                            </div>
                        </div>
                    </form>
                </div>


                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
</body>

</html>