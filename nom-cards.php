<div class="row justify-content-between">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-header">
                <h5>Sucursal</h5>
            </div>
            <div class="card-body">
                <h5 class="card-title"><?php echo $sucursal ?></h5>
                <h6 class="card-subtitle mb-2 text-muted"><?php echo $zona ?></h6>
                <h6 class="card-subtitle mb-2 text-muted"><?php echo $region ?></h6>
                <p class="card-text">Fecha programada: <?php echo $fecha ?></p>
            </div>
        </div>

    </div>
    <div class="col-sm-4">
        <div class="card">
            <div class="card-header">
                <h5>Certificados</h5>
            </div>
            <div class="card-body">
                <ul>
                    <li><?php echo $ejercicio - 1 ?></li>
                    <li>Certificado 2</li>
                    <li>Certificado 1</li>
                    <li>Certificado 2</li>
                    <li>Certificado 1</li>
                    <li>Certificado 2</li>
                </ul>
            </div>
        </div>
    </div>
</div>