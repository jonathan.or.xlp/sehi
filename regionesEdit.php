<?php
include('autentificacion.php');
require_once('config.php');

if (isset($_POST['saveNew'])) {
    $action = 'new';
    $region = $_POST["region"];

    $update = mysqli_query($link, "INSERT INTO regiones (region) VALUES('$region')");
    $regionID = mysqli_insert_id($link);
} elseif (isset($_POST['save'])) {
    $action = 'update';
    $regionID = $_POST['regionID'];
    $region = $_POST["region"];
    $password = $_POST["password"];

    $update = mysqli_query($link, "UPDATE regiones SET region='$region' WHERE regionID=$regionID");
}if (isset($_GET["regionID"])) {
    $action='edit';
    $regionID = $_GET["regionID"];
    $sql = mysqli_query($link, "SELECT * FROM regiones WHERE regionID=$regionID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: regiones.php");
    } else {
        $row = mysqli_fetch_assoc($sql);

        $regionID = $row['regionID'];
        $region = $row["region"];
    }
} else {
    //header("location: usuarios.php");
    $regionID = 0;
    $action='new';
    $region = '';
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page linktent  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> <?php echo $action=='new'? 'Agregar Region':'Editar Region'; ?></h3>
                <div class="line"></div>
                <br/>
                <div class="">
                    <?php
                    if (isset($update) && $update) {
                        echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
                    } elseif (isset($update) && !$update) {
                        echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.<br>'.mysqli_error($link).'</div>';
                    }
                    ?>
                    <form action="" method="post" class="ml-4">
                        <div class="form-group row">
                            <label for="region" class="col-sm-1 col-form-label">Region</label>
                            <div class="col-sm-4">
                                <input type="text" name="region" value="<?php echo $region; ?>" class="form-control" placeholder="region" required>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <input type="hidden" name="regionID" value=<?php echo $regionID; ?>>
                            <label class="col-sm-1">&nbsp;</label>
                            <div class="col-sm-4 text-right">
                                <a href="regiones.php" class="btn btn-sm btn-light">Regresar</a>
                                <input type="submit" name="<?php echo $action=='edit'? 'save' : 'saveNew';?>" class="btn btn-sm btn-secondary" value="Guardar">
                            </div>
                        </div>
                    </form>
                </div>


                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
</body>

</html>