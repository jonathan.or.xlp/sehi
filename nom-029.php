<?php
include('autentificacion.php');
setlocale(LC_ALL, "es_MX");
date_default_timezone_set("America/Mexico_City");

if (isset($_GET["sucursalID"])) {
    require_once('config.php');
    require_once('functionsDB.php');
    include_once('nom-helpers.php');

    $ejercicio = getEjercicioAbierto($link);
    $userID = $_SESSION["id"];

    $table = "nom029"; // nombre de la tabla principal de la norma
    $tableID = $table."ID"; // nombre de la columna ID de la norma, debrá seguir el estandar <nombreTabla><ID>
    $sucursalID = $_GET["sucursalID"];

    $sql = mysqli_query($link, "SELECT S.sucursal,Z.zona,R.region, A.* FROM asignaciones AS A INNER JOIN sucursales AS S ON A.sucursalID = S.sucursalID INNER JOIN zonas AS Z ON S.zonaID = Z.zonaID INNER JOIN regiones AS R ON Z.regionID = R.regionID WHERE ejercicioID = $ejercicio AND usuarioID = $userID AND A.sucursalID = $sucursalID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: index.php");
    } else {
        $row = mysqli_fetch_assoc($sql);

        $sucursal = $row["sucursal"];
        $zona = $row["zona"];
        $region = $row["region"];
        $fecha = $row["fecha"];
        $estatus = $row["estatus"];

        $exist = mysqli_query($link, "SELECT $tableID, sucursalID FROM $table WHERE ejercicioID = '$ejercicio' AND sucursalID = $sucursalID");

        if (mysqli_num_rows($exist) == 0) {
            // FALTA OBTENER DATOS DEL AÑO PASADO EN TABLA PRINCIPAL Y SECUNDARIAS
            // SI ES EL PRIMER AÑO SE INSERTA UN REGISTRO SOLO CON LOS CAMPOS PRINCIPALES
            $result = mysqli_query($link, "INSERT INTO $table (ejercicioID, sucursalID) VALUES ('$ejercicio',$sucursalID)");
            $nomID = mysqli_insert_id($link);
        }else{
            $row = mysqli_fetch_assoc($exist);
            $nomID = $row[$tableID];
        }
        $where = "WHERE $tableID = $nomID";
    }
}else{
    header("location: index.php");
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page Content  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-gavel" aria-hidden="true"></span> NOM-029-STPS-2011</h3>
                <h4>Mantenimiento de las instalaciones eléctricas</h4>
                <?php require('nom-cards.php'); ?>
                <div class="line"></div>
                <div class="accordion" id="accordionExample">
                    <?php
                    // Procesamos todas las secciones
                    $d1 = proccessFormSection("1");
                    $d2 = proccessFormSection("2");
                    $d3 = proccessFormSection("3");
                    $d4 = proccessFormSection("4");
                    $d5 = proccessFormSection("5");
                    $d6 = proccessFormSection("6");
                    $d7 = proccessFormSection("7");

                    // Obtenemos banderas de captura de secciones
                    $c1 = sectionCompleted("1");
                    $c2 = sectionCompleted("2");
                    $c3 = sectionCompleted("3");
                    $c4 = sectionCompleted("4");
                    $c5 = sectionCompleted("5");
                    $c6 = sectionCompleted("6");
                    $c7 = sectionCompleted("7");

                    //Combos
                    $siNo = ["S" => 'Sí', "N" => 'No'];
                    $listadoEquipos = ["Acometida" => "Acometida","Planta de emergencia" => "Planta de emergencia","Reemplazo de lámparas" => "Reemplazo de lámparas","Sistema de ventilación" => "Sistema de ventilación", "Tableros de distribución" => "Tableros de distribución", "Transformador" => "Transformador"];
                    $listadoEquiposHerramientas = ["Destornillador" => "Destornillador", "Pinzas para electricista" => "Pinzas para electricista", "Martillo" => "Martillo", "Flexómetro" => "Flexómetro", "Escalera" => "Escalera", "Multímetro" => "Multímetro", "Taladro" => "Taladro", "Juego de dados y maneral" => "Juego de dados y maneral"];

                    // obtenemos datos almacenados en BD
                    $data = getDataFromDB();
                    //print("<pre>".print_r($data,true)."</pre>");
                    
                    initSection("accordionExample", "1", "Previsiones para las instalaciones", $d1, $c1);
                        questionRadio("q1", "La indicación para que toda instalación eléctrica se considere que se encuentra energizada, mientras no se compruebe lo contrario con aparatos, equipos o instrumentos de medición destinados a tal efecto", $siNo, $data, "N", 10);
                        questionRadio("q2", "Utilizar el equipo de medición que se requiera para evaluar la presencia o ausencia de la energía eléctrica en equipos o instalaciones eléctricas a revisar", $siNo, $data, "N", 10);
                        questionRadio("q3", "Según aplique, colocar señalización, candados o cualquier otro dispositivo para garantizar que el circuito permanezca desenergizado cuando se le realizan actividades de mantenimiento.", $siNo, $data, "N", 10);
                        questionRadio("q4", "Antes de realizar actividades de mantenimiento, seguir las instrucciones para verificar que la puesta a tierra esté en condiciones de funcionamiento o bien se colocan las tierras temporales.", $siNo, $data, "N", 10);
                        questionRadio("q5", "Después de haber realizado los trabajos de mantenimiento, seguir las instrucciones para realizar una inspección en todo el circuito o red en el que se efectuaron los mantenimientos, con objeto de asegurarse que ha quedado libre de materiales, herramientas y personal. Al término de dicha inspección, ya se podrán retirar los candados, señales o cualquier otro dispositivo utilizado.", $siNo, $data, "N", 10);
                    endSection($sucursalID, "1");
                    initSection("accordionExample", "2", "Para el desarrollo de las actividades de mantenimiento a las instalaciones eléctricas se cuenta con", $d2, $c2);
                        questionRadio("q6", "El diagrama unifilar actualizado y al menos el cuadro general de cargas correspondientes a la zona donde se realizará el mantenimiento", $siNo, $data, "N", 10);
                        questionRadio("q7", "Las indicaciones para conseguir las autorizaciones por escrito que correspondan, donde se describa al menos la actividad a realizar, la hora de inicio, una estimación de la hora de conclusión, la persona que autorizó la entrada y la salida, el estado de la reparación (temporal o permanente) y la precisión de si se realizará el mantenimiento con la instalación eléctrica energizada o con las medidas de seguridad para desenergizarla", $siNo, $data, "N", 10);
                        questionRadio("q8", "Las instrucciones concretas sobre el trabajo a realizar", $siNo, $data, "N", 10);
                        questionRadio("q9", "Las indicaciones para identificar las instalaciones eléctricas que representen mayor peligro para los trabajadores encargados de brindar el mantenimiento", $siNo, $data, "N", 10);
                        questionRadio("q10", "Los procedimientos de seguridad que incluyan medidas de seguridad necesarias para impedir daños al personal expuesto y las acciones que se deben aplicar antes, durante y después en los equipos o áreas donde se realizarán las actividades de mantenimiento.", $siNo, $data, "N", 10);
                        questionRadio("q11", "Las indicaciones para la colocación de señales, avisos, candados, etiquetas de seguridad en las instalaciones eléctricas que estén en mantenimiento.", $siNo, $data, "N", 10);
                        questionRadio("q12", "Las distancias de seguridad que deben observarse cuando los dispositivos de protección abran con carga.", $siNo, $data, "N", 10);
                    endSection($sucursalID, "2");
                    initSection("accordionExample", "3", "  Herramientas, equipos, materiales de protección aislante y equipo de protección personal", $d3, $c3);
                        questionRadio("q13", "Deben ser entregados al trabajador junto con las instrucciones para su revisión o reemplazo, para verificar que están en condiciones de funcionamiento", $siNo, $data, "N", 10);
                        questionRadio("q14", "Deben contar con instrucciones al alcance de los trabajadores para que observen las adecuadas condiciones para su almacenamiento, transporte y mantenimiento, que garanticen su buen funcionamiento", $siNo, $data, "N", 10);
                        questionRadio("q15", "Se deben seleccionar de acuerdo a los voltajes de operación del circuito cuando se trabaje con líneas vivas", $siNo, $data, "N", 10);
                        questionRadio("q16", "Deben manipularse para realizar el mantenimiento en las instalaciones eléctricas energizadas o desenergizadas de acuerdo a las instrucciones de seguridad", $siNo, $data, "N", 10);
                    endSection($sucursalID, "3");
                    initSection("accordionExample", "4", "Listado de Equipos", $d4, $c4);   
                        questionTableDynamic("listadoequipos", "Listado de Equipos", [
                            "equipo" => ["Equipo", "select",$listadoEquipos],
                            "descripcion" => ["Descripción", "text"], 
                            "V" => ["V", "text"], 
                            "A" => ["A", "text"],
                            "kVA" => ["kVA","text"]], isset($data["DT"]["listadoequipos"]["rows"]) ? $data["DT"]["listadoequipos"]["rows"] : [], 10);
                    endSection($sucursalID, "4");
                    initSection("accordionExample", "5", "Características técnicas y de mantenimiento de equipos eléctricos", $d5, $c5);   
                        questionTableDynamic("caracequipelectricos", "Características técnicas y de mantenimiento de equipos eléctricos", [
                            "equipo" => ["Equipo", "select",$listadoEquipos],
                            "descripcion" => ["Descripción Equipo", "text"], 
                            "desctrabajo" => ["Descripción del Trabajo", "text"], 
                            "duracion" => ["Duración(min)", "int"],
                            "personas" => ["Personas","int"], 
                            "frecuencia" => ["Frecuencia","text"]], isset($data["DT"]["caracequipelectricos"]["rows"]) ? $data["DT"]["caracequipelectricos"]["rows"] : [], 10);
                    endSection($sucursalID, "5");
                    initSection("accordionExample", "6", "Equipo y Herramientas", $d6, $c6);   
                        questionTableDynamic("equipomantenimiento", "Equipo y Herramientas", [
                            "trabajo" => ["Trabajo", "text"], 
                            "equipo" => ["Equipo y Herramienta", "select", $listadoEquiposHerramientas],
                            "peso" => ["Peso aproximado (kg)","decimal"]], isset($data["DT"]["equipomantenimiento"]["rows"]) ? $data["DT"]["equipomantenimiento"]["rows"] : [], 10);
                    endSection($sucursalID, "6");
                    initSection("accordionExample", "7", "Características de seguridad de los equipos eléctricos y procedimientos", $d7, $c7);   
                        questionTableDynamic("seguridadequipos", "Características de seguridad de los equipos eléctricos y procedimientos", [
                            "equipo" => ["Equipo", "select",$listadoEquipos], 
                            "descripcion" => ["Descripción", "text"],
                            "protecciones" => ["Protecciones especiales","text"],
                            "procexistentes" => ["Procedimientos de seguridad existentes","text"], 
                            "procrequeridos" => ["Procedimientos de seguridad requeridos","text"]], isset($data["DT"]["seguridadequipos"]["rows"]) ? $data["DT"]["seguridadequipos"]["rows"] : [], 10);
                    endSection($sucursalID, "7");
                    ?>
                </div>
            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
    <?php require('nom-helpers-script.php') ?>
</body>

</html>