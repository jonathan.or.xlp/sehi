<?php include('autentificacion.php');
setlocale(LC_ALL, "es_MX");
date_default_timezone_set("America/Mexico_City");
require_once('config.php');
require_once('functionsDB.php');

if (isset($_POST['asignar'])) {
    $ejercicio = $_POST['ejercicio'];
    $sucursalID = $_POST['sucursalID'];
    $tecnico = $_POST['tecnico'];
    $fecha = $_POST['fecha'];
    $asignacionID = $_POST['asignacionID'];

    //print("<pre>".print_r($_POST,true)."</pre>");
    if($asignacionID == "0"){
        $insert = mysqli_query($link, "INSERT INTO asignaciones (ejercicioID, usuarioID, sucursalID, fecha) VALUES('$ejercicio',$tecnico,$sucursalID,'$fecha')");
        $asignacionID = mysqli_insert_id ($link);
    }else{
        $insert = mysqli_query($link, "UPDATE asignaciones SET ejercicioID = '{$ejercicio}', usuarioID = {$tecnico}, sucursalID = {$sucursalID}, fecha = '{$fecha}' WHERE asignacionID={$asignacionID}");
    }
    
    // ELIMINAR TODOS LOS CERTIFICADOS ASIGNADOS
    // $delete = mysqli_query($link,"DELETE FROM asignacionescertificados WHERE asignacionID={$asignacionID}");
    // if(isset($_POST['switch']) && is_array($_POST['switch'])){
    //     foreach($_POST['switch'] as $certificadoID){
    //         //print("<pre>".print_r($certificadoID,true)."</pre>");
    //         $insert2 = mysqli_query($link,"INSERT INTO asignacionescertificados(asignacionID, certificadoID) VALUES({$asignacionID},{$certificadoID})");
    //     }
    // }
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page Content  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> Asignar Técnico</h3>
                <div class="line"></div>
                <div class="">
                    <?php
                    $ejercicio = getEjercicioAbierto($link);
                    if (isset($insert) && $insert) {
                        echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
                    }
                    ?>
                </div>
                <div class="row">
                    <form class="form-inline col-10" method="get">
                        <div class="form-group">
                            <select name="filter" class="form-control" onchange="form.submit()">
                                <option value=0>Región</option>
                                <?php
                                $filter = (isset($_GET['filter']) ? strtolower($_GET['filter']) : NULL);
                                $data = mysqli_query($link, "SELECT * FROM regiones");
                                while ($row = mysqli_fetch_assoc($data)) { ?>
                                    <option value=<?php echo $row['regionID'];
                                                    if ($filter == $row['regionID']) echo ' selected'; ?>><?php echo $row['region']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="filter2" class="form-control" onchange="form.submit()">
                                <option value=0>Zona</option>
                                <?php
                                $filter2 = (isset($_GET['filter2']) ? strtolower($_GET['filter2']) : NULL);
                                $regionFilter = (isset($_GET['filter'])  && $_GET['filter'] > 0 ? ' WHERE regionID=' . $_GET['filter'] : '');
                                $data = mysqli_query($link, 'SELECT * FROM zonas' . $regionFilter);
                                while ($row = mysqli_fetch_assoc($data)) { ?>
                                    <option value=<?php echo $row['zonaID'];
                                                    if ($filter2 == $row['zonaID']) echo ' selected'; ?>><?php echo $row['zona']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="filter3" class="form-control" onchange="form.submit()">
                                <option value=0>Tipo de Sucursal</option>
                                <?php
                                $filter3 = (isset($_GET['filter3']) ? strtolower($_GET['filter3']) : NULL);
                                $data = mysqli_query($link, "SELECT * FROM tipossucursal");
                                while ($row = mysqli_fetch_assoc($data)) { ?>
                                    <option value=<?php echo $row['tipoSucursalID'];
                                                    if ($filter3 == $row['tipoSucursalID']) echo ' selected'; ?>><?php echo $row['tipoSucursal']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </form>
                    <div class="col text-right">

                    </div>
                </div>
                <div class="line"></div>

                <div class="">
                    <table class="table table-striped table-hover table-sm">
                        <thead>
                            <tr class="bg-primary text-light">
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">Clave</th>
                                <th>Sucursal</th>
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">Zona</th>
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">Región</th>
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">Tipo</th>
                                <th>Técnico</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            if ($filter || $filter2 || $filter3) {
                                $clauses = array();
                                if ($filter) $clauses[] = "R.regionID = $filter";
                                if ($filter2) $clauses[] = "Z.zonaID = $filter2";
                                if ($filter3) $clauses[] = "TS.tipoSucursalID = $filter3";
                                $where = !empty($clauses) ? ' WHERE ' . implode(' AND ', $clauses) : '';
                                $sql = mysqli_query($link, "SELECT S.sucursalID,S.clave,S.sucursal,Z.zona,R.region,TS.tipoSucursal FROM sucursales AS S JOIN zonas AS Z ON Z.zonaID= S.zonaID JOIN regiones AS R ON Z.regionID = R.regionID JOIN tipossucursal AS TS ON TS.tipoSucursalID = S.tipoSucursalID $where ORDER BY ABS(S.clave)");
                            } else {
                                $sql = mysqli_query($link, "SELECT S.sucursalID,S.clave,S.sucursal,Z.zona,R.region,TS.tipoSucursal FROM sucursales AS S JOIN zonas AS Z ON Z.zonaID= S.zonaID JOIN regiones AS R ON Z.regionID = R.regionID JOIN tipossucursal AS TS ON TS.tipoSucursalID = S.tipoSucursalID ORDER BY ABS(S.clave)");
                            }
                            if (mysqli_num_rows($sql) == 0) {
                                echo '<tr><td colspan="8">No hay datos.</td></tr>';
                            } else {
                                while ($row = mysqli_fetch_assoc($sql)) {
                                    $suc = $row['sucursalID'];
                                    $sql2 = mysqli_query($link, "SELECT asignacionID, usuarioID, usuario, fecha FROM asignaciones INNER JOIN usuarios USING (usuarioID) WHERE asignaciones.sucursalID = $suc AND ejercicioID = '$ejercicio'");
                                    $row2 = mysqli_fetch_assoc($sql2);
                                    echo '
                                            <tr>
                                                <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">' . $row['clave'] . '</td>
                                                <td>' . $row['sucursal'] . '</td>
                                                <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">' . $row['zona'] . '</td>
                                                <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">' . $row['region'] . '</td>
                                                <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell">' . $row['tipoSucursal'] . '</td>
                                                <td>';
                                    if (isset($row2)) {
                                        $asignacion = $row2['asignacionID'];
                                        $tecnico = $row2['usuarioID'];
                                        $fecha = $row2['fecha'];

                                        echo $row2['usuario'];
                                    } else {
                                        $asignacion = 0;
                                        $tecnico = 0;
                                        $fecha = '';
                                    }
                                    echo '</td>
                                                <td>
                                                    <a href="" title="Asignar" class="btn btn-outline-success btn-sm AsignarButton" data-toggle="modal" data-target="#exampleModal" data-asignacion="' . $asignacion . '" data-sucursal="' . $row['sucursal'] . '" data-sucursalid="' . $row['sucursalID'] . '" data-tecnico="' . $tecnico . '" data-fecha="' . $fecha . '"><span class="fas fa-wrench" aria-hidden="true"></span></a>
                                                </td>
                                            </tr>
                                            ';
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Asignar Técnico</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="" method="post">
                                    <div class="form-group row">
                                        <label for="ejercicio" class="col-sm-2 col-form-label">Ejercicio</label>
                                        <input type="text" class="col-sm-9 form-control" name="ejercicio" readonly value="<?php echo $ejercicio; ?>" />
                                    </div>
                                    <div class="form-group row">
                                        <label for="sucursal" class="col-sm-2 col-form-label">Sucursal</label>
                                        <input type="hidden" name="sucursalID" id="sucursalID" value="" />
                                        <input type="text" class="col-sm-9 form-control" name="sucursal" id="sucursal" readonly placeholder="" />
                                    </div>
                                    <div class="form-group row">
                                        <label for="tecnico" class=" col-sm-2 col-form-label">Técnico</label>
                                        <select class="col-sm-9 form-control" id="tecnico" name="tecnico" required>
                                            <?php
                                            $data = mysqli_query($link, "SELECT usuarioID, usuario FROM usuarios WHERE rolID=2");
                                            while ($row = mysqli_fetch_assoc($data)) {
                                                echo '<option value=' . $row['usuarioID'] . '>' . $row['usuario'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fecha" class="col-sm-2 col-form-label">Fecha</label>
                                        <input type="date" class="col-sm-9 form-control" id="fecha" name="fecha" required />
                                    </div>
                                    <div class="line"></div>

                                    <div class="form-group row">
                                        <label class="col-sm-2">&nbsp;</label>
                                        <div class="col-sm-9 text-right">
                                            <input type="hidden" name="asignacionID" id="asignacionID">
                                            <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Cancelar</button>
                                            <input type="submit" name="asignar" class="btn btn-sm btn-secondary" value="Asignar">
                                        </div>
                                    </div>


                                </form>
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
    <script type="text/javascript">
        Date.prototype.toDateInputValue = (function() {
            var local = new Date(this);
            local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
            return local.toJSON().slice(0, 10);
        });
        $(document).on("click", ".AsignarButton", function(e) {
            let asignacionID = $(this).data('asignacion')
            let sucursalID = $(this).data('sucursalid')
            let sucursal = $(this).data('sucursal')
            let tecnico = $(this).data('tecnico')
            let fecha = $(this).data('fecha')
            $("#sucursalID").attr('value', sucursalID);
            $("#sucursal").attr('placeholder', sucursal);
            if (tecnico != 0) {
                $("#tecnico").val(tecnico);
                $("#fecha").val(fecha);
            } else {
                $("#tecnico").prop("selectedIndex", -1);
                $("#fecha").val(new Date().toDateInputValue());
            }
            $("#asignacionID").val(asignacionID);
        });
    </script>
</body>
</html>