<?php include('autentificacion.php'); ?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page Content  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> Regiones</h3>
                <?php

                require_once('config.php'); //conexión a la base de datos con variable $link

                if (isset($_GET['aksi']) == 'delete') {
                    // escaping, additionally removing everything that could be (html/javascript-) code
                    $id = mysqli_real_escape_string($link, (strip_tags($_GET["id"], ENT_QUOTES)));
                    $data = mysqli_query($link, "SELECT * FROM regiones WHERE regionID='$id'");
                    if (mysqli_num_rows($data) == 0) {
                        echo '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> No se encontraron datos.</div>';
                    } else {
                        $delete = mysqli_query($link, "DELETE FROM regiones WHERE regionID='$id'");
                        if ($delete) {
                            echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Datos eliminado correctamente.</div>';
                        } else {
                            echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Error, no se pudo eliminar los datos.<br>'.mysqli_error($link).'</div>';
                        }
                    }
                }
                ?>
                <div class="row">
                    <div class="col text-right">
                        <a class="btn btn-secondary" href="regionesEdit.php">Nueva</a>
                    </div>
                </div>
                <div class="line"></div>

                <div class="">
                    <table class="table table-striped table-hover table-sm">
                        <thead>
                            <tr class="bg-primary text-light">
                                <th>Region</th>
                                <th class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell text-center">Zonas</th>
                                <th class="text-center">Sucursales</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $sql = mysqli_query($link, "SELECT R.regionID, R.region, COUNT(DISTINCT Z.zonaID) AS czonas, COUNT(S.sucursalID) AS csucursales FROM regiones AS R LEFT JOIN zonas AS Z ON R.regionID= Z.regionID left JOIN sucursales AS S ON Z.zonaID = S.zonaID GROUP BY R.regionID, R.region");
                            
                            if (mysqli_num_rows($sql) == 0) {
                                echo '<tr><td colspan="8">No hay datos.</td></tr>';
                            } else {
                                while ($row = mysqli_fetch_assoc($sql)) {
                                    echo '
                                            <tr>
                                                <td>'.$row['region'].'</td>
                                                <td class="d-none d-md-table-cell d-lg-table-cell d-xl-table-cell text-center">' . $row['czonas'] . '</td>
                                                <td class="text-center">' . $row['csucursales'] . '</td>
                                                <td>
                                                    <a href="regionesEdit.php?regionID=' . $row['regionID'] . '" title="Editar datos" class="btn btn-outline-success btn-sm"><span class="fa fa-edit" aria-hidden="true"></span></a>
                                                    <a href="" title="Eliminar" class="btn btn-outline-danger btn-sm deleteButton" data-toggle="modal" data-target="#exampleModal" data-region="' . $row['regionID'] . '"><span class="fa fa-trash" aria-hidden="true"></span></a>
                                                </td>
                                            </tr>
                                            ';
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Confirmar</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                ¿Está seguro que desea eliminar la region?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-light" data-dismiss="modal">Cancelar</button>
                                <a class='btn btn-outline-danger' href=''> Eliminar</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
    <script type="text/javascript">
        $(document).on("click", ".deleteButton", function(e) {

            //get data-id attribute of the clicked element
            var regionID = $(this).data('region')
            //populate the textbox
            $(".modal-footer a").attr('href', 'regiones.php?aksi=delete&id=' + regionID);
        });
    </script>
</body>

</html>