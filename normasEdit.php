<?php
include('autentificacion.php');
require_once('config.php');

if (isset($_POST['saveNew'])) {
    $action = 'new';
    $clave = $_POST["clave"];
    $descripcion = $_POST["descripcion"];
    $script = $_POST["script"];

    $update = mysqli_query($link, "INSERT INTO normas (clave, descripcion, script) VALUES('$clave','$descripcion','$script')");
    $normaID = mysqli_insert_id($link);
} elseif (isset($_POST['save'])) {
    $action = 'update';
    $normaID = $_POST['normaID'];
    $clave = $_POST["clave"];
    $descripcion = $_POST["descripcion"];
    $script = $_POST["script"];

    $update = mysqli_query($link, "UPDATE normas SET clave='$clave', descripcion='$descripcion', script='$script' WHERE normaID=$normaID");
}if (isset($_GET["normaID"])) {
    $action='edit';
    $normaID = $_GET["normaID"];
    $sql = mysqli_query($link, "SELECT * FROM normas WHERE normaID=$normaID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: normas.php");
    } else {
        $row = mysqli_fetch_assoc($sql);

        $normaID = $row['normaID'];
        $clave = $row["clave"];
        $descripcion = $row["descripcion"];
        $script = $row["script"];
    }
} else {
    //header("location: usuarios.php");
    $normaID = 0;
    $action='new';
    $clave = '';
    $descripcion = '';
    $script = '';
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page linktent  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> <?php echo $action=='new'? 'Agregar Norma':'Editar Norma'; ?></h3>
                <div class="line"></div>
                <br/>
                <div class="">
                    <?php
                    if (isset($update) && $update) {
                        echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
                    } elseif (isset($update) && !$update) {
                        echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.<br>'.mysqli_error($link).'</div>';
                    }
                    ?>
                    <form action="" method="post" class="ml-4">
                        <div class="form-group row">
                            <label for="clave" class="col-sm-1 col-form-label">Clave</label>
                            <div class="col-sm-4">
                                <input type="text" name="clave" value="<?php echo $clave; ?>" class="form-control" placeholder="NOM-000-XXXX" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="descripcion" class="col-sm-1 col-form-label">Descripción</label>
                            <div class="col-sm-4">
                                <input type="text" name="descripcion" value="<?php echo $descripcion; ?>" class="form-control" placeholder="Descripción" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="script" class="col-sm-1 col-form-label">Path</label>
                            <div class="col-sm-4">
                                <input type="text" name="script" value="<?php echo $script; ?>" class="form-control" placeholder="nom-000" required>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <input type="hidden" name="normaID" value=<?php echo $normaID; ?>>
                            <label class="col-sm-1">&nbsp;</label>
                            <div class="col-sm-4 text-right">
                                <a href="normas.php" class="btn btn-sm btn-light">Regresar</a>
                                <input type="submit" name="<?php echo $action=='edit'? 'save' : 'saveNew';?>" class="btn btn-sm btn-secondary" value="Guardar">
                            </div>
                        </div>
                    </form>
                </div>


                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
</body>

</html>