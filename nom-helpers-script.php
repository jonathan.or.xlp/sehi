<script type="text/javascript">
    $(document).ready(function() {
        //sections
        setTimeout(function() {
            $(".collapse.show").each(function() {
                $(this).prev(".card-header").find(".fa-plus").addClass("fa-minus").removeClass("fa-plus");
                var $card = $(this).closest('.card');
                var $open = $($(this).data('parent')).find('.collapse.show');
                var additionalOffset = 0;
                if ($card.prevAll().filter($open.closest('.card')).length !== 0) {
                    additionalOffset = $open.height();
                }
                $('html,body').animate({
                    scrollTop: $card.offset().top - additionalOffset - 80
                }, 200);
            })
        });
        $(".collapse").on('show.bs.collapse', function() {
            $(this).prev(".card-header").find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
            var $card = $(this).closest('.card');
            var $open = $($(this).data('parent')).find('.collapse.show');
            var additionalOffset = 0;
            if ($card.prevAll().filter($open.closest('.card')).length !== 0) {
                additionalOffset = $open.height();
            }
            $('html,body').animate({
                scrollTop: $card.offset().top - additionalOffset - 80
            }, 500);
        }).on('hide.bs.collapse', function() {
            $(this).prev(".card-header").find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
        });
        //questionBoolInt
        $("[id^='chk']").on('change', function() {
            var id = this.id.substring(3, this.id.length);
            if (this.checked) {
                $("#" + id).show();
                $("label[for='" + id + "']").show();
            } else {
                $("#" + id).hide();
                $("#" + id).val(0);
                $("label[for='" + id + "']").hide();
            }
        });
        $("[id^='chk']").change();
        //questionBool
        $("[id^='bool']").on('change', function() {
            var id = this.id.substring(4, this.id.length);
            if (this.checked) {
                $("#" + id).val('true');
            } else {
                $("#" + id).val('false');
            }
        });
        $("[id^='bool']").change();
    });
</script>