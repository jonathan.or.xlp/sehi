<?php
include('autentificacion.php');
require_once('config.php');

if (isset($_POST['saveNew'])) {
    $action = 'new';
    $usuario = $_POST["usuario"];
    $password = $_POST["password"];
    $nombreCompleto = $_POST["nombreCompleto"];
    $email = $_POST["email"];
    $telefono = $_POST["telefono"];
    $rolID = $_POST["rolID"];
    $sucursalID = isset($_POST['sucursalID'])?$_POST['sucursalID']:'NULL';
    //echo "INSERT INTO usuarios (usuario,pass,nombreCompleto,email,telefono,rolID,sucursalID) VALUES('$usuario', '$password', '$nombreCompleto', '$email', '$telefono', $rolID" . ($sucursalID == -1 ? ",NULL" : ", $sucursalID") . ")";
    $update = mysqli_query($link, "INSERT INTO usuarios (usuario,pass,nombreCompleto,email,telefono,rolID,sucursalID) VALUES('$usuario', '$password', '$nombreCompleto', '$email', '$telefono', $rolID" . ($sucursalID == -1 ? ",NULL" : ", $sucursalID") . ")");
    $usuarioID = mysqli_insert_id($link);
} elseif (isset($_POST['save'])) {
    $action = 'update';
    $usuarioID = $_POST['usuarioID'];
    $usuario = $_POST["usuario"];
    $password = $_POST["password"];
    $nombreCompleto = $_POST["nombreCompleto"];
    $email = $_POST["email"];
    $telefono = $_POST["telefono"];
    $rolID = $_POST["rolID"];
    $sucursalID = isset($_POST['sucursalID'])?$_POST['sucursalID']:'NULL';

    //echo "UPDATE usuarios SET usuario='$usuario', pass='$password', nombreCompleto='$nombreCompleto', email='$email', telefono='$telefono', rolID=$rolID, sucursalID=$sucursalID WHERE usuarioID=$usuarioID";
    $update = mysqli_query($link, "UPDATE usuarios SET usuario='$usuario', pass='$password', nombreCompleto='$nombreCompleto', email='$email', telefono='$telefono', rolID=$rolID, sucursalID=$sucursalID WHERE usuarioID=$usuarioID");
}
if (isset($_GET["usuarioID"])) {
    $action = 'edit';
    $usuarioID = $_GET["usuarioID"];
    $sql = mysqli_query($link, "SELECT * FROM usuarios WHERE usuarioID=$usuarioID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: usuarios.php");
    } else {
        $row = mysqli_fetch_assoc($sql);

        $usuarioID = $row['usuarioID'];
        $usuario = $row["usuario"];
        $password = $row["pass"];
        $nombreCompleto = $row["nombreCompleto"];
        $email = $row["email"];
        $telefono = $row["telefono"];
        $rolID = $row["rolID"];
        if (isset($row['sucursalID'])) {
            $sucursalID = $row['sucursalID'];
        } else {
            $sucursalID = NULL;
        }
    }
} else {
    //header("location: usuarios.php");
    $usuarioID = 0;
    $action = 'new';
    $usuario = '';
    $password = '';
    $nombreCompleto = '';
    $email = '';
    $telefono = '';
    $rolID = 2;
    $sucursalID = NULL;
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page linktent  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> <?php echo $action == 'new' ? 'Agregar Usuario' : 'Editar Usuario'; ?></h3>
                <div class="line"></div>
                <br />
                <div class="">
                    <?php
                    if (isset($update) && $update) {
                        echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
                    } elseif (isset($update) && !$update) {
                        echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.<br>' . mysqli_error($link) . '</div>';
                    }
                    ?>
                    <form action="" method="post" class="ml-4">
                        <div class="form-group row">
                            <label for="usuario" class="col-sm-1 col-form-label">Usuario</label>
                            <div class="col-sm-4">
                                <input type="text" name="usuario" value="<?php echo $usuario; ?>" class="form-control" placeholder="usuario" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-sm-1 col-form-label">Password</label>
                            <div class="col-sm-4">
                                <input type="text" name="password" value="<?php echo $password; ?>" class="form-control" placeholder="password" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nombreCompleto" class="col-sm-1 col-form-label">Nombre</label>
                            <div class="col-sm-4">
                                <input type="text" name="nombreCompleto" value="<?php echo $nombreCompleto; ?>" class="form-control" placeholder="nombre" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-1 col-form-label">E-mail</label>
                            <div class="col-sm-4">
                                <input type="email" name="email" value="<?php echo $email; ?>" class="form-control" placeholder="email@empresa.com">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="telefono" class="col-sm-1 col-form-label">Teléfono</label>
                            <div class="col-sm-4">
                                <input type="tel" name="telefono" pattern="[0-9]{10}?" value="<?php echo $telefono; ?>" class="form-control" placeholder="0000000000">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="rolID" class="col-sm-1 col-form-label">Rol</label>
                            <div class="col-sm-4">
                                <select name="rolID" id="rolID" class="form-control">
                                    <?php
                                    $data = mysqli_query($link, "SELECT * FROM roles");
                                    while ($row = mysqli_fetch_assoc($data)) { ?>
                                        <option value=<?php echo $row['rolID'];
                                                            if ($rolID == $row['rolID']) echo ' selected'; ?>><?php echo $row['rol']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row" id="tienda">
                            <label for="sucursalID" class="col-sm-1 col-form-label">Tienda</label>
                            <div class="col-sm-4">
                                <select name="sucursalID" id="selectTienda" class="form-control">
                                    <?php
                                    $data = mysqli_query($link, "SELECT * FROM sucursales");
                                    while ($row = mysqli_fetch_assoc($data)) { ?>
                                        <option value=<?php echo $row['sucursalID'];
                                                            if (isset($sucursalID) && $sucursalID == $row['sucursalID']) echo ' selected'; ?>><?php echo $row['sucursal']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <input type="hidden" name="usuarioID" value=<?php echo $usuarioID; ?>>
                            <label class="col-sm-1">&nbsp;</label>
                            <div class="col-sm-4 text-right">
                                <a href="usuarios.php" class="btn btn-sm btn-light">Regresar</a>
                                <input type="submit" name="<?php echo $action == 'edit' ? 'save' : 'saveNew'; ?>" class="btn btn-sm btn-secondary" value="Guardar">
                            </div>
                        </div>
                    </form>
                </div>


                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
    <script type="text/javascript">
        //<![CDATA[
        <?php if ($sucursalID == NULL) { ?>
            $("#selectTienda").prop("selectedIndex", -1);
        <?php } ?>

        $(function() {
            if ($('#rolID').val() == 3) {
                $('#tienda').show();
            } else {
                $('#tienda').hide();
            }
            $('#rolID').change(function() {
                if ($('#rolID').val() == 3) {
                    $('#tienda').show();
                } else {
                    $('#tienda').hide();
                }
            });
        });

        //]]>
    </script>
</body>

</html>