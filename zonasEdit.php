<?php
include('autentificacion.php');
require_once('config.php');

if (isset($_POST['saveNew'])) {
    $action = 'new';
    $zona = $_POST["zona"];
    $regionID = $_POST["regionID"];

    $update = mysqli_query($link, "INSERT INTO zonas (zona, regionID) VALUES('$zona', $regionID)");
    $zonaID = mysqli_insert_id($link);
} elseif (isset($_POST['save'])) {
    $action = 'update';
    $zonaID = $_POST['zonaID'];
    $zona = $_POST["zona"];
    $regionID = $_POST["regionID"];

    $update = mysqli_query($link, "UPDATE zonas SET zona='$zona', regionID=$regionID WHERE zonaID=$zonaID");
}if (isset($_GET["zonaID"])) {
    $action='edit';
    $zonaID = $_GET["zonaID"];
    $sql = mysqli_query($link, "SELECT * FROM zonas WHERE zonaID=$zonaID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: zonas.php");
    } else {
        $row = mysqli_fetch_assoc($sql);

        $zonaID = $row['zonaID'];
        $zona = $row["zona"];
        $regionID = $row["regionID"];
    }
} else {
    //header("location: usuarios.php");
    $zonaID = 0;
    $action='new';
    $zona = '';
    $regionID = 0;
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page linktent  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> <?php echo $action=='new'? 'Agregar Zona':'Editar Zona'; ?></h3>
                <div class="line"></div>
                <br/>
                <div class="">
                    <?php
                    if (isset($update) && $update) {
                        echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
                    } elseif (isset($update) && !$update) {
                        echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.<br>'.mysqli_error($link).'</div>';
                    }
                    ?>
                    <form action="" method="post" class="ml-4">
                        <div class="form-group row">
                            <label for="zona" class="col-sm-1 col-form-label">Zona</label>
                            <div class="col-sm-4">
                                <input type="text" name="zona" value="<?php echo $zona; ?>" class="form-control" placeholder="zona" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="regionID" class="col-sm-1 col-form-label">Region</label>
                            <div class="col-sm-4">
                                <select name="regionID" class="form-control">
                                    <?php
                                    $data = mysqli_query($link, "SELECT * FROM regiones");
                                    while ($row = mysqli_fetch_assoc($data)) { ?>
                                        <option value=<?php echo $row['regionID'];
                                                        if ($regionID == $row['regionID']) echo ' selected'; ?>><?php echo $row['region']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                        </div>
                        <div class="form-group row">
                            <input type="hidden" name="zonaID" value=<?php echo $zonaID; ?>>
                            <label class="col-sm-1">&nbsp;</label>
                            <div class="col-sm-4 text-right">
                                <a href="zonas.php" class="btn btn-sm btn-light">Regresar</a>
                                <input type="submit" name="<?php echo $action=='edit'? 'save' : 'saveNew';?>" class="btn btn-sm btn-secondary" value="Guardar">
                            </div>
                        </div>
                    </form>
                </div>


                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
</body>

</html>