<?php
include('autentificacion.php');
setlocale(LC_ALL, "es_MX");
date_default_timezone_set("America/Mexico_City");

if (isset($_GET["sucursalID"])) {
    require_once('config.php');
    require_once('functionsDB.php');
    include_once('nom-helpers.php');

    $ejercicio = getEjercicioAbierto($link);
    $userID = $_SESSION["id"];

    $table = "nom022"; // nombre de la tabla principal de la norma
    $tableID = $table."ID"; // nombre de la columna ID de la norma, debrá seguir el estandar <nombreTabla><ID>
    $sucursalID = $_GET["sucursalID"];

    $sql = mysqli_query($link, "SELECT S.sucursal,Z.zona,R.region, A.* FROM asignaciones AS A INNER JOIN sucursales AS S ON A.sucursalID = S.sucursalID INNER JOIN zonas AS Z ON S.zonaID = Z.zonaID INNER JOIN regiones AS R ON Z.regionID = R.regionID WHERE ejercicioID = $ejercicio AND usuarioID = $userID AND A.sucursalID = $sucursalID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: index.php");
    } else {
        $row = mysqli_fetch_assoc($sql);

        $sucursal = $row["sucursal"];
        $zona = $row["zona"];
        $region = $row["region"];
        $fecha = $row["fecha"];
        $estatus = $row["estatus"];

        $exist = mysqli_query($link, "SELECT $tableID, sucursalID FROM $table WHERE ejercicioID = '$ejercicio' AND sucursalID = $sucursalID");

        if (mysqli_num_rows($exist) == 0) {
            // FALTA OBTENER DATOS DEL AÑO PASADO EN TABLA PRINCIPAL Y SECUNDARIAS
            // SI ES EL PRIMER AÑO SE INSERTA UN REGISTRO SOLO CON LOS CAMPOS PRINCIPALES
            $result = mysqli_query($link, "INSERT INTO $table (ejercicioID, sucursalID) VALUES ('$ejercicio',$sucursalID)");
            $nomID = mysqli_insert_id($link);
        }else{
            $row = mysqli_fetch_assoc($exist);
            $nomID = $row[$tableID];
        }
        $where = "WHERE $tableID = $nomID";
    }
}else{
    header("location: index.php");
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page Content  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-gavel" aria-hidden="true"></span> NOM-022-STPS-2015</h3>
                <h4>Electricidad Estática en los Centros de Trabajo</h4>
                <?php require('nom-cards.php'); ?>
                <div class="line"></div>
                <div class="accordion" id="accordionExample">
                    <?php
                    // Procesamos todas las secciones
                    $d1 = proccessFormSection("1");
                    $d2 = proccessFormSection("2");
                    $d3 = proccessFormSection("3");

                    // Obtenemos banderas de captura de secciones
                    $c1 = sectionCompleted("1");
                    $c2 = sectionCompleted("2");
                    $c3 = sectionCompleted("3");

                    // obtenemos datos almacenados en BD
                    $data = getDataFromDB();
                    //print("<pre>".print_r($data,true)."</pre>");
                    
                    initSection("accordionExample", "1", "Imagen Satelital de la Tienda", $d1, $c1,"enctype='multipart/form-data'");    
                        questionImagesFile("imagenSatelital","Imagen Satelital de la Tienda",$data);
                    endSection($sucursalID, "1");
                    initSection("accordionExample", "2", "Pararrayos", $d2, $c2);   
                        questionTableDynamic("pararrayos", "Pararrayos", ["ubicacion" => ["Ubicación", "text"], "altura" => ["Altura del terreno", "decimal"], "angulo" => ["Ángulo de Protección", "int"], "estado" => ["Estado Físico", "select",["bueno" => "BUENO","malo" => "MALO"]],"observaciones" => ["Observaciones","text"]], isset($data["DT"]["pararrayos"]["rows"]) ? $data["DT"]["pararrayos"]["rows"] : []);
                    endSection($sucursalID, "2");
                    initSection("accordionExample", "3", "Medición de Resistencias", $d3, $c3);   
                        questionTableDynamic("resistencias", "Medición de Resistencias", 
                            ["electrodo" => ["Electrodo de tierra", "text"],
                             "temperatura" => ["Tempertura Medida en Suelo (°C)", "decimal"],
                             "humedad" => ["Humedad Relativa %", "decimal"],
                             "calibre" => ["Conductor Calibre (AWG)", "text"],
                             "corriente" => ["Corriente (A)", "decimal"],
                             "voltaje" => ["Voltaje (Volts)", "decimal"],
                             "vmedido" => ["Valor Medido (Ohm)", "decimal"],
                             "vestudio" => ["Valor Estudio (Ohm)", "decimal"]],
                              isset($data["DT"]["resistencias"]["rows"]) ? $data["DT"]["resistencias"]["rows"] : []);
                    endSection($sucursalID, "3");
                    ?>
                </div>
            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
    <?php require('nom-helpers-script.php') ?>
</body>

</html>