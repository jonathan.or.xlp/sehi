<?php
include('autentificacion.php');
require_once('config.php');
require_once('functionsDB.php');

if (isset($_POST['saveNew'])) {
    $ejercicioAbierto = getEjercicioAbierto($link);
    if ( $ejercicioAbierto == -1) {
        $action = 'new';
        $ejercicioID = $_POST["ejercicioID"];
        $estatus = "abierto";
        $fechaInicio = date("Y-m-d");


        $update = mysqli_query($link, "INSERT INTO ejercicios (ejercicioID, estatus, fechaInicio) VALUES('$ejercicioID','$estatus','$fechaInicio')");
    }else{
        $error = "Error: Existe un ejercicio abierto: " . $ejercicioAbierto;
        $ejercicioID = date("Y");
        $action = 'new';
    }
} else {
    $ejercicioID = date("Y");
    $action = 'new';
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page linktent  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-user" aria-hidden="true"></span> Aperturar Ejercicio</h3>
                <div class="line"></div>
                <br />
                <div class="">
                    <?php
                    if (isset($update) && $update) {
                        echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Los datos han sido guardados con éxito.</div>';
                    } elseif (isset($update) && !$update) {
                        echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.<br>' . mysqli_error($link) . '</div>';
                    }
                    if (isset($error)){
                        echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Error, no se pudo guardar los datos.<br>' . $error . '</div>';
                    }
                    ?>
                    <form action="" method="post" class="ml-4">
                        <div class="form-group row">
                            <label for="ejercicioID" class="col-sm-1 col-form-label">Ejercicio</label>
                            <div class="col-sm-4">
                                <input type="text" name="ejercicioID" value="<?php echo $ejercicioID; ?>" class="form-control" placeholder="año" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-1">&nbsp;</label>
                            <div class="col-sm-4 text-right">
                                <a href="ejercicios.php" class="btn btn-sm btn-light">Regresar</a>
                                <input type="submit" name="saveNew" class="btn btn-sm btn-secondary" value="Guardar">
                            </div>
                        </div>
                    </form>
                </div>


                <div class="line"></div>

            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
</body>

</html>