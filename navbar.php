<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
    <div class="container-fluid">
        <button type="button" id="sidebarCollapse" class="btn btn-outline-primary">
            <i class="fas fa-align-left"></i>
            <span>Menu</span>
        </button>
        <div class="nav-item float-left">
            <h2><span class="badge badge-primary">
                <?php
                    require_once('config.php');
                    require_once('functionsDB.php');
                    echo getEjercicioAbierto($link);
                ?>
            </span></h2>
        </div>
        <div class="nav-item dropdown float-rigth">
            <a class="btn btn-link btn-sm dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?php echo htmlspecialchars($_SESSION["fullname"]); ?> </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="myprofile.php"><?php echo(utf8_encode($_SESSION['username'])); ?></a>
                <!--<a class="dropdown-item" href="#">Bitácora</a>
                <div class="dropdown-divider"></div>-->
                <a class="dropdown-item" href="logout.php">Cerrar</a>
            </div>
        </div>
    </div>    
</nav>