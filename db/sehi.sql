-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-01-2021 a las 08:16:19
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sehi`
--
CREATE DATABASE IF NOT EXISTS `sehi` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sehi`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignaciones`
--

DROP TABLE IF EXISTS `asignaciones`;
CREATE TABLE `asignaciones` (
  `ejercicioID` varchar(4) NOT NULL,
  `usuarioID` int(11) NOT NULL,
  `sucursalID` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `estatus` varchar(30) NOT NULL DEFAULT 'PENDIENTE'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `asignaciones`
--

INSERT INTO `asignaciones` (`ejercicioID`, `usuarioID`, `sucursalID`, `fecha`, `estatus`) VALUES
('2018', 41, 33, '2019-12-21', 'COMPLETADA'),
('2018', 41, 117, '2019-11-30', 'PENDIENTE'),
('2018', 41, 120, '2020-01-29', 'EN PROGRESO'),
('2018', 41, 121, '2020-01-29', 'PENDIENTE'),
('2018', 41, 122, '2020-01-30', 'PENDIENTE'),
('2018', 41, 130, '2020-02-19', 'PENDIENTE'),
('2018', 41, 220, '2020-02-14', 'PENDIENTE'),
('2018', 42, 33, '2020-02-04', 'PENDIENTE'),
('2018', 43, 126, '2019-12-04', 'PENDIENTE'),
('2018', 44, 42, '2019-12-21', 'PENDIENTE'),
('2018', 44, 115, '2019-11-21', 'PENDIENTE'),
('2018', 44, 118, '2020-01-31', 'PENDIENTE'),
('2018', 45, 116, '2019-12-12', 'PENDIENTE'),
('2018', 45, 119, '2019-11-15', 'PENDIENTE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `certificados`
--

DROP TABLE IF EXISTS `certificados`;
CREATE TABLE `certificados` (
  `certificadoID` int(11) NOT NULL,
  `descripcion` varchar(400) NOT NULL,
  `fechainicial` date NOT NULL,
  `fechafinal` date NOT NULL,
  `file` varchar(255) NOT NULL,
  `fileEquipo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `certificados`
--

INSERT INTO `certificados` (`certificadoID`, `descripcion`, `fechainicial`, `fechafinal`, `file`, `fileEquipo`) VALUES
(18, 'equipo test', '2020-12-01', '2021-02-06', 'a:3:{i:0;s:11:\"buttons.png\";i:1;s:9:\"coffe.png\";i:2;s:14:\"headphones.png\";}', 'gps.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config`
--

DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `razonID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `config`
--

INSERT INTO `config` (`razonID`) VALUES
(1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ejercicios`
--

DROP TABLE IF EXISTS `ejercicios`;
CREATE TABLE `ejercicios` (
  `ejercicioID` varchar(4) NOT NULL,
  `estatus` varchar(30) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaTermino` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ejercicios`
--

INSERT INTO `ejercicios` (`ejercicioID`, `estatus`, `fechaInicio`, `fechaTermino`) VALUES
('2018', 'abierto', '2019-11-25', '2020-01-30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

DROP TABLE IF EXISTS `estados`;
CREATE TABLE `estados` (
  `estadoID` int(11) NOT NULL,
  `estado` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `levantamientos`
--

DROP TABLE IF EXISTS `levantamientos`;
CREATE TABLE `levantamientos` (
  `levantamientoID` int(11) NOT NULL,
  `ejercicioID` varchar(4) NOT NULL,
  `sucursalID` int(11) NOT NULL,
  `normaID` int(11) NOT NULL,
  `estatus` varchar(2) NOT NULL DEFAULT 'AP'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `levantamientos`
--

INSERT INTO `levantamientos` (`levantamientoID`, `ejercicioID`, `sucursalID`, `normaID`, `estatus`) VALUES
(1, '2018', 117, 1, 'AP'),
(2, '2018', 121, 1, 'AP'),
(3, '2018', 122, 1, 'AP'),
(4, '2018', 220, 1, 'AP'),
(5, '2018', 130, 1, 'AP'),
(6, '2018', 120, 1, 'AP'),
(7, '2018', 33, 1, 'AP'),
(8, '2018', 117, 11, 'AP'),
(9, '2018', 117, 4, 'AP'),
(10, '2018', 117, 7, 'AP'),
(11, '2018', 117, 10, 'AP'),
(12, '2018', 117, 12, 'AP');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

DROP TABLE IF EXISTS `municipios`;
CREATE TABLE `municipios` (
  `municipioID` int(11) NOT NULL,
  `municipio` varchar(100) NOT NULL,
  `estadoID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom002`
--

DROP TABLE IF EXISTS `nom002`;
CREATE TABLE `nom002` (
  `nom002ID` int(11) NOT NULL,
  `ejercicioID` varchar(4) NOT NULL DEFAULT '',
  `sucursalID` int(11) NOT NULL,
  `rociadores` int(11) DEFAULT 0,
  `alarmaIncendios` int(11) DEFAULT NULL,
  `extintoresPQS` int(11) DEFAULT NULL,
  `extintoresCO2` int(11) DEFAULT NULL,
  `extintoresK` int(11) DEFAULT NULL,
  `hidrantes` int(11) DEFAULT NULL,
  `litrosAguaIncendios` int(11) DEFAULT NULL,
  `detecStoresHumo` int(11) DEFAULT NULL,
  `brigadistas` int(11) DEFAULT NULL,
  `tipoGas` varchar(10) DEFAULT NULL,
  `maxGas` int(11) DEFAULT NULL,
  `maxGasolina` int(11) DEFAULT NULL,
  `maxDiesel` int(11) DEFAULT NULL,
  `maxAceiteHi` int(11) DEFAULT NULL,
  `maxKg` int(11) DEFAULT NULL,
  `q15` varchar(10) DEFAULT NULL,
  `q16` varchar(10) DEFAULT NULL,
  `q17` varchar(10) DEFAULT NULL,
  `q18` varchar(10) DEFAULT NULL,
  `q19` varchar(10) DEFAULT NULL,
  `q20` varchar(10) DEFAULT NULL,
  `q21` varchar(10) DEFAULT NULL,
  `q22` varchar(10) DEFAULT NULL,
  `q23` varchar(10) DEFAULT NULL,
  `q24` varchar(10) DEFAULT NULL,
  `q25` varchar(10) DEFAULT NULL,
  `q26` varchar(10) DEFAULT NULL,
  `q27` varchar(10) DEFAULT NULL,
  `q28` varchar(10) DEFAULT NULL,
  `q29` varchar(10) DEFAULT NULL,
  `q30` varchar(10) DEFAULT NULL,
  `q31` varchar(10) DEFAULT NULL,
  `q32` varchar(10) DEFAULT NULL,
  `q33` varchar(10) DEFAULT NULL,
  `q34` varchar(10) DEFAULT NULL,
  `q35` varchar(10) DEFAULT NULL,
  `q36` varchar(10) DEFAULT NULL,
  `q37` varchar(10) DEFAULT NULL,
  `q38` varchar(10) DEFAULT NULL,
  `q39` varchar(10) DEFAULT NULL,
  `q40` varchar(10) DEFAULT NULL,
  `q41` varchar(10) DEFAULT NULL,
  `q42` varchar(10) DEFAULT NULL,
  `q43` varchar(10) DEFAULT NULL,
  `q44` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom002`
--

INSERT INTO `nom002` (`nom002ID`, `ejercicioID`, `sucursalID`, `rociadores`, `alarmaIncendios`, `extintoresPQS`, `extintoresCO2`, `extintoresK`, `hidrantes`, `litrosAguaIncendios`, `detecStoresHumo`, `brigadistas`, `tipoGas`, `maxGas`, `maxGasolina`, `maxDiesel`, `maxAceiteHi`, `maxKg`, `q15`, `q16`, `q17`, `q18`, `q19`, `q20`, `q21`, `q22`, `q23`, `q24`, `q25`, `q26`, `q27`, `q28`, `q29`, `q30`, `q31`, `q32`, `q33`, `q34`, `q35`, `q36`, `q37`, `q38`, `q39`, `q40`, `q41`, `q42`, `q43`, `q44`) VALUES
(1, '2018', 117, 15, 30, 100, 100, 100, 100, 100, 0, 15, 'LP', 0, 0, 0, 0, 0, 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'S', 'N', 'S', 'N', 'S', 'N', 'S'),
(2, '2018', 121, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '2018', 122, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom002_distaceitehi`
--

DROP TABLE IF EXISTS `nom002_distaceitehi`;
CREATE TABLE `nom002_distaceitehi` (
  `nom002ID` int(11) NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Descripcion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom002_distaceitehi`
--

INSERT INTO `nom002_distaceitehi` (`nom002ID`, `Cantidad`, `Descripcion`) VALUES
(1, 32323, 'asdasdasdasd'),
(1, 2322, '2342342342');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom002_distdiesel`
--

DROP TABLE IF EXISTS `nom002_distdiesel`;
CREATE TABLE `nom002_distdiesel` (
  `nom002ID` int(11) NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Descripcion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom002_distdiesel`
--

INSERT INTO `nom002_distdiesel` (`nom002ID`, `Cantidad`, `Descripcion`) VALUES
(1, 14, 'lljkjklsdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom002_distgasolina`
--

DROP TABLE IF EXISTS `nom002_distgasolina`;
CREATE TABLE `nom002_distgasolina` (
  `nom002ID` int(11) NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Descripcion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom002_distgasolina`
--

INSERT INTO `nom002_distgasolina` (`nom002ID`, `Cantidad`, `Descripcion`) VALUES
(1, 45, 'asdasdasd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom002_distkg`
--

DROP TABLE IF EXISTS `nom002_distkg`;
CREATE TABLE `nom002_distkg` (
  `nom002ID` int(11) NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Descripcion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom002_distkg`
--

INSERT INTO `nom002_distkg` (`nom002ID`, `Cantidad`, `Descripcion`) VALUES
(1, 121212, 'sdfsdfsdfsdf'),
(1, 222, 'dsdfsdfsdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom002_tanquesgas`
--

DROP TABLE IF EXISTS `nom002_tanquesgas`;
CREATE TABLE `nom002_tanquesgas` (
  `nom002ID` int(11) NOT NULL,
  `Serial` varchar(50) NOT NULL,
  `Volumen` decimal(10,2) NOT NULL,
  `Fecha` date NOT NULL,
  `Ubicacion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom002_tanquesgas`
--

INSERT INTO `nom002_tanquesgas` (`nom002ID`, `Serial`, `Volumen`, `Fecha`, `Ubicacion`) VALUES
(1, 'asasasas', '1212.00', '2020-02-18', 'xxxxx'),
(1, 'sasas1212', '121212.00', '2020-02-03', 'ssdsdsd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom006`
--

DROP TABLE IF EXISTS `nom006`;
CREATE TABLE `nom006` (
  `nom006ID` int(11) NOT NULL,
  `ejercicioID` varchar(4) NOT NULL,
  `sucursalID` int(11) NOT NULL,
  `montacargas` varchar(10) DEFAULT NULL,
  `apiladorElectrico` varchar(10) DEFAULT NULL,
  `datosMontacargas` int(11) DEFAULT NULL,
  `SdatosMontacargas` varchar(50) DEFAULT NULL,
  `datosApilador` int(11) DEFAULT NULL,
  `SdatosApilador` varchar(50) DEFAULT NULL,
  `puestosAutorizados` int(11) DEFAULT NULL,
  `SpuestosAutorizados` varchar(50) DEFAULT NULL,
  `patines` int(11) DEFAULT NULL,
  `Spatines` varchar(50) DEFAULT NULL,
  `planas` int(11) DEFAULT NULL,
  `Splanas` varchar(50) DEFAULT NULL,
  `pesoMax` int(11) DEFAULT NULL,
  `SpesoMax` varchar(50) DEFAULT NULL,
  `alturaMax` int(11) DEFAULT NULL,
  `SalturaMax` varchar(50) DEFAULT NULL,
  `capacitacion` varchar(5) DEFAULT NULL,
  `Scapacitacion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom006`
--

INSERT INTO `nom006` (`nom006ID`, `ejercicioID`, `sucursalID`, `montacargas`, `apiladorElectrico`, `datosMontacargas`, `SdatosMontacargas`, `datosApilador`, `SdatosApilador`, `puestosAutorizados`, `SpuestosAutorizados`, `patines`, `Spatines`, `planas`, `Splanas`, `pesoMax`, `SpesoMax`, `alturaMax`, `SalturaMax`, `capacitacion`, `Scapacitacion`) VALUES
(1, '2018', 117, 'Si', 'Si', 1, 'x', 2, 'xx', 3, 'xxx', 4, 'xxxx', 5, 'xxxxx', 6, 'xxxxxx', 7, 'xxxxxxx', 'S', 'sxss');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom022`
--

DROP TABLE IF EXISTS `nom022`;
CREATE TABLE `nom022` (
  `nom022ID` int(11) NOT NULL,
  `ejercicioID` varchar(4) NOT NULL,
  `sucursalID` int(11) NOT NULL,
  `imagenSatelital` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom022`
--

INSERT INTO `nom022` (`nom022ID`, `ejercicioID`, `sucursalID`, `imagenSatelital`) VALUES
(2, '2018', 117, 'a:1:{i:0;s:44:\"WhatsApp Image 2020-11-14 at 2.37.30 PM.jpeg\";}');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom022_pararrayos`
--

DROP TABLE IF EXISTS `nom022_pararrayos`;
CREATE TABLE `nom022_pararrayos` (
  `nom022ID` int(11) NOT NULL,
  `ubicacion` varchar(100) NOT NULL,
  `altura` decimal(10,2) NOT NULL,
  `angulo` int(11) NOT NULL,
  `estado` varchar(10) NOT NULL,
  `observaciones` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom022_pararrayos`
--

INSERT INTO `nom022_pararrayos` (`nom022ID`, `ubicacion`, `altura`, `angulo`, `estado`, `observaciones`) VALUES
(2, 'AZOTEA DE PISO DE VENTAS', '18.80', 45, 'bueno', 'SE ENCUENTRA EN BUEN ESTADO'),
(2, 'ASDASDASDAS', '12222.00', 33, 'bueno', 'SDSDASDASD');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom022_resistencias`
--

DROP TABLE IF EXISTS `nom022_resistencias`;
CREATE TABLE `nom022_resistencias` (
  `nom022ID` int(11) NOT NULL,
  `electrodo` varchar(100) NOT NULL,
  `temperatura` decimal(10,2) NOT NULL,
  `humedad` decimal(10,2) NOT NULL,
  `calibre` varchar(100) NOT NULL,
  `corriente` decimal(10,2) NOT NULL,
  `voltaje` decimal(10,2) NOT NULL,
  `vmedido` decimal(10,2) NOT NULL,
  `vestudio` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom022_resistencias`
--

INSERT INTO `nom022_resistencias` (`nom022ID`, `electrodo`, `temperatura`, `humedad`, `calibre`, `corriente`, `voltaje`, `vmedido`, `vestudio`) VALUES
(2, 'PLANTA DE EMERGENCIA', '33.30', '60.30', '0/1', '0.77', '0.00', '0.25', '0.25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom029`
--

DROP TABLE IF EXISTS `nom029`;
CREATE TABLE `nom029` (
  `nom029ID` int(11) NOT NULL,
  `ejercicioID` varchar(4) NOT NULL,
  `sucursalID` int(11) NOT NULL,
  `q1` varchar(10) DEFAULT NULL,
  `q2` varchar(10) DEFAULT NULL,
  `q3` varchar(10) DEFAULT NULL,
  `q4` varchar(10) DEFAULT NULL,
  `q5` varchar(10) DEFAULT NULL,
  `q6` varchar(10) DEFAULT NULL,
  `q7` varchar(10) DEFAULT NULL,
  `q8` varchar(10) DEFAULT NULL,
  `q9` varchar(10) DEFAULT NULL,
  `q10` varchar(10) DEFAULT NULL,
  `q11` varchar(10) DEFAULT NULL,
  `q12` varchar(10) DEFAULT NULL,
  `q13` varchar(10) DEFAULT NULL,
  `q14` varchar(10) DEFAULT NULL,
  `q15` varchar(10) DEFAULT NULL,
  `q16` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom029`
--

INSERT INTO `nom029` (`nom029ID`, `ejercicioID`, `sucursalID`, `q1`, `q2`, `q3`, `q4`, `q5`, `q6`, `q7`, `q8`, `q9`, `q10`, `q11`, `q12`, `q13`, `q14`, `q15`, `q16`) VALUES
(1, '2018', 117, 'S', 'S', 'S', 'S', 'S', 'N', 'S', 'N', 'S', 'N', 'S', 'N', 'S', 'N', 'S', 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom029_caracequipelectricos`
--

DROP TABLE IF EXISTS `nom029_caracequipelectricos`;
CREATE TABLE `nom029_caracequipelectricos` (
  `nom029ID` int(11) NOT NULL,
  `equipo` varchar(100) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `desctrabajo` varchar(150) NOT NULL,
  `duracion` int(11) NOT NULL,
  `personas` int(11) NOT NULL,
  `frecuencia` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom029_caracequipelectricos`
--

INSERT INTO `nom029_caracequipelectricos` (`nom029ID`, `equipo`, `descripcion`, `desctrabajo`, `duracion`, `personas`, `frecuencia`) VALUES
(1, 'Acometida', 'hfghfgh', 'wwwww', 12, 3, '3 años');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom029_equipomantenimiento`
--

DROP TABLE IF EXISTS `nom029_equipomantenimiento`;
CREATE TABLE `nom029_equipomantenimiento` (
  `nom029ID` int(11) NOT NULL,
  `trabajo` varchar(100) NOT NULL,
  `equipo` varchar(100) NOT NULL,
  `peso` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom029_equipomantenimiento`
--

INSERT INTO `nom029_equipomantenimiento` (`nom029ID`, `trabajo`, `equipo`, `peso`) VALUES
(1, 'Mantenimiento  menor (tableros, fusibles  y lámparas)', 'Juego de dados y maneral', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom029_listadoequipos`
--

DROP TABLE IF EXISTS `nom029_listadoequipos`;
CREATE TABLE `nom029_listadoequipos` (
  `nom029ID` int(11) NOT NULL,
  `equipo` varchar(100) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `V` varchar(30) NOT NULL,
  `A` varchar(30) NOT NULL,
  `kVA` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom029_listadoequipos`
--

INSERT INTO `nom029_listadoequipos` (`nom029ID`, `equipo`, `descripcion`, `V`, `A`, `kVA`) VALUES
(1, 'Planta de emergencia', 'fsfsdfsdf', 'sdf', 'sdf', 'sdf'),
(1, 'Acometida', 'fghfgh', 'sss', 'ss', 'ss');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom029_seguridadequipos`
--

DROP TABLE IF EXISTS `nom029_seguridadequipos`;
CREATE TABLE `nom029_seguridadequipos` (
  `nom029ID` int(11) NOT NULL,
  `equipo` varchar(100) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `protecciones` varchar(200) NOT NULL,
  `procexistentes` varchar(150) NOT NULL,
  `procrequeridos` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom029_seguridadequipos`
--

INSERT INTO `nom029_seguridadequipos` (`nom029ID`, `equipo`, `descripcion`, `protecciones`, `procexistentes`, `procrequeridos`) VALUES
(1, 'Planta de emergencia', 'sdsdsd', 'sdsdsd', 'sdsdsd', 'sdsdsd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom030`
--

DROP TABLE IF EXISTS `nom030`;
CREATE TABLE `nom030` (
  `nom030ID` int(11) NOT NULL,
  `ejercicioID` varchar(4) NOT NULL,
  `sucursalID` int(11) NOT NULL,
  `q1` varchar(10) DEFAULT NULL,
  `q2` varchar(10) DEFAULT NULL,
  `q3` varchar(10) DEFAULT NULL,
  `q4` varchar(10) DEFAULT NULL,
  `q5` varchar(10) DEFAULT NULL,
  `q6` varchar(10) DEFAULT NULL,
  `q7` varchar(10) DEFAULT NULL,
  `q8` varchar(10) DEFAULT NULL,
  `q9` varchar(10) DEFAULT NULL,
  `q10` varchar(10) DEFAULT NULL,
  `q11` varchar(10) DEFAULT NULL,
  `q12` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom030`
--

INSERT INTO `nom030` (`nom030ID`, `ejercicioID`, `sucursalID`, `q1`, `q2`, `q3`, `q4`, `q5`, `q6`, `q7`, `q8`, `q9`, `q10`, `q11`, `q12`) VALUES
(1, '2018', 117, 'S', 'S', 'S', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA', 'NA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom033`
--

DROP TABLE IF EXISTS `nom033`;
CREATE TABLE `nom033` (
  `nom033ID` int(11) NOT NULL,
  `ejercicioID` varchar(4) NOT NULL,
  `sucursalID` int(11) NOT NULL,
  `imagenEspaciosConfinados` varchar(500) DEFAULT NULL,
  `tiene_espacio_confinado` varchar(10) DEFAULT NULL,
  `espacio_confinado` varchar(100) DEFAULT NULL,
  `tipo_espacio_confinado` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom033`
--

INSERT INTO `nom033` (`nom033ID`, `ejercicioID`, `sucursalID`, `imagenEspaciosConfinados`, `tiene_espacio_confinado`, `espacio_confinado`, `tipo_espacio_confinado`) VALUES
(1, '2018', 117, 'a:1:{i:0;s:36:\"0cd33fe1810560d0f21f5e8b85765b83.jpg\";}', 'S', 'Instalación subterránea con limitación de oxígeno', 'NA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nom_section`
--

DROP TABLE IF EXISTS `nom_section`;
CREATE TABLE `nom_section` (
  `nomName` varchar(30) NOT NULL,
  `nomID` int(11) NOT NULL,
  `seccion` varchar(30) NOT NULL,
  `capturada` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nom_section`
--

INSERT INTO `nom_section` (`nomName`, `nomID`, `seccion`, `capturada`) VALUES
('nom002', 1, '1', 1),
('nom002', 1, '2', 1),
('nom002', 1, '5', 1),
('nom030', 1, '1', 1),
('nom006', 1, '2', 1),
('nom006', 1, '1', 1),
('nom006', 1, '3', 1),
('nom022', 1, '1', 1),
('nom002', 1, '3', 1),
('nom022', 2, '1', 1),
('nom022', 2, '2', 1),
('nom022', 2, '3', 1),
('nom029', 1, '1', 1),
('nom029', 1, '2', 1),
('nom029', 1, '3', 1),
('nom029', 1, '4', 1),
('nom029', 1, '5', 1),
('nom029', 1, '6', 1),
('nom029', 1, '7', 1),
('nom033', 1, '1', 1),
('nom033', 1, '2', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `normas`
--

DROP TABLE IF EXISTS `normas`;
CREATE TABLE `normas` (
  `normaID` int(11) NOT NULL,
  `clave` varchar(50) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `script` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `normas`
--

INSERT INTO `normas` (`normaID`, `clave`, `descripcion`, `script`) VALUES
(1, 'NOM-002-STPS-2010', 'Prevención y protección contra incendios', 'nom-002'),
(2, 'NOM-004-STPS-1999', 'Sistemas de protección y dispositivos de seguridad maquinaria', 'nom-004'),
(3, 'NOM-005-STPS-1998', 'Manejo, transporte y almacenamiento de sustancias químicas peligrosas', 'nom-005'),
(4, 'NOM-006-STPS-2014', 'Manejo y almacenamiento de materiales', 'nom-006'),
(5, 'NOM-015-STPS-2001', 'Condiciones térmicas elevadas o abatidas', 'nom-015'),
(6, 'NOM-017-STPS-2008', 'Equipo de protección personal', 'nom-017'),
(7, 'NOM-022-STPS-2015', 'Electricidad estática en los centros de trabajo', 'nom-022'),
(8, 'NOM-025-STPS-2008', 'Condiciones de iluminación', 'nom-025'),
(9, 'NOM-027-STPS-2008', 'Actividades de soldadura y corte', 'nom-027'),
(10, 'NOM-029-STPS-2011', 'Mantenimiento de las instalaciones eléctricas', 'nom-029'),
(11, 'NOM-030-STPS-2009', 'Servicios preventivos de seguridad y salud en el trabajo-Funciones y actividades', 'nom-030'),
(12, 'NOM-033-STPS-2015', 'Espacios Confinados', 'nom-033'),
(13, 'NOM-034-STPS-2016', 'Condiciones de seguridad para el acceso y desarrollo de actividades de trabajadores con discapacidad', 'nom-034'),
(14, 'NOM-035-STPS-2018', 'Factores de riesgo psicosocial en el trabajo', 'nom-035'),
(15, 'NOM-004-HERRAMIENTAS', 'Programa de herramientas ', 'nom-004-h'),
(16, 'NOM-004-MAQUINARIA', 'Programa de Maquinaria ', 'nom-004-m'),
(17, 'NOM-005-ESPECIFICO', 'Programa específico', 'nom-005-e');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `razonessociales`
--

DROP TABLE IF EXISTS `razonessociales`;
CREATE TABLE `razonessociales` (
  `razonID` int(11) NOT NULL,
  `razon` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `razonessociales`
--

INSERT INTO `razonessociales` (`razonID`, `razon`) VALUES
(4, 'Administración de Personal Encinal, S.A. de C.V.'),
(2, 'Crystal Ejecutivo, S.A. de C.V.'),
(3, 'Operadora de Negocios Crucero, S.A. de C.V.'),
(5, 'Supervisión y Mantenimiento de Inmuebles, S.A. de C.V.'),
(1, 'Tiendas Chedraui, S.A. de C.V.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regiones`
--

DROP TABLE IF EXISTS `regiones`;
CREATE TABLE `regiones` (
  `regionID` int(11) NOT NULL,
  `region` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `regiones`
--

INSERT INTO `regiones` (`regionID`, `region`) VALUES
(1, 'REGIÓN BAJIO'),
(2, 'REGIÓN METROPOLITANA'),
(3, 'REGIÓN ORIENTE'),
(4, 'REGIÓN PACIFICO'),
(5, 'REGIÓN SUR');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `rolID` int(11) NOT NULL,
  `rol` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`rolID`, `rol`) VALUES
(1, 'Administrador'),
(2, 'Técnico'),
(3, 'Tienda');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursales`
--

DROP TABLE IF EXISTS `sucursales`;
CREATE TABLE `sucursales` (
  `sucursalID` int(11) NOT NULL,
  `sucursal` varchar(100) NOT NULL,
  `clave` varchar(10) NOT NULL,
  `zonaID` int(11) NOT NULL,
  `tipoSucursalID` int(11) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `cp` varchar(5) NOT NULL,
  `colonia` varchar(200) NOT NULL,
  `municipio` varchar(100) NOT NULL,
  `estado` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sucursales`
--

INSERT INTO `sucursales` (`sucursalID`, `sucursal`, `clave`, `zonaID`, `tipoSucursalID`, `direccion`, `cp`, `colonia`, `municipio`, `estado`) VALUES
(1, 'LEON TORRES', '55', 1, 1, 'BLVD.TORRES LANDA ESQ.A.VELAZQ', '37450', 'SAN MIGUEL', 'LEON', 'GTO'),
(2, 'MORELIA CENTRO', '56', 2, 1, 'HEROES DE NOCUPETARO NUM. 201', '58000', 'CENTRO', 'MORELIA', 'MICH'),
(3, 'QUERETARO CANDILES', '95', 2, 1, 'AVENIDA PROLONG.CANDILES #204', '76900', 'FRACC.CAMINO REAL RESIDENCIAL', 'QUERETARO', 'QRO'),
(4, 'SAN JUAN DEL RIO CENTRO', '96', 2, 1, 'PINO SUAREZ NORTE NO.64', '76800', 'CENTRO', 'SAN JUAN DEL RIO', 'QRO'),
(5, 'DURANGO NAZAS', '103', 1, 1, 'NAZAS LOTE 33 #612', '34269', 'FRACCIONAMIENTO VERGEL DEL DESIERTO', 'DURANGO', 'DGO'),
(6, 'GUADALAJARA VALLARTA', '108', 3, 1, 'AV. VALLARTA NO. 5556', '45029', 'LOMAS UNIVERSIDAD', 'ZAPOPAN', 'JAL'),
(7, 'GUADALAJARA FEDERALISMO', '114', 3, 1, 'PROL. FEDERALISMO NORTE NO. 1786', '45180', 'QUINTA DEL FEDERALISMO', 'ZAPOPAN', 'JAL'),
(8, 'TLAJOMULCO', '119', 3, 1, 'CAMINO UNION DEL CUATRO PARCELA 17Z1P 1/2', '45654', 'LA UNION DEL CUATRO', 'TLAJOMULCO DE ZU??IGA', 'JAL'),
(9, 'FRESNILLO PLATEROS', '123', 1, 1, 'CARRETERA  A DURANGO S/N', '99020', 'TECNOL??GICA', 'FRESNILLO', 'ZAC'),
(10, 'TEPIC NAYARIT', '124', 3, 1, 'AV. DE LOS INSURGENTES NO. 2179', '63176', 'DIAZ ORDAZ', 'TEPIC', 'NAY'),
(11, 'DURANGO EL REFUGIO', '125', 1, 1, 'BLVD. DOMINGO ARRIETA NO. 601', '34170', 'EL REFUGIO', 'VICTORIA DE DURANGO', 'DGO'),
(12, 'NUEVO VALLARTA', '143', 3, 1, 'CARR. MEZCALES-PUERTO VALLARTA NO.1487', '63735', 'VALLE DORADO', 'BAHIA DE BANDERAS', 'NAY'),
(13, 'LEON SAN JUAN BOSCO', '149', 1, 1, 'PROL. BLVD. SAN JUAN BOSCO NO. 2109', '37407', 'RANCHO CRUZ DE LAS HILAMAS', 'LEON', 'GTO'),
(14, 'SOLEDAD DE GRACIANO SLP', '155', 1, 1, 'CARRETERA FEDERAL 57 TRAMO S.L.P MATEHUALA KM 4', '78433', 'LA RAZA', 'SOLEDAD DE GRACIANO', 'SLP'),
(15, 'GUADALAJARA COLON', '159', 3, 1, 'AV. COLON NO. 4856', '45608', 'CERRO DEL TESORO', 'TLAQUEPAQUE', 'JAL'),
(16, 'MORELIA LA HUERTA', '160', 2, 1, 'CALZADA LA HUERTA NO. 2555', '58080', 'EX HACIENDA LA HUERTA', 'MORELIA', 'MICH'),
(17, 'BUCERIAS', '176', 3, 1, 'CARR. MEZCALES-PUERTO VALLARTA NO.1487', '63732', 'VALLE DORADO', 'BAHIA DE BANDERAS', 'NAY'),
(18, 'PASEO QUERETARO', '185', 2, 1, 'ANILLO VIAL FRAY JUNIPERO SERRA NO. 7901', '76146', 'COLONIA LA PURISIMA DELEGACION EPIGMENIO GONZALEZ', 'QUERETARO', 'QRO'),
(19, 'JURIQUILLA', '186', 2, 1, 'SANTA FE', '76230', 'JURIQUILLA', 'QUERETARO', 'QRO'),
(20, 'QUERETARO CENTRO SUR', '244', 2, 1, 'AV.BERNARDO QUINTANA SUR 502', '76090', 'CENTRO SUR', 'QUERETARO', 'QRO'),
(21, 'QUERETARO BERNARDO QUINTANA', '245', 2, 1, 'BLVD.BERNANDO QUINTANA 4032', '76130', 'SAN PABLO', 'QUERETARO', 'QRO'),
(22, 'LEON CAMPESTRE', '246', 1, 1, 'BLVD.CAMPRESTRE NO. 694 ESQ. ANTONIO MADRAZO', '37230', 'HACIENDA DE LAS TROJES', 'LEON', 'GTO'),
(23, 'LEON POLIFORUM', '247', 1, 1, 'BLVD. FRANCISCO VILLA NO. 105', '37526', 'EL TLACUACHE', 'LEON', 'GTO'),
(24, 'GUADALAJARA LOMAS TONALA', '248', 3, 1, 'AV. RIO NILO # 7540', '45417', 'ORIENTE', 'GUADALAJARA', 'JAL'),
(25, 'GUADALAJARA ACUEDUCTO', '249', 3, 1, 'AV.ACUEDUCTO NO. 6050', '45118', 'COL. LOMAS DEL BOSQUE', 'GUADALAJARA', 'JAL'),
(26, 'AGUASCALIENTES ASUNCI??N', '250', 1, 1, 'BOULEVARD JOSE MARIA CHAVEZ # 1809 O VALENTE QUINTA AV.', '20280', 'PREDIOS DE VILLA ASUNCION', 'AGUASCALIENTES', 'AGS'),
(27, 'SAN LUIS POTOSI EL DORADO', '251', 1, 1, 'AV.NEREO RODRIGUEZ BARRAGAN450', '78200', 'DEL VALLE', 'SAN LUIS POTOSI', 'SLP'),
(28, 'SAN LUIS DE LA PAZ', '643', 2, 2, 'CALLE 5 DE MAYO No.103', '37900', 'CENTRO', 'SAN LUIS DE LA PAZ', 'GTO'),
(29, 'MEXICO ATIZAPAN', '18', 4, 1, 'AV SAN MATEO ESQ COMONFORT 17', '52900', 'CENTRO', 'ATIZAPAN DE ZARAGOZA', 'EMEX'),
(30, 'MEXICO PALOMAS', '39', 5, 1, 'AV.CTRAL.ESQ.AV.JARD.MOREL.S/N', '55065', 'COL. STA.CRUZ VENTA DE CARPIO', 'ECATEPEC DE MORELOS', 'EMEX'),
(31, 'MEXICO TECAMAC', '91', 5, 1, 'AV. SIERRA HERMOSA NO.2A MZA78. CARRET. FED. MEXICO-PAC', '55740', 'FRACC. SIERRA HERMOSA', 'TECAMAC', 'EMEX'),
(32, 'MEXICO SANTA FE', '98', 6, 1, 'PROLONGACION VASCO DE QUIROGA No. 3800', '5348', 'SANTA FE CUAJIMALPA', 'CUAJIMALPA DE MORELOS', 'CDMX'),
(33, 'MEXICO IZTAPALAPA', '100', 7, 1, 'SOTO Y GAMA NO. 21', '9240', 'PROGRESISTA', 'IZTAPALAPA', 'CDMX'),
(34, 'MEXICO TOREO', '106', 6, 1, 'BOULEVARD MANUEL AVILA CAMACHO NO.5', '53390', 'LOMAS DE SOTELO', 'NAUCALPAN DE JUAREZ', 'EMEX'),
(35, 'MEXICO OJO DE AGUA', '107', 5, 1, 'AV. SANTA CRUZ OJO DE AGUA NO 88', '55770', 'EX EJIDO DE SAN FRANCISCO', 'TECAMAC', 'EMEX'),
(36, 'MEXICO NICOLAS ROMERO', '111', 4, 1, 'CARR.PAL.ATIZAPAN-NICOLAS ROMERO NO.4', '54473', 'FRANCISCO SARABIA', 'NICOLAS ROMERO', 'EMEX'),
(37, 'MEXICO TEPOZAN', '116', 8, 1, 'AV. TEPOZANES No. 3', '56430', 'VALLE DE LOS REYES', 'LA PAZ', 'EMEX'),
(38, 'MEXICO SAN ESTEBAN', '117', 4, 1, 'CALZADA SAN ESTEBAN NO.46', '53550', 'SAN ESTEBAN HUITZILACASCO', 'NAUCALPAN DE JUAREZ', 'EMEX'),
(39, 'MEXICO CHURUBUSCO', '121', 8, 1, 'RIO CHURUBUSCO NO. 1072', '9420', 'NUEVA ROSITA', 'IZTAPALAPA', 'CDMX'),
(40, 'MEXICO BUEN TONO', '129', 8, 1, 'BUEN TONO NO.8', '6070', 'CENTRO DE LA CIUDAD DE MEXICO AREA 7', 'CUAUHTEMOC', 'CDMX'),
(41, 'MEXICO CIUDAD LABOR', '130', 4, 1, 'AV. BOULEVARD REFORMA No.42', '54944', 'SAN FRANCISCO CHILPAN', 'TULTITLAN', 'EMEX'),
(42, 'AGRICOLA ORIENTAL', '132', 7, 2, 'CALLE SUR 16 No. 271', '8500', 'AGRICOLA ORIENTAL', 'IZTACALCO', 'CDMX'),
(43, 'MEXICO TULYEHUALCO', '140', 7, 1, 'AV. AQUILES CERDAN No. 360', '16740', 'QUIRINO MENDOZA', 'TULYEHUALCO', 'CDMX'),
(44, 'MEXICO COPILCO', '141', 7, 1, 'AV. PEDRO ENRIQUEZ URE??A NO. 514', '4330', 'PUEBLO DE LOS REYES', 'COYOACAN', 'CDMX'),
(45, 'MEXICO COACALCO', '146', 4, 1, 'VIALIDAD MEXIQUENSE NO. 92', '55712', 'FRACC. SAN FRANCISCO', 'COACALCO', 'EMEX'),
(46, 'MEXICO CUAJIMALPA FRONDOSO', '147', 4, 1, 'AV. JOSE MARIA CASTORENA NO. 316', '5000', 'CUAJIMALPA', 'CUAJIMALPA DE MORELOS', 'CDMX'),
(47, 'MEXICO XOCHIMILCO', '154', 7, 1, 'CARRETERA A SANTIAGO TEPELCATLALPAN NO. 400', '16210', 'LA CONCHA', 'XOCHIMILCO', 'CDMX'),
(48, 'MEXICO HEROES DE TECAMAC', '157', 5, 1, 'AV. OZUMBILLA LOTE 27 MZA. 42 NO. 97', '55764', 'LOS HEROES TECAMAC III', 'TECAMAC', 'EMEX'),
(49, 'MEXICO PLAZA AMERICAS', '158', 7, 1, 'BENITO JUAREZ NO. 117', '4980', 'EX HACIENDA COAPA', 'COYOACAN', 'CDMX'),
(50, 'MEXICO BELLAVISTA', '161', 4, 2, 'BLVD. BELLAVISTA NO. 22', '52994', 'LOMAS DE BELLAVISTA', 'ATIZAPAN DE ZARAGOZA', 'EMEX'),
(51, 'MEXICO ZARAGOZA', '165', 8, 1, 'CALZADA IGNACIO ZARAGOZA NO. 846', '8100', 'PANTITLAN', 'IZTACALCO', 'CDMX'),
(52, 'MEXICO LAGO DE GUADALUPE', '167', 4, 2, 'BOSQUE DE BOLOGNIA S/N', '54766', 'BOSQUES DEL LAGO', 'CUAUTITLAN IZCALLI', 'EMEX'),
(53, 'MEXICO TEPEYAC', '173', 5, 1, 'HENRY FORD NO.120', '7820', 'BANDOJITO', 'GUSTAVO A. MADERO', 'CDMX'),
(54, 'MEXICO FORTUNA', '174', 6, 1, 'AV. FORTUNA 334', '7760', 'MAGDALENA DE LAS SALINAS', 'GUSTAVO A. MADERO', 'CDMX'),
(55, 'MEXICO LOMAS ESTRELLA', '175', 7, 1, 'AV. TLAHUAC NUM 4409', '9890', 'LOMAS ESTRELLA', 'IZTAPALAPA', 'CDMX'),
(56, 'PEDREGAL', '177', 6, 1, 'AV. SAN JERONIMO NO.236', '4519', 'LA OTRA BANDA', 'COYOACAN', 'CDMX'),
(57, 'MEXICO SENTURA', '180', 4, 1, 'BOULEVARD MANUEL AVILA CAMACHO NO. 2610', '54040', 'VALLE DE LOS PINOS', 'TLALNEPANTLA DE BAEZ', 'EMEX'),
(58, 'CIEN FUEGOS', '181', 5, 1, 'AV. ACUEDUCTO 650', '7369', 'RESIDENCIAL ZACATENCO', 'GUSTAVO A. MADERO', 'CDMX'),
(59, 'MEXICO INTERLOMAS', '231', 6, 1, 'BOULEVAR INTERLOMAS 5', '52765', 'SAN FERNANDO LA HERRADURA', 'HUIXQUILUCAN', 'EMEX'),
(60, 'MEXICO POLANCO', '232', 6, 1, 'AV.MIGUEL DE CERVANTES DE SAAVEDRA 397', '11500', 'IRRIGACION', 'MIGUEL HIDALGO', 'CDMX'),
(61, 'MEXICO COAPA', '233', 7, 1, 'CALZ. DEL HUESO 670', '4870', 'LOS ROBLES COAYOCAN', 'COYOACAN', 'CDMX'),
(62, 'MEXICO AJUSCO', '234', 7, 1, 'CARRETERA PIACHO AJUSCO 175', '14200', 'HEROES DE PADIERNA', 'TLALPAN', 'CDMX'),
(63, 'MEXICO UNIVERSIDAD', '235', 6, 1, 'AV.UNIVERSIDAD 740', '3310', 'SANTA CRUZ ATOYAC', 'BENITO JUAREZ', 'CDMX'),
(64, 'MEXICO MUNDO E', '236', 4, 1, 'BOULEVARD M.AVILA CAMACHO 1007', '54055', 'SAN LUCAS TEPETLACALCO', 'TLALNEPANTLA DE BAZ', 'EMEX'),
(65, 'MEXICO TENAYUCA', '237', 8, 1, 'AV.LA VENTISCA 100', '7268', 'SOLIDARIDAD', 'GUSTAVO A. MADERO', 'CDMX'),
(66, 'MEXICO MOLINA', '239', 5, 1, 'AV.EDUARDO MOLINA 1623', '7420', 'EL COYOL', 'GUSTAVO A. MADERO', 'CDMX'),
(67, 'MEXICO NEZA ESTADIO', '240', 8, 1, '4TA AVENIDA # 257', '57809', 'LAZARO CARDENAS CD.NEZA', 'NEZAHUALCOYOTL', 'EMEX'),
(68, 'MEXICO ANFORA', '241', 8, 1, 'ANFORA 71', '15320', 'MADERO', 'VENUSTIANO CARRANZA', 'CDMX'),
(69, 'MEXICO NEZA IMPULSORA', '260', 5, 1, 'AV. PLAZAS DE ARAGON NO. 1 MZ. 2 LOTE 2. BLV. PLAZA CEN', '57139', 'PLAZA DE ARAGON', 'NEZAHUALCOYOTL', 'EMEX'),
(70, 'MEXICO CIUDAD AZTECA', '650', 5, 2, 'AV.CENTRAL S/N', '55120', 'SANTA CRUZ VENTA DE CARPIO', 'ECATEPEC DE MORELOS', 'EMEX'),
(71, 'MEXICO SAMARA', '652', 6, 2, 'AVENIDA ANTONIO DOVALI JAIME No.70', '1260', 'SANTA FE', 'ALVARO OBREGON', 'CDMX'),
(72, 'TECAMACHALCO', '655', 6, 2, 'AV. DE LOS BOSQUES NO. 128', '52780', 'LOMAS DE TECAMACHALCO', 'HUIXQUILUCAN', 'EMEX'),
(73, 'TEZONTLE IZTAPALAPA', '664', 7, 3, 'CALLE 15 DE JUNIO DE 1861', '9240', 'LEYES DE REFORMA', 'IZTAPALAPA', 'CDMX'),
(74, 'MEXICO VIVEROS', '667', 4, 2, 'PARQUE CENTRAL No. 44', '54070', 'UNIDAD HABITACIONAL ADOLFO LOPEZ MATEOS', 'TLALNEPANTLA DE BAZ', 'EMEX'),
(75, 'MEXICO TEZONTLE', '670', 7, 2, 'AV. CANAL TEZONTLE No. 1512', '9020', 'DR. ALFONSO ORTIZ TIRADO', 'IZTAPALAPA', 'CDMX'),
(76, 'EJE 4 NORTE AZCAPOTZALCO', '678', 6, 3, 'ANTIGUA CALZADA DE GUADALUPE NO. 2013', '2020', 'BARRIO DE SAN MARCOS', 'AZCAPOTZALCO', 'CDMX'),
(77, 'MEXICO VALLE DEL JUCAR', '681', 5, 3, 'AV. GENERAL FRANCISCO MUNGUIA NO. 55', '55065', 'COL. EJERCITO DEL TRABAJO', 'ECATEPEC DE MORELOS', 'EMEX'),
(78, 'MEXICO VALLE ALTO NEZA', '682', 5, 3, 'AV. VALLE ALTO NO. 214', '55280', 'COL. VALLE DE ARAGON', 'NEZAHUALCOYOTL', 'EMEX'),
(79, 'MEXICO BARRIO ALTO CUAUTEPEC', '683', 8, 3, 'ROBERTO EZQUERRA N??415', '7180', 'FELIPE BERRIOZABAL', 'DELEG. GUSTAVO A. MADERO', 'CDMX'),
(80, 'MEXICO JALALPA ALTA', '688', 4, 3, 'GUSTAVO DIAZ ORDAZ NO. 52', '1296', 'JALALPA TEPITO', 'ALVARO OBREGON', 'CDMX'),
(81, 'MEXICO PRESIDENTES ALVARO OBRE', '689', 4, 3, 'AV. BENITO JUAREZ MZ. 19 LT 10 Y 11', '1290', 'PRESIDENTES', 'ALVARO OBREGON', 'CDMX'),
(82, 'MEXICO HANK GONZALEZ', '693', 5, 3, 'AV. HANK GONZALEZ MZ. 19 LOTE 7', '55065', 'COL. GRANJAS VALLE DE GUADALUPE', 'ECATEPEC DE MORELOS', 'EMEX'),
(83, 'KAUA TLALPAN', '694', 7, 3, 'CALLE KAUA 77', '14100', 'COL. PEDREGAL DE SAN NICOLAS', 'DEL. TLALPAN', 'CDMX'),
(84, 'MEXICO FLORIDA', '696', 8, 3, 'CALLE AGUSTIN LARA 6  LT. 2  MZ. 76', '7130', 'COL. LAS PALMAS', 'DELG GUSTAVO A. MADERO', 'CDMX'),
(85, 'MEXICO JUVENTINO ROSAS', '697', 8, 3, 'JUVENTINO ROSAS MZ 11 LT 17', '7110', 'LOMAS DE CUAUTEPEC', 'GUSTAVO A. MADERO', 'CDMX'),
(86, 'MEXICO CAYETANO ANDRADE', '699', 7, 3, 'JOS?? PER??N DEL VALLE', '9530', 'SANTA MARTHA ACATITLA SUR', 'DELEG. IZTAPALAPA', 'CDMX'),
(87, 'MEXICO REFORMA PERALVILLO', '700', 8, 4, 'AV. PASEO DE LA REFORMA #250 ESQ. FCO GLZ BOCANEGRA', '6200', 'PERALVILLO', 'CUAUHTEMOC', 'CDMX'),
(88, 'MEXICO ECATEPEC ROSALES', '701', 5, 4, 'AV. ALFREDO DEL MAZO ESQ. FILIBERTO G??MEZ #62', '55270', 'GRANJAS VALLE DE GUADALUPE', 'ECATEPEC DE MORELOS', 'EMEX'),
(89, 'MEXICO CUAUTEPEC', '702', 8, 4, 'AV. CUAUTEPEC #117', '7280', 'JORGE NEGRETE', 'GUSTAVO A. MADERO', 'CDMX'),
(90, 'VASCO DE QUIROGA', '710', 4, 3, 'AV. VASCO DE QUIROGA NO. 1684', '1290', 'PUEBLO NUEVO', 'ALVARO OBREGON', 'CDMX'),
(91, 'MEXICO EJE 3 COACALCO', '711', 4, 4, 'EJE 3 NO. 92', '54930', 'SAN PABLO DE LAS SALINAS', 'TULTITLAN', 'EMEX'),
(92, 'MEXICO PANTITLAN', '712', 5, 4, 'CALLE No. 274', '8100', 'AGRICOLA PANTITLAN', 'IZTACALCO', 'CDMX'),
(93, 'MEXICO CHALMA NORTE', '713', 8, 4, 'RIO DE LA LOZA N?? 4', '54140', 'SAN MIGUEL CHALMA', 'TLALNEPANTLA', 'EMEX'),
(94, 'MEXICO NEZA TEXCOCO', '714', 8, 4, 'AV. TEXCOCO 292', '57610', 'PAVON', 'NEZAHUALCOYOTL', 'EMEX'),
(95, 'MEXICO GUSTAVO BAZ', '715', 4, 4, 'AV. GUSTAVO BAZ NO.29', '54080', 'XOCOYAHUALCO', 'TLALNEPANTLA DE BAZ', 'EMEX'),
(96, 'MEXICO VILLA DE LAS FLORES', '719', 4, 3, 'COACALCO S/N ESQ. ORQUIDEAS', '55710', 'VILLA DE LAS FLORES', 'COACALCO DE BERRIOZABAL', 'EMEX'),
(97, 'MEXICO PUENTE DE GUERRA', '720', 6, 3, 'PUENTE DE GUERRA 47', '2120', 'SAN MARTIN XOCHINAHUAC', 'AZCAPOTZALCO', 'CDMX'),
(98, 'MEXICO GUERRA DE REFORMA', '725', 7, 3, 'GUERRA DE REFORMA NO. 807', '9310', 'LEYES DE REFORMA 3RA SECCION', 'IZTAPALAPA', 'CDMX'),
(99, 'MEXICO LA ERA', '726', 4, 3, 'Avenida  29 de Octubre  Mz.248  A lote 40', '1860', 'Lomas de la Era', 'Alvaro Obreg??n', 'CDMX'),
(100, 'MEXICO JUAREZ NEZA', '727', 8, 3, 'JUAREZ MZ. 21 LT. 21', '57430', 'EL PORVENIR', 'NEZAHUALCOYOTL', 'EMEX'),
(101, 'MEXICO LUCAS PATONI', '729', 8, 3, 'BENITO JUAREZ NO. 193', '54140', 'EX EJIDO LUCAS PATONI', 'TLALNEPANTLA', 'EMEX'),
(102, 'MEXICO SANTA LUCIA', '731', 6, 3, 'AV. SANTA LUCIA NO. 343', '1408', 'OLIVAR DEL CONDE', 'ALVARO OBREGON', 'CDMX'),
(103, 'MEXICO MANUEL SALAZAR', '732', 6, 3, 'MANUEL SALAZAR NO. 197', '2400', 'SAN JUAN TLIHUACA', 'AZCAPOTZALCO', 'CDMX'),
(104, 'MEXICO TECOLOXTITLA', '736', 7, 3, 'MANZANILLO NO. 107', '9520', 'SAN SEBATIAN TECOLOXTITLA', 'IZTAPALAPA', 'CDMX'),
(105, 'A MAGDALENA CONTRERAS', '739', 7, 3, 'AVENIDA MEXICO N. 348', '10820', 'LA GUADALUPE', 'MAGDALENA CONTRERAS', 'CDMX'),
(106, 'LA PRESA TLALNEPANTLA', '741', 5, 3, 'DE LOS VOLCANES MZ 92 LOTE 1019', '54187', 'L??ZARO C??RDENAS', 'TLALNEPANTLA', 'EMEX'),
(107, 'A PRO HOGAR CENTRAL', '743', 5, 3, 'CENTRAL NO. 258', '2600', 'PRO HOGAR', 'AZCAPOTZALCO', 'CDMX'),
(108, 'ZAPOTITLAN TLAHUAC', '744', 7, 3, 'FRANCISCO JIMENEZ 17', '13360', 'LA CONCHITA', 'ZAPOTITLAN TLAHUAC', 'CDMX'),
(109, 'SAN HERMILO COYOACAN', '745', 7, 3, 'SAN HERMILO LOTE 18 MZ 890', '4600', 'EJIDO SANTA URSULA COAPA', 'DELEGACION COYOACAN', 'CDMX'),
(110, 'MORELOS VENUSTIANO CARRANA', '746', 8, 3, 'Panadero Esq. Av. Ferrocarril de cintura No. 86', '15270', 'Morelos', 'Venustiano Carranza', 'CDMX'),
(111, 'MEXICO ENCUENTRO TLALNEPANTLA', '747', 8, 2, 'TENAYUCA LOTE 25 ', '54150', 'PUEBLO DE SAN BARTOLO', 'TENAYUCA', 'EMEX'),
(112, 'MEXICO NICOLAS SAN JUAN', '748', 6, 3, 'NICOLAS SAN JUAN 525', '3100', 'DEL VALLE', 'BENITO JUAREZ', 'CDMX'),
(113, 'MEXICO LA PRADERA', '753', 5, 3, 'FRANCISCO MORAZAN 244', '7500', 'GUSTAVO A MADERO', 'GUSTAVO A MADERO', 'CDMX'),
(114, 'PACHUCA TULIPANES', '754', 5, 1, 'Blvd. Nuevo Hidalgo No. 903', '42083', 'Lote D Norte de San Francisco Ex Hacienda de Colonias', 'PACHUCA', 'HGO'),
(115, 'XALAPA CENTRO', '2', 9, 1, 'DR. RAFAEL LUCIO NO: 28', '91000', 'CENTRO', 'XALAPA', 'VER'),
(116, 'XALAPA ALMACENES', '3', 9, 1, 'DR. RAFAEL LUCIO NO. 25', '91000', 'CENTRO', 'XALAPA', 'VER'),
(117, 'XALAPA CRYSTAL', '4', 9, 1, 'AV. LAZARO CARDENAS ESQ. AV. ANTONIO CHEDRAUI CARAM', '91150', 'ENCINAL', 'XALAPA', 'VER'),
(118, 'VERACRUZ CENTRO', '5', 10, 1, 'DIAZ MIRON NO. 400', '91700', 'CENTRO', 'VERACRUZ', 'VER'),
(119, 'VERACRUZ FLORESTA', '6', 10, 1, 'PROL. DIAZ MIRON ESQ. LEOPOLDO KIEL NO. 1', '91940', 'FRACC. FLORESTA', 'VERACRUZ', 'VER'),
(120, 'VERACRUZ NORTE', '7', 10, 1, 'CARMEN PEREZ ESQ. MA. MATAMORO', '91870', 'FRACC. LOS PINOS', 'VERACRUZ', 'VER'),
(121, 'COATZACOALCOS CENTRO', '8', 11, 1, 'REVOLUCION ESQ INDEPENDENCIA', '96410', 'MA. DE LA PIEDAD', 'COATZACOALCOS', 'VER'),
(122, 'CORDOBA CRYSTAL', '9', 12, 1, 'AUTOPISTA CORDOBA-ORIZABA KM 1 S/N ESQ AV 21', '94570', 'LAS LOMAS', 'CORDOBA', 'VER'),
(123, 'TAMPICO CRYSTAL', '12', 13, 1, 'AV. HIDALGO ESQ. REGIOMONTANA', '89349', 'FRACC. LOMAS DEL NARANJAL', 'TAMPICO', 'TAMP'),
(124, 'PUEBLA CRYSTAL', '15', 14, 1, 'BLVD. VALSEQUILLO NO. 115 ESQ. AV 51 PONIENTE', '72440', 'BOULEVARES', 'PUEBLA', 'PUE'),
(125, 'POZA RICA CRYSTAL', '16', 13, 1, 'BLVD. LAZARO CARDENAS 807', '93340', 'MORELOS', 'POZA RICA', 'VER'),
(126, 'COATEPEC CRYSTAL', '17', 9, 1, 'MELCHOR OCAMPO NO. 75 LOCAL F', '91500', 'SUCHIL', 'COATEPEC', 'VER'),
(127, 'XALAPA MUSEO', '19', 9, 1, 'LUCIO BLANCO ESQ PEDRO BENITEZ', '91020', 'OBRERO CAMPESINO', 'XALAPA', 'VER'),
(128, 'VERACRUZ AMERICAS', '20', 10, 1, 'BLVD RUIZ CORTINEZ ESQ AMERICA', '94294', 'COSTA VERDE', 'BOCA DEL RIO', 'VER'),
(129, 'COATZACOALCOS CRYSTAL', '21', 11, 1, 'ABETOS NO. 15', '96558', 'MA. DE LA PIEDAD', 'COATZACOALCOS', 'VER'),
(130, 'MINATITLAN BERLIN', '22', 11, 1, 'BERLIN S/N (CARRETERA TRANSISMICA)', '96710', 'INSURGENTES', 'MINATITLAN', 'VER'),
(131, 'OAXACA LA NORIA', '23', 12, 1, 'AV. PERIFERICO NO. 300', '68120', 'SAN JOSE DE LA NORIA', 'OAXACA DE JUAREZ', 'OAX'),
(132, 'CIUDAD VALLES CUAUHTEMOC', '25', 13, 1, 'PEDRO ANTONIO STOS. NO. 55 ESQ REFORMA', '79040', 'CUAHUTEMOC', 'CIUDAD VALLES', 'SLP'),
(133, 'POZA RICA PALMAS', '27', 13, 1, 'BLVD. ADOLFO RUIZ CORTINES S/N', '93310', 'LAZARO CARDENAS', 'POZA RICA', 'VER'),
(134, 'TUXPAN CRYSTAL', '28', 13, 1, 'BLVD.DEMETRIO RUIZ M.65', '92860', 'ZAPOTE GORDO', 'TUXPAN', 'VER'),
(135, 'ORIZABA CENTRO', '32', 12, 1, 'ORIENTE NO. 6 1265', '94300', 'CENTRO', 'ORIZABA', 'VER'),
(136, 'MARTINEZ DE LA TORRE', '33', 9, 1, 'BOULEVARD ALFINIO FLORES 903', '93600', 'ADOLFO RUIZ CORTINEZ', 'MARTINEZ DE LA TORRE', 'VER'),
(137, 'TAMPICO MADERO', '36', 13, 1, 'AV. EJERCITO MEXICANO NO. 100', '89460', 'LOMA DEL GALLO', 'CIUDAD MADERO', 'TAMP'),
(138, 'PUEBLA CAPU', '38', 14, 1, 'BLVD.NORTE 3622', '72030', 'SAN FELIPE HUEYOTLIPAN', 'PUEBLA', 'PUE'),
(139, 'TUXTEPEC LOS ANGELES', '42', 11, 1, 'BLVD. BENITO JUAREZ 1291', '68300', 'FRACC. LOS ANGELES', 'TUXTEPEC', 'OAX'),
(140, 'TAMPICO ANDONEGUI', '44', 13, 1, 'AV.EMILIO PORTES GIL 1903', '89060', 'TAMAULIPAS', 'TAMPICO', 'TAMP'),
(141, 'TEHUACAN CENTRO', '46', 12, 1, 'AV.INDPENDENCIA ORTE. S/N ESQ.7 NTE', '75700', 'CENTRO', 'TEHUACAN', 'PUE'),
(142, 'VERACRUZ EL COYOL', '48', 10, 1, 'EJES UNOSURPONIENTE 101', '91779', 'FRACCIONAMIENTO EL COYOL', 'VERACRUZ', 'VER'),
(143, 'PUEBLA CRUZ DEL SUR', '52', 14, 1, 'BLVD. FORJADORES DE PUEBLA NO. 1009', '72700', 'CUAUTLANCINGO', 'PUEBLA', 'PUE'),
(144, 'CORDOBA CENTRO', '53', 12, 1, 'AV. 8 NO. 801 ENTRE CALLES 11 Y 13', '94500', 'CENTRO', 'CORDOBA', 'VER'),
(145, 'XALAPA ANIMAS', '59', 9, 1, 'AV. LAZARO CARDENAS ESQ. FEDERICO MENZEL S/N', '91194', 'LOMAS DE ANIMAS', 'XALAPA', 'VER'),
(146, 'TEZIUTLAN CRYSTAL', '64', 9, 1, 'PROLONGACION DE MINA 754', '73800', 'BARRIO DE SAN FRANCIA', 'TEZIUTLAN', 'PUE'),
(147, 'SAN MARTIN TEXMELUCAN', '70', 14, 1, 'ANTIG. CARRETERA A TLAXCALA. KM.1 S/N', '74059', 'SAN DANIEL', 'SAN MARTIN TEXMELUCAN', 'PUE'),
(148, 'ACAYUCAN LA PALMA', '71', 11, 1, 'AV. JUAN DE LA LUZ ENRIQUEZ  711', '96056', 'BARRIO LA PALMA', 'ACAYUCAN', 'VER'),
(149, 'PUEBLA CENTRO SUR', '76', 14, 1, 'PROLONGACION 11SUR. NO.11904', '72490', 'PONIENTE. COL.AGUA SANTA', 'PUEBLA', 'PUE'),
(150, 'PUEBLA UPAEP', '77', 14, 1, 'CALLE 9 PONIENTE NO.1901 ENTRE 19 SUR Y PRIV.21 SUR', '72000', 'LA PAZ', 'PUEBLA', 'PUE'),
(151, 'VERACRUZ LAS BRISAS', '79', 10, 1, 'AV.RAFAEL CUERVO 1150', '91855', 'TARIMOYA ZONA URBANA', 'VERACRUZ', 'VER'),
(152, 'PUEBLA XONACA', '92', 14, 1, 'BLVD. XONACA NO.3408', '72270', 'VISTA HERMOSA', 'PUEBLA', 'PUE'),
(153, 'OAXACA REFORMA', '99', 12, 1, 'ESCUELA NAVAL MILITAR No.917', '68050', 'REFORMA CENTRO', 'OAXACA DE JUAREZ', 'OAX'),
(154, 'OAXACA MADERO', '105', 12, 1, 'CALZADA FRANCISCO I. MADERO NO. 1332', '68030', 'EX MARQUEZADO', 'OAXACA DE JUAREZ', 'OAX'),
(155, 'VERACRUZ PONTI', '120', 10, 1, 'Blvd. de los Patos No. 1109', '91725', 'El palmar', 'Veracruz', 'VER'),
(156, 'VERACRUZ EL DORADO', '139', 10, 1, 'CARRETERA PASO DEL TORO-BOCA DEL RIO-ANTON LIZARDO 4405', '94290', 'SAN JOSE', 'BOCA DEL RIO', 'VER'),
(157, 'APIZACO TLAXCALA', '145', 14, 1, 'CARRETERA APIZACO HUAMANTLA No. 2407', '90347', 'LA CIENEGA', 'APIZACO', 'TLAX'),
(158, 'COATZACOALCOS UNIVERSIDAD', '150', 11, 1, 'AV. UNIVERSIDAD VERACRUZANA K.M. 9.5', '96536', 'LAS GAVIOTAS', 'COATZACOALCOS', 'VER'),
(159, 'TAMPICO ALTAMIRA', '163', 13, 1, 'AV. DE LA INDUSTRIA NO. 10940', '89604', 'MIRAMAR', 'ALTAMIRA', 'TAMP'),
(160, 'OAXACA XOXOCOTLAN', '169', 12, 1, 'BLVD. GUADALUPE HINOJOSA DE MURAT 215', '71230', 'FRACC SANTA CRUZ XOXOCOTLAN', 'SANTA CRUZ XOXOCOTLAN', 'OAX'),
(161, 'VERACRUZ ALUMINIO', '172', 10, 1, 'CARRETERA MEX-XAL-VER KM. 435.3', '91726', 'NUEVO VERACRUZ', 'VERACRUZ', 'VER'),
(162, 'ATLIXCO CHACUACO', '191', 14, 1, 'NI??OS H??ROES S/N', '74200', 'ALTOS REVOLUCI??N', 'ATLIXCO', 'PUE'),
(163, 'SANTA ANA CHIAUTEMPAN', '193', 14, 1, 'ANTONIO DIAZ VARELA', '90800', 'XIMENTLA CENTRO', 'CHIAUTEMPAN', 'TLAX'),
(164, 'VERACRUZ PORTAL', '198', 10, 1, 'SALVADOR DIAZ MIRON 2069', '91918', 'CENTRO', 'VERACRUZ', 'VER'),
(165, 'HUAMANTLA', '601', 14, 2, 'ALLENDE NORTE NO.602 ENTRE JUAREZ Y BLV.YANCUILTLALPAN', '90500', 'EMILIANO ZAPATA', 'HUAMANTLA', 'TLAX'),
(166, 'PAPANTLA', '602', 13, 2, 'JUAN DE LA LUZ ENRIQUEZ ESQ.OBISPO DE LAS CASAS NO.101', '93400', 'CENTRO', 'PAPANTLA', 'VER'),
(167, 'TIERRA BLANCA', '605', 12, 1, 'AV.AQUILES SERDAN NO.1102', '95110', 'HOJAS DE MAIZ', 'TIERRA BLANCA', 'VER'),
(168, 'CARDEL EL MODELO', '606', 10, 1, 'EMILIANO ZAPATA 60', '91685', 'MODELO', 'LA ANTIGUA', 'VER'),
(169, 'NANCHITAL', '613', 11, 2, 'BLVD.LOPEZ PORTILLO 50', '96360', 'SAN AGUSTIN', 'NANCHITAL', 'VER'),
(170, 'JALTIPAN', '616', 11, 2, 'CARRETERA TRANSITMICA 932', '96280', 'DEPORTIVA', 'JALTIPAN', 'VER'),
(171, 'TEPEACA', '622', 14, 2, 'MORELOS NO. 309-B', '75200', 'CENTRO', 'TEPEACA', 'PUE'),
(172, 'XALAPA CAROLINO ANAYA', '623', 9, 2, 'CAMINO ANTIGUO A NAOLINCO NO. 407', '91158', 'CAROLINO ANAYA', 'XALAPA', 'VER'),
(173, 'XALAPA IGNACIO DE LA LLAVE', '626', 9, 2, 'AV.RUIZ CORTINEZ 47 ENTRE AV.IGNACIO DE LA LLAVE Y NAR.', '91070', 'SALUD', 'XALAPA', 'VER'),
(174, 'LOMA BONITA', '627', 11, 2, '16 DE SEPTIEMBRE NO. 34', '68400', 'CENTRO', 'LOMA BONITA', 'OAX'),
(175, 'COSAMALOAPAN', '634', 11, 2, 'BLVD. MIGUEL ALEMAN NO. 1390', '95400', 'CENTRO', 'COSAMALOAPAN', 'VER'),
(176, 'NOGALES', '636', 12, 2, 'AV. JUAREZ NO. 466 - B', '94720', 'AQUILES SERDAN', 'NOGALES', 'VER'),
(177, 'HUEJUTLA DE REYES', '649', 13, 1, 'CARRETERA MEXICO-TAMPICO S/N', '43000', 'LOS CANTORES', 'HUEJUTLA DE REYES', 'HGO'),
(178, 'XALAPA 20 DE NOVIEMBRE', '657', 9, 2, 'AV. 20 DE NOVIEMBRE ORIENTE NO. 77', '91030', 'JOS?? CARDEL', 'XALAPA', 'VER'),
(179, 'HUATUSCO', '660', 12, 2, 'AV. 7 PTE No. 644', '94100', 'CENTRO', 'HUATUSCO', 'VER'),
(180, 'PEROTE', '661', 9, 2, 'AV. ALEJANDRO VON HUMBOLDT NO. 26', '91270', 'CENTRO', 'PEROTE', 'VER'),
(181, 'PUEBLA ANGELOPOLIS', '668', 14, 1, 'LATERAL SUR DE LA VIA ATLIXCAYOTL NO.6510', '72820', 'SAN BERNARDINO TLAXCALANZINGO', 'SAN ANDRES CHOLULA', 'PUE'),
(182, 'PANUCO', '673', 13, 2, 'BLVD. SALVADOR DIAZ MIRON NUM. 701', '93990', 'SALAS', 'P??NUCO', 'VER'),
(183, 'SAN ANDRES TUXTLA', '675', 11, 1, 'BLVD. 5 DE FEBRERO NUM. 131', '95700', 'CENTRO', 'SAN ANDRES TUXTLA', 'VER'),
(184, 'POZA RICA PETROMEX', '676', 13, 1, 'BLVD. ADOLFO RUIZ CORTINES S/N', '93260', 'LAZARO CARDENAS', 'POZA RICA', 'VER'),
(185, 'JB LOBOS VERACRUZ', '690', 10, 2, 'CALLE JB LOBOS S/N', '91779', 'LAS BAJADAS', 'VERACRUZ', 'VER'),
(186, 'TAMPICO CENTRO', '706', 13, 4, 'BENITO JUAREZ NO. 222 ENTRE FCO. I. MADERO Y S. DIAZ MI', '89000', 'CENTRO', 'TAMPICO', 'TAMP'),
(187, 'VERACRUZ AEROPUERTO', '708', 10, 2, 'AV. MALIBRAN LAS BRUJAS 78', '91698', 'MALIBRAN LAS BRUJAS', 'VERACRUZ', 'VER'),
(188, 'VERACRUZ ALLENDE', '738', 10, 3, 'IGNACIO ALLENDE  N??1915', '91700', 'CENTRO', 'VERACRUZ', 'VER'),
(189, 'PUEBLA EXPLANADA', '749', 14, 2, 'CALLE ALEJANDRA No. 512', '72760', 'JUNTA AUXILIAR SANTIAGO MOMOXPAN', 'SAN PEDRO CHOLULA', 'PUE'),
(190, 'ACAPULCO CAYACO', '60', 15, 1, 'AV. LAZARO CARDENAS S/N ESQ. CARRETERA CAYACO PUNTA M.', '39799', 'LA SABANA', 'ACAPULCO DE JUAREZ', 'GRO'),
(191, 'LA PAZ SANTA FE', '122', 16, 1, 'BLVD. PINO PALLAS NO. 105', '23085', 'SANTA FE', 'LA PAZ', 'BCS'),
(192, 'LA PAZ PALACIO', '126', 16, 1, 'ISABEL LA CATOLICA No.1915', '23000', 'CENTRO', 'LA PAZ', 'BCS'),
(193, 'LA PAZ COLIMA', '127', 16, 1, 'CALLE COLIMA S/N', '23060', 'PUEBLO NUEVO', 'LA PAZ', 'BCS'),
(194, 'CABO SAN LUCAS', '128', 16, 1, 'CARR. A TODOS SANTOS KM 1.5 S/N', '23474', 'ARCOS DEL SOL', 'CABO SAN LUCAS', 'BCS'),
(195, 'SAN JOSE DEL CABO', '133', 16, 1, 'CARR. TRANSPENINSULAR S/N', '23427', 'EL ZACATAL', 'SAN JOSE DEL CABO', 'BCS'),
(196, 'ACAPULCO DIAMANTE', '138', 15, 1, 'BLVD. DE LAS NACIONES No. 1817 INMEDIACIONES DEL AEROPU', '39897', 'PLAYA DIAMANTE', 'ACAPULCO DE JUAREZ', 'GRO'),
(197, 'CUERNAVACA CENTRAL DE ABASTOS', '151', 17, 1, 'CARR. ZAPATA-TEZOYUCA NO.38', '62766', 'CAMPO NUEVO', 'EMILIANO ZAPATA', 'MOR'),
(198, 'TEMIXCO MORELOS', '166', 17, 1, 'BOULEVARD APATLACO NO. 171', '62590', 'CAMPO DEL RAYO', 'TEMIXCO', 'MOR'),
(199, 'SAN JOSE DEL CABO CAMPESTRE', '178', 16, 1, 'CARRETERA LIBRE TRANSPENINSULAR ESQUINA LIBRAMIENTO AER', '23427', 'CLUB CAMPESTRE SAN JOSE  DEL CABO', 'SAN JOSE DEL CABO', 'BCS'),
(200, 'CABO NORTE', '189', 16, 1, 'FRACC. \"C\"\" PARCELA 121 ZZP 1/3\"', '23429', ' EJIDO CABO S.C.', 'LOS CABOS', 'BCS'),
(201, 'JIUTEPEC', '195', 17, 1, 'BLVD. CAUHNAHUAC NO 3', '62550', 'MOCTEZUMA', 'JIUTEPEC MORELOS', 'MOR'),
(202, 'CUERNAVACA FLORES MAGON', '238', 17, 1, 'PASEO. CUAUHNAHUAC KM 1.5', '62370', 'FLORES MAGON', 'CUERNAVACA', 'MOR'),
(203, 'TOLUCA ALFREDO DEL MAZO', '242', 17, 1, 'AV. ALFREDO DEL MAZO 705', '50010', 'TLACOPA', 'TOLUCA', 'EMEX'),
(204, 'TOLUCA METEPEC', '243', 17, 1, 'PROLONGACION GUADALUPE VICTORIA # 471', '52160', 'LA PURISIMA', 'METEPEC', 'EMEX'),
(205, 'ACAPULCO PIE DE LA CUESTA', '254', 15, 1, 'CALZ.PIE DE LA CUESTA 239', '39480', 'BARRIO DEL PASITO', 'ACAPULCO DE JUAREZ', 'GRO'),
(206, 'TAXCO GUERRERO', '603', 17, 2, 'AV.PLATEROS 66', '40230', 'BARRIO DE FLORIDA', 'TAXCO', 'GRO'),
(207, 'VALLE DE BRAVO', '611', 17, 2, 'BLVD.JUAN HERRERA Y PI??A NO.7', '51200', 'BARRIO OTUMBA', 'VALLE DE BRAVO', 'EMEX'),
(208, 'TELOLOAPAN', '619', 17, 2, 'CLUB DE LEONES NO.56', '40400', 'CENTRO', 'TELOLOAPAN', 'GRO'),
(209, 'CHILAPA', '620', 15, 2, 'PROLONGACION 18 NORTE 1314', '41100', 'MUNICIPIO LIBRE', 'CHILAPA DE ALVAREZ', 'GRO'),
(210, 'ATOYAC', '621', 15, 2, 'AQUILES SERDAN NO.86', '40930', 'SANTA DOROTEA', 'ATOYAC DE ALVAREZ', 'GRO'),
(211, 'TLAPA DE COMONFORT', '625', 15, 2, 'MORELOS 197', '41300', 'SAN FRANCISCO', 'TLAPA DE COMONFORT', 'GRO'),
(212, 'OMETEPEC', '633', 15, 2, 'BOULEVARD JUAN N. ALVAREZ N0. 24', '41706', 'BARRIO DE TALAPA', 'OMETEPEC', 'GRO'),
(213, 'TENANCINGO', '635', 17, 2, 'BENITO JUAREZ S/N', '51400', 'SAN ISIDRO', 'TENANCINGO', 'EMEX'),
(214, 'TEJUPILCO', '648', 17, 2, 'AV. 27 DE SEPTIEMBRE NO. 60', '51400', 'CENTRO', 'TEJUPILCO', 'EMEX'),
(215, 'CUERNAVACA ALTA TENSION', '658', 17, 2, 'AVENIDA ALTA TENSION NO. 202', '62448', 'CANTARRANAS', 'CUERNAVACA', 'MOR'),
(216, 'PINOTEPA NACIONAL', '685', 15, 2, 'AV. PORFIRIO DIAZ ENTRE 3?? Y 5?? NORTE  No. 224', '71600', 'CENTRO', 'SANTIAGO PINOTEPA', 'OAX'),
(217, 'ACAPULCO CENTRO', '705', 15, 4, 'AV. CUAUHTEMOC LOTES 2 Y 10 NO 105 ENTRE AV 5 MAYO Y M', '39300', 'CENTRO', 'ACAPULCO DE JUAREZ', 'GRO'),
(218, 'ZITACUARO', '707', 17, 4, 'MIGUEL HIDALGO PTE 13 ENTRE 5 DE MAYO Y DR E. GAR', '61518', 'JOSE MARIA MORELOS', 'ZITACUARO', 'MICH'),
(219, 'CAPULHUAC EDO DE MEXICO', '737', 17, 2, 'AV. 16 DE SEPTIEMBRE 313', '52730', 'SAN MIGUELITO', 'CAPULHUA DE MIRAFUETES', 'EMEX'),
(220, 'VILLA MINA', '10', 18, 1, 'AV. FCO JAVIER MINA ESQ. ARBOLEDAS', '86000', 'CENTRO', 'VILLAHERMOSA', 'TAB'),
(221, 'VILLA CRYSTAL', '11', 18, 1, 'AV. HEROICO COLEGIO MILITAR ESQ QUINTIN ARAUZ S/N', '86139', 'ATASTA DE SERRA', 'VILLAHERMOSA', 'TAB'),
(222, 'TUXTLA CRYSTAL', '13', 19, 1, 'BLVD BELISARIO DOMINGUEZ 1691', '29100', 'CENTRO', 'TUXTLA GUTIERREZ', 'CHPS'),
(223, 'VILLA OLMECA', '24', 18, 1, 'ADOLFO RUIZ CORTINEZ 1310', '86035', 'TABASCO 2000', 'VILLAHERMOSA', 'TAB'),
(224, 'CANCUN TULUM', '26', 20, 1, 'AV. TULUM No. 57 y 59 SUPERMANZANA 22', '77500', 'CENTRO', 'BENITO JUAREZ', 'QROO'),
(225, 'MERIDA ITZAES', '29', 21, 1, 'CALLE 86-B NUM. 544', '97000', 'CENTRO', 'MERIDA', 'YUC'),
(226, 'TAPACHULA SUR', '30', 19, 1, '4A.AV.SUR PROLONGACION \"C\"\". LAS PALOMAS NO. 224\"', '30797', 'CANTARRANAS', 'TAPACHULA', 'CHPS'),
(227, 'CARDENAS PUEBLO NUEVO', '31', 18, 1, 'ADOLFO LOPEZ MATEOS S/N', '86500', 'PUEBLO NUEVO', 'CARDENAS', 'TAB'),
(228, 'TUXTLA ORIENTE', '37', 19, 1, 'BLVD. ANGEL ALBINO C. NO.741', '29040', 'REAL DEL BOSQUE', 'TUXTLA GUTIERREZ', 'CHPS'),
(229, 'CAMPECHE SANTA ANA', '41', 22, 1, 'AVENIDA GOBERNADORES 418', '24050', 'SANTA ANA', 'CAMPECHE', 'CAMP'),
(230, 'MERIDA AMERICAS', '43', 21, 1, 'CALLE 21 NO. 327-A  POR 50 Y 52  (56 A)', '97229', 'ITZIMNA', 'MERIDA', 'YUC'),
(231, 'CANCUN PORTILLO', '45', 20, 1, 'AV.LOPEZ PORTILLO 558', '77514', 'SUPER MANZANA 61', 'BENITO JUAREZ', 'QROO'),
(232, 'CANCUN AMERICAS', '47', 20, 1, 'AV. TULUM NO. 260.  SUPERMANZANA 07.  MZA. 4 Y 9', '77500', 'SUPER MANZANA 07', 'BENITO JUAREZ', 'QROO'),
(233, 'VILLA ALMACENES', '49', 18, 1, 'GIL Y SAENZ.  ARBOLEDAS S/N.  ESQ. LAMBERTO CASTELLANOS', '86000', 'CENTRO', 'VILLAHERMOSA', 'TAB'),
(234, 'CIUDAD DEL CARMEN TACUBAYA', '50', 23, 1, 'AV.31 NO.164', '24180', 'TACUBAYA', 'CIUDAD DEL CARMEN', 'CAMP'),
(235, 'MATAMOROS SEXTA', '54', 24, 1, 'AV. SEXTA NO. 200', '87350', 'COL.INDUSTRIAL', 'MATAMOROS', 'TAMP'),
(236, 'COZUMEL CENTRO', '58', 25, 1, 'AV. RAFAEL E. MELGAR NO.1001', '77600', 'CENTRO', 'COZUMEL', 'QROO'),
(237, 'VILLA AMERICAS', '61', 18, 1, 'AV. PROF. RAMON MENDOZA  NO 102 ESQ. AV. UNIVERSIDAD', '86020', 'JOSE MA.PINO SUAREZ', 'VILLAHERMOSA', 'TAB'),
(238, 'PLAYA DEL CARMEN CENTRO', '62', 25, 1, '45 AV. SUR ESQ. CALLE-3 SUR S/N', '77710', 'CENTRO', 'SOLIDARIDAD', 'QROO'),
(239, 'MATAMOROS DEL NI??O', '63', 24, 1, 'AV. DEL NI??O 121 ESQ. CALLE ZAPATA', '87315', 'EJIDO 20 DE NOVIEMBRE', 'MATAMOROS', 'TAMP'),
(240, 'NUEVO LAREDO AQUILES SERDAN', '65', 24, 1, 'AV.AQUILES SERDAN NO. 1725', '88050', 'FERROCARRIL', 'NUEVO LAREDO', 'TAMP'),
(241, 'MERIDA ORIENTE', '66', 21, 1, 'CALLE 57 A NO. 746-A', '97160', 'PACANTUN', 'MERIDA', 'YUC'),
(242, 'CANCUN TALLERES', '68', 20, 1, 'LOTE49 MZNZ1 SUPERMANZANA 218.FRAC.I FRANCA NORTE S/N', '77500', 'PUENTE DE JUAREZ.S/N SAN DAMIA', 'BENITO JUAREZ', 'QROO'),
(243, 'CHETUMAL INSURGENTES', '69', 22, 1, 'AV. INSURGENTES KM. 5.025 ENTRE COBA Y TORCASA', '77084', 'ENMACIPACION DE MEXICO', 'CHETUMAL', 'QROO'),
(244, 'SAN CRISTOBAL SAN ROMAN', '72', 19, 1, 'HERMANOS PANIAGUA # 50', '29240', 'BARRIO SAN RAMON', 'SAN CRISTOBAL DE LAS CASAS', 'CHPS'),
(245, 'MERIDA MONTERREAL', '78', 21, 2, 'CALLE 37 215-A', '97133', 'MONTERREAL', 'MERIDA', 'YUC'),
(246, 'PLAYA DEL CARMEN AMERICAS', '90', 25, 1, 'CARRR.FED. CHETUMAL CANCUN. CRUZ DE SERVICIOS KM 291', '77710', 'KM 291SMZ GRAN PLAZA', 'SOLIDARIDAD', 'QROO'),
(247, 'MERIDA POLIGONO', '93', 21, 1, 'CALLE 33 NO 200 A INTERIOR 40', '97143', 'ITZIMA 108', 'MERIDA', 'YUC'),
(248, 'PLAYA DEL CARMEN VILLAS', '94', 25, 1, 'AV. DE LOS GAVILANES  S/N  MANZANA 013', '77723', 'VILLAS DEL SOL', 'SOLIDARIDAD', 'QROO'),
(249, 'VILLA TAMULTE', '97', 18, 1, 'REVOLUCION NO. 910. ENTRE CALLE TAMPICO Y MANZANILLO', '86150', 'TAMULTE DE LAS BARRANCAS', 'VILLAHERMOSA', 'TAB'),
(250, 'CIUDAD DEL CARMEN PALMIRAS', '101', 23, 1, 'AV. ISLA DE TRIS NO. 7-E', '24100', 'S/NOMBRE', 'CIUDAD DEL CARMEN', 'CAMP'),
(251, 'CAMPECHE VILLA TURQUESA', '102', 22, 1, 'AVENIDA ALVARO OBREGON NO. 443', '24020', 'BARRIO DE SANTA LUCIA', 'CAMPECHE', 'CAMP'),
(252, 'NUEVO LAREDO HIPODROMO', '110', 24, 1, 'PERU N??6301', '88170', 'HIPODROMO', 'NUEVO LAREDO', 'TAMP'),
(253, 'CANCUN VILLAS DEL MAR', '112', 20, 1, 'RESERVA NTE DEL EJIDO DE ISLA MUJERES MZA11 LOTE 111016', '77516', 'SUPERMANZANA 248', 'BENITO JUAREZ', 'QROO'),
(254, 'COMALCALCO NORTE', '113', 23, 1, 'BOULEVARD ADOLFO LOPEZ MATEOS NORTE NO. 102', '86300', 'CENTRO', 'COMALCALCO', 'TAB'),
(255, 'CHETUMAL MULTIPLAZA', '118', 22, 1, 'AV. CONSTITUYENTES DEL 74 NO. 254', '77086', 'FRACCIONAMIENTO EL ENCANTO', 'OTHON P. BLANCO', 'QROO'),
(256, 'MACUSPANA CENTRO', '131', 23, 1, 'ESQ. DE BENITO JUARES Y ALATORRE S/N', '86706', 'CENTRO', 'MACUSPANA', 'TAB'),
(257, 'CANCUN LAKIN', '134', 20, 1, 'Orqu??deas SM259 MZA 101 L01 entre Av. Lakin y sin nombr', '77500', 'Fraccionamiento Villas Otoch Paraiso', 'Benito Juarez', 'QROO'),
(258, 'MERIDA CAUCEL', '135', 21, 1, 'CALLE 23 NO. 612 ENTRE LA 62 Y 70', '97314', 'FRACCIONAMIENTO CIUDAD CAUCEL', 'MERIDA', 'YUC'),
(259, 'TAPACHULA PONIENTE', '137', 19, 1, '17 A CALLE PONIENTE No.41 ESQ CON 12 A AV. NORTE', '30730', '1?? DE MAYO', 'TAPACHULA', 'CHPS'),
(260, 'CANCUN CHAC MOOL', '144', 20, 1, 'CHAC MOOL  LOTE 1 MANZANA 1 SUPERMANZANA 315', '77560', 'EJIDO ALFREDO V. BONFIL', 'BENITO JUAREZ', 'QROO'),
(261, 'TULUM IPAE', '153', 25, 1, 'NEPTUNO MZA 004 LOTE 001', '77780', 'TULUM RUINAS EN TULUM', 'TULUM', 'QROO'),
(262, 'TUXTLA NORTE', '170', 19, 1, 'LIBRAMIENTO NORTE ORIENTE No. 126', '29034', 'LA PIMIENTA', 'TUXTLA GUTIERREZ', 'CHPS'),
(263, 'PARAISO TABASCO', '171', 23, 1, 'BOULEVARD MANUEL ANTONIO ROMERO ZURITA S/N', '86605', 'CENTRO', 'PARAISO', 'TAB'),
(264, 'COMITAN', '183', 19, 1, 'BOULEVARD SUR DR.BELISARIO DOMINGUEZ NO. 809', '30029', 'BARRIO LA PILETA', 'COMITAN DE  DOMINGUEZ', 'CHPS'),
(265, 'CANCUN ARCO NORTE', '184', 20, 1, 'AV HEBERTO CASTILLO MARTINEZ S/N', '77507', 'PASEO DEL MAR', 'CANC??N', 'QROO'),
(266, 'COZUMEL CARRET. TRANSVERSAL', '190', 25, 1, 'LIC. FELIX GONZALEZ CANTO', '77620', 'EMILIANO ZAPATA', 'COZUMEL', 'QROO'),
(267, 'PLAYA DEL CARMEN VELAMAR', '199', 25, 1, 'AV. PETEMPICH S/N', '77712', 'EJIDAL', 'PLAYA DEL CARMEN ', 'QROO'),
(268, 'VILLA CARRIZAL', '252', 18, 1, 'AV. ADOLFO RUIZ CORTINEZ Y PERIFERICO S/N', '86038', 'EL CARRIZAL', 'VILLAHERMOSA', 'TAB'),
(269, 'MERIDA NORTE', '253', 21, 1, 'CALLE 60 # 301-A POR 4 Y 16 (ZONA INDUSTRIAL)', '97110', 'REVOLUCION CORDEMEX', 'MERIDA', 'YUC'),
(270, 'CANCUN KABAH', '255', 20, 1, 'AV. KABAH Y COBA 54. MZA. 1  LOTE 2 SUPERMANZANA 36', '77507', 'KABAH', 'BENITO JUAREZ', 'QROO'),
(271, 'CANCUN MUNDO MAYA', '256', 20, 1, 'AV.LOPEZ PORTILLO SMZ.98 MANZANA53 LTE1 ENTE CALLE 121', '77537', 'SUPERMANZANA 98', 'BENITO JUAREZ', 'QROO'),
(272, 'PALENQUE', '604', 23, 1, 'CARRETERA PALENQUE PAKANNA NO. 84 ENTRE MANUEL VELASCO', '29960', 'TULIJA', 'PALENQUE', 'CHPS'),
(273, 'TICUL', '608', 21, 2, 'CALLE 21 NO.213 ENTRE CALLE 28 Y 30', '97860', 'CENTRO', 'TICUL', 'YUC'),
(274, 'ESCARCEGA', '610', 22, 2, 'AV.HECTOR PEREZ MTZ 71', '24350', 'CENTRO', 'ESCARCEGA', 'CAMP'),
(275, 'JALPA DE MENDEZ', '612', 23, 2, 'PROL DE AL AV.CONSTITUCION 125', '86200', 'BARRIO LA CANDELARIA', 'JALPA DE MENDEZ', 'TAB'),
(276, 'MACUSPANA', '614', 23, 2, 'PASEO JOSE NROVIROSA NO.130', '86706', 'CENTRO', 'MACUSPANA', 'TAB'),
(277, 'HUATULCO', '615', 22, 1, 'BOLV.CHAHUE LOTE 23 A MZA 02', '70989', 'SECTOR R', 'BAHIAS DE HUATULCO', 'OAX'),
(278, 'PUERTO ESCONDIDO', '617', 22, 1, 'AV.OAXACA 105', '71980', 'CENTRO', 'PUERTO ESCONDIDO', 'OAX'),
(279, 'CHAMPOTON', '618', 22, 2, 'LUIS DONALDO COLOSIO NO. 358', '24400', 'VENUSTIANO CARRANZA', 'CHAMPOTON', 'CAMP'),
(280, 'VILLAFLORES', '628', 19, 2, 'BLVD. SALIDA A VILLACORZO DESVIO A FCO. VILLA NO. 245', '30470', 'PREDIO URBANO', 'VILLAFLORES', 'CHPS'),
(281, 'CHIAPA DE CORZO', '630', 19, 2, 'CLZ. VICTORICO L. GRAJALES NO. 5. ENTRE CARRET. CHIAPA', '29160', 'NANDAMBUA 1A. SECCION', 'CHIAPA DE CORZO', 'CHPS'),
(282, 'VALLADOLID', '631', 21, 2, 'CALLE 42 NO. 2909 ENTRE CALLE 40 Y 42', '97780', 'CENTRO', 'VALLADOLID', 'YUC'),
(283, 'EMILIANO ZAPATA', '632', 23, 2, 'AV. CORREGIDORA NO. 600', '86981', 'GANADERA', 'EMILIANO ZAPATA', 'TAB'),
(284, 'ISLA MUJERES', '644', 20, 1, 'AV. RUEDA MEDINA SM 003 MZ 113 LT 017', '77407', 'ELECTRICISTAS', 'ISLA MUJERES', 'QROO'),
(285, 'FELIPE CARRILLO PUERTO', '645', 22, 2, 'AV. LAZARO CARDENAS NO. 639', '77230', 'CECILIO CHI', 'FELIPE CARRILLO PUERTO', 'QROO'),
(286, 'PUERTO AVENTURAS', '651', 25, 2, 'CARRETERA FEDERAL 307 MZNA 012.002 LOTE 001.060-1', '77733', 'PUERTO AVENTURAS', 'SOLIDARIDAD', 'QROO'),
(287, 'TEAPA', '654', 18, 2, 'BLVD. FRANCISCO TRUJILLO GURRIA NO. 1', '86800', 'CENTRO', 'TEAPA', 'TAB'),
(288, 'TENOSIQUE', '659', 23, 2, '26 NO.804', '86901', 'CENTRO', 'TENOSIQUE', 'TAB'),
(289, 'PUERTO MORELOS CENTRO', '663', 25, 2, 'CARRET. FED. CANCUN-PLAYA DEL CARMEN MZA 01 LTE1-02 S/N', '77505', 'SUPERMANZANA 18', 'BENITO JUAREZ', 'QROO'),
(290, 'CANCUN ZONA HOTELERA', '665', 20, 2, 'BOULEVARD KUKULCAN MZA 48 Y 49 LTE ZC1-F1 SECC A', '77500', 'ZONA HOTELERA S/N', 'BENITO JUAREZ', 'QROO'),
(291, 'HUIMANGUILLO', '671', 18, 2, 'GRAL IGNACIO GUTIERREZ GOMEZ NUM 115-A', '86400', 'CENTRO', 'HUIMANGUILLO', 'TAB'),
(292, 'TUXTLA CENTRAL CAMIONERA', '677', 19, 2, 'Av. 9?? sur Oriente No. 1828', '29019', 'CAMIONERA', 'Tuxtla Guti??rrez Chiapas', 'CHPS'),
(293, 'VILLA GAVIOTAS', '703', 18, 2, 'MALECON LEANDRO RIVIROSA WADE NO. 1310', '86090', 'LAS GAVIOTAS NORTE', 'VILLAHERMOSA', 'TAB'),
(294, 'CIUDAD HIDALGO', '709', 19, 2, '3A AVENIDA NORTE NO.22', '30840', 'CENTRO', 'SUCHIATE', 'CHPS'),
(295, 'OCOZOCOAUTLA', '716', 19, 2, 'TERCERA ORIENTE ESQ. PRIMERA SUR', '29140', 'BARRIO SAN ANTONIO', 'OCOZOCOAUTLA DE ESPINOSA', 'CHPS'),
(296, 'HUIXTLA', '722', 19, 2, 'CALLE GUERRERO PONIENTE NO. 33', '30640', 'CENTRO', 'HUIXTLA', 'CHPS'),
(297, 'PUERTO MORELOS PLAYA', '723', 25, 1, 'AV. RAFAEL MELGAR NO.4', '77580', 'CENTRO', 'PUERTO MORELOS', 'QROO'),
(298, 'SAN PEDRO POCHUTLA', '734', 22, 2, 'INTERSECCI??N DE LAS AV. L??ZARO C??RDENAS N. 95A', '70900', 'CENTRO', 'SAN PEDRO POCHUTLA', 'OAX'),
(299, 'TUXTLA AMBAR', '752', 19, 2, 'SAN PEDRO TAPANATEPEC NO.24', '29040', 'EL RETIRO', 'TUXTLA GUTIERREZ', 'CHPS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursalesgenerales`
--

DROP TABLE IF EXISTS `sucursalesgenerales`;
CREATE TABLE `sucursalesgenerales` (
  `ejercicioID` varchar(4) NOT NULL,
  `sucursalID` int(11) NOT NULL,
  `m2` decimal(10,2) NOT NULL,
  `trabajadores` int(11) NOT NULL,
  `clientes` int(11) NOT NULL,
  `promotores` int(11) NOT NULL,
  `visitantes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sucursalesgenerales`
--

INSERT INTO `sucursalesgenerales` (`ejercicioID`, `sucursalID`, `m2`, `trabajadores`, `clientes`, `promotores`, `visitantes`) VALUES
('2018', 33, '1000.00', 0, 0, 0, 0),
('2018', 117, '400.00', 100, 2000, 20, 5000),
('2018', 121, '300.20', 0, 0, 0, 0),
('2018', 122, '0.00', 0, 0, 0, 0),
('2018', 130, '0.00', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipossucursal`
--

DROP TABLE IF EXISTS `tipossucursal`;
CREATE TABLE `tipossucursal` (
  `tipoSucursalID` int(11) NOT NULL,
  `tipoSucursal` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipossucursal`
--

INSERT INTO `tipossucursal` (`tipoSucursalID`, `tipoSucursal`) VALUES
(1, 'CHEDRAUI'),
(4, 'SUPER CHE'),
(2, 'SUPER CHEDRAUI'),
(3, 'SUPERCITO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `usuarioID` int(11) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `pass` varchar(12) NOT NULL,
  `nombreCompleto` varchar(300) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `rolID` int(11) NOT NULL,
  `sucursalID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuarioID`, `usuario`, `pass`, `nombreCompleto`, `email`, `telefono`, `rolID`, `sucursalID`) VALUES
(27, 'jonasor', 'jonasor', 'Jonathan Edir Ortega Rojas', 'jonasor@hotmail.com', '', 1, NULL),
(36, 'chucho', 'chucho', 'chucho', '', '', 1, NULL),
(38, 'tienda', 'tienda', 'tienda', '', '', 3, 115),
(41, 'aaron', 'aaron', 'Aarón  Córdoba Gálvez', 'acgalvez@chedraui.com.mx', '', 2, NULL),
(42, 'adrian', 'adrian', 'Adrián Domínguez Vázquez', 'adominguezv@chedraui.com.mx', '', 2, NULL),
(43, 'marcos', 'marcos', 'Marcos Saldaña Alarcón', 'msalarcon@chedraui.com.mx', '', 2, NULL),
(44, 'melquiades', 'melquiades', 'Melquiades Cruz Alarcón', 'malarcon@chedraui.com.mx', '', 2, NULL),
(45, 'norberto', 'norberto', 'Norberto Sánchez Dionisio', 'ndionisio@chedraui.com.mx', '', 2, NULL),
(46, 'paloma', 'paloma', 'Paloma Zurisadai Rodríguez Ruíz', 'pzurisadai@chedraui.com.mx', '', 2, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zonas`
--

DROP TABLE IF EXISTS `zonas`;
CREATE TABLE `zonas` (
  `zonaID` int(11) NOT NULL,
  `zona` varchar(30) NOT NULL,
  `regionID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `zonas`
--

INSERT INTO `zonas` (`zonaID`, `zona`, `regionID`) VALUES
(1, 'ZONA LEON', 1),
(2, 'ZONA QUERETARO', 1),
(3, 'ZONA GUADALAJARA', 1),
(4, 'ZONA V DE M NORTE', 2),
(5, 'ZONA V DE M ORIENTE', 2),
(6, 'ZONA V DE M SELECTO', 2),
(7, 'ZONA V DE M SUR', 2),
(8, 'ZONA V DE M CENTRO', 2),
(9, 'ZONA XALAPA', 3),
(10, 'ZONA VERACRUZ', 3),
(11, 'ZONA COATZACOALCOS', 3),
(12, 'ZONA CORDOBA', 3),
(13, 'ZONA TAMPICO', 3),
(14, 'ZONA PUEBLA', 3),
(15, 'ZONA ACAPULCO', 4),
(16, 'ZONA BAJA CALIFORNIA', 4),
(17, 'ZONA TOLUCA', 4),
(18, 'ZONA VILLAHERMOSA', 5),
(19, 'ZONA TUXTLA', 5),
(20, 'ZONA CANCUN', 5),
(21, 'ZONA MERIDA', 5),
(22, 'ZONA CAMPECHE', 5),
(23, 'ZONA TABASCO', 5),
(24, 'ZONA FRONTERA', 5),
(25, 'ZONA RIVIERA MAYA', 5);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asignaciones`
--
ALTER TABLE `asignaciones`
  ADD PRIMARY KEY (`ejercicioID`,`usuarioID`,`sucursalID`),
  ADD KEY `sucursalID` (`sucursalID`),
  ADD KEY `usuarioID` (`usuarioID`);

--
-- Indices de la tabla `certificados`
--
ALTER TABLE `certificados`
  ADD PRIMARY KEY (`certificadoID`);

--
-- Indices de la tabla `ejercicios`
--
ALTER TABLE `ejercicios`
  ADD PRIMARY KEY (`ejercicioID`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`estadoID`);

--
-- Indices de la tabla `levantamientos`
--
ALTER TABLE `levantamientos`
  ADD PRIMARY KEY (`levantamientoID`),
  ADD KEY `ejercicioID` (`ejercicioID`),
  ADD KEY `sucursalID` (`sucursalID`),
  ADD KEY `normaID` (`normaID`);

--
-- Indices de la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD PRIMARY KEY (`municipioID`),
  ADD KEY `estadoID` (`estadoID`);

--
-- Indices de la tabla `nom002`
--
ALTER TABLE `nom002`
  ADD PRIMARY KEY (`nom002ID`,`ejercicioID`,`sucursalID`),
  ADD KEY `ejercicioID` (`ejercicioID`),
  ADD KEY `sucursalID` (`sucursalID`);

--
-- Indices de la tabla `nom002_distaceitehi`
--
ALTER TABLE `nom002_distaceitehi`
  ADD KEY `nom002ID` (`nom002ID`);

--
-- Indices de la tabla `nom002_distdiesel`
--
ALTER TABLE `nom002_distdiesel`
  ADD KEY `nom002ID` (`nom002ID`);

--
-- Indices de la tabla `nom002_distgasolina`
--
ALTER TABLE `nom002_distgasolina`
  ADD KEY `nom002ID` (`nom002ID`);

--
-- Indices de la tabla `nom002_distkg`
--
ALTER TABLE `nom002_distkg`
  ADD KEY `nom002ID` (`nom002ID`);

--
-- Indices de la tabla `nom002_tanquesgas`
--
ALTER TABLE `nom002_tanquesgas`
  ADD KEY `nom002ID` (`nom002ID`);

--
-- Indices de la tabla `nom006`
--
ALTER TABLE `nom006`
  ADD PRIMARY KEY (`nom006ID`,`ejercicioID`,`sucursalID`) USING BTREE,
  ADD KEY `ejercicioID` (`ejercicioID`) USING BTREE,
  ADD KEY `sucursalID` (`sucursalID`) USING BTREE;

--
-- Indices de la tabla `nom022`
--
ALTER TABLE `nom022`
  ADD PRIMARY KEY (`nom022ID`,`ejercicioID`,`sucursalID`),
  ADD KEY `ejercicioID_2` (`ejercicioID`),
  ADD KEY `sucursalID_2` (`sucursalID`);

--
-- Indices de la tabla `nom022_pararrayos`
--
ALTER TABLE `nom022_pararrayos`
  ADD KEY `nom022ID` (`nom022ID`);

--
-- Indices de la tabla `nom022_resistencias`
--
ALTER TABLE `nom022_resistencias`
  ADD KEY `nom022ID` (`nom022ID`);

--
-- Indices de la tabla `nom029`
--
ALTER TABLE `nom029`
  ADD PRIMARY KEY (`nom029ID`,`ejercicioID`,`sucursalID`),
  ADD KEY `ejercicioID_2` (`ejercicioID`),
  ADD KEY `sucursalID_2` (`sucursalID`);

--
-- Indices de la tabla `nom029_caracequipelectricos`
--
ALTER TABLE `nom029_caracequipelectricos`
  ADD KEY `nom029ID` (`nom029ID`) USING BTREE;

--
-- Indices de la tabla `nom029_equipomantenimiento`
--
ALTER TABLE `nom029_equipomantenimiento`
  ADD KEY `nom029ID` (`nom029ID`) USING BTREE;

--
-- Indices de la tabla `nom029_listadoequipos`
--
ALTER TABLE `nom029_listadoequipos`
  ADD KEY `nom029ID` (`nom029ID`) USING BTREE;

--
-- Indices de la tabla `nom029_seguridadequipos`
--
ALTER TABLE `nom029_seguridadequipos`
  ADD KEY `nom029ID` (`nom029ID`) USING BTREE;

--
-- Indices de la tabla `nom030`
--
ALTER TABLE `nom030`
  ADD PRIMARY KEY (`nom030ID`,`ejercicioID`,`sucursalID`),
  ADD KEY `ejercicioID_2` (`ejercicioID`),
  ADD KEY `sucursalID_2` (`sucursalID`);

--
-- Indices de la tabla `nom033`
--
ALTER TABLE `nom033`
  ADD PRIMARY KEY (`nom033ID`,`ejercicioID`,`sucursalID`),
  ADD KEY `ejercicioID_2` (`ejercicioID`),
  ADD KEY `sucursalID_2` (`sucursalID`);

--
-- Indices de la tabla `normas`
--
ALTER TABLE `normas`
  ADD PRIMARY KEY (`normaID`);

--
-- Indices de la tabla `razonessociales`
--
ALTER TABLE `razonessociales`
  ADD PRIMARY KEY (`razonID`),
  ADD UNIQUE KEY `razon` (`razon`);

--
-- Indices de la tabla `regiones`
--
ALTER TABLE `regiones`
  ADD PRIMARY KEY (`regionID`),
  ADD UNIQUE KEY `region` (`region`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`rolID`),
  ADD UNIQUE KEY `rol` (`rol`);

--
-- Indices de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  ADD PRIMARY KEY (`sucursalID`),
  ADD UNIQUE KEY `sucursal` (`sucursal`,`clave`),
  ADD KEY `zonaID` (`zonaID`),
  ADD KEY `tipoSucursalID` (`tipoSucursalID`);

--
-- Indices de la tabla `sucursalesgenerales`
--
ALTER TABLE `sucursalesgenerales`
  ADD PRIMARY KEY (`ejercicioID`,`sucursalID`),
  ADD KEY `sucursalID` (`sucursalID`);

--
-- Indices de la tabla `tipossucursal`
--
ALTER TABLE `tipossucursal`
  ADD PRIMARY KEY (`tipoSucursalID`),
  ADD UNIQUE KEY `tipoSucursal` (`tipoSucursal`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuarioID`),
  ADD UNIQUE KEY `usuario` (`usuario`),
  ADD KEY `rolID` (`rolID`),
  ADD KEY `usuarios_ibfk_2` (`sucursalID`);

--
-- Indices de la tabla `zonas`
--
ALTER TABLE `zonas`
  ADD PRIMARY KEY (`zonaID`),
  ADD UNIQUE KEY `zona` (`zona`),
  ADD KEY `regionID` (`regionID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `certificados`
--
ALTER TABLE `certificados`
  MODIFY `certificadoID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `estadoID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `levantamientos`
--
ALTER TABLE `levantamientos`
  MODIFY `levantamientoID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `municipios`
--
ALTER TABLE `municipios`
  MODIFY `municipioID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `nom002`
--
ALTER TABLE `nom002`
  MODIFY `nom002ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `nom006`
--
ALTER TABLE `nom006`
  MODIFY `nom006ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `nom022`
--
ALTER TABLE `nom022`
  MODIFY `nom022ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `nom029`
--
ALTER TABLE `nom029`
  MODIFY `nom029ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `nom030`
--
ALTER TABLE `nom030`
  MODIFY `nom030ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `nom033`
--
ALTER TABLE `nom033`
  MODIFY `nom033ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `normas`
--
ALTER TABLE `normas`
  MODIFY `normaID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `razonessociales`
--
ALTER TABLE `razonessociales`
  MODIFY `razonID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `regiones`
--
ALTER TABLE `regiones`
  MODIFY `regionID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `rolID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  MODIFY `sucursalID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=300;

--
-- AUTO_INCREMENT de la tabla `tipossucursal`
--
ALTER TABLE `tipossucursal`
  MODIFY `tipoSucursalID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usuarioID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT de la tabla `zonas`
--
ALTER TABLE `zonas`
  MODIFY `zonaID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `asignaciones`
--
ALTER TABLE `asignaciones`
  ADD CONSTRAINT `asignaciones_ibfk_1` FOREIGN KEY (`sucursalID`) REFERENCES `sucursales` (`sucursalID`),
  ADD CONSTRAINT `asignaciones_ibfk_2` FOREIGN KEY (`usuarioID`) REFERENCES `usuarios` (`usuarioID`),
  ADD CONSTRAINT `asignaciones_ibfk_3` FOREIGN KEY (`ejercicioID`) REFERENCES `ejercicios` (`ejercicioID`);

--
-- Filtros para la tabla `levantamientos`
--
ALTER TABLE `levantamientos`
  ADD CONSTRAINT `levantamientos_ibfk_1` FOREIGN KEY (`ejercicioID`) REFERENCES `ejercicios` (`ejercicioID`),
  ADD CONSTRAINT `levantamientos_ibfk_2` FOREIGN KEY (`sucursalID`) REFERENCES `sucursales` (`sucursalID`),
  ADD CONSTRAINT `levantamientos_ibfk_3` FOREIGN KEY (`normaID`) REFERENCES `normas` (`normaID`);

--
-- Filtros para la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD CONSTRAINT `municipios_ibfk_1` FOREIGN KEY (`estadoID`) REFERENCES `estados` (`estadoID`);

--
-- Filtros para la tabla `nom002`
--
ALTER TABLE `nom002`
  ADD CONSTRAINT `nom002_ibfk_1` FOREIGN KEY (`ejercicioID`) REFERENCES `ejercicios` (`ejercicioID`),
  ADD CONSTRAINT `nom002_ibfk_2` FOREIGN KEY (`sucursalID`) REFERENCES `sucursales` (`sucursalID`);

--
-- Filtros para la tabla `nom002_distaceitehi`
--
ALTER TABLE `nom002_distaceitehi`
  ADD CONSTRAINT `nom002_distaceitehi_ibfk_1` FOREIGN KEY (`nom002ID`) REFERENCES `nom002` (`nom002ID`);

--
-- Filtros para la tabla `nom002_distdiesel`
--
ALTER TABLE `nom002_distdiesel`
  ADD CONSTRAINT `nom002_distdiesel_ibfk_1` FOREIGN KEY (`nom002ID`) REFERENCES `nom002` (`nom002ID`);

--
-- Filtros para la tabla `nom002_distgasolina`
--
ALTER TABLE `nom002_distgasolina`
  ADD CONSTRAINT `nom002_distgasolina_ibfk_1` FOREIGN KEY (`nom002ID`) REFERENCES `nom002` (`nom002ID`);

--
-- Filtros para la tabla `nom002_distkg`
--
ALTER TABLE `nom002_distkg`
  ADD CONSTRAINT `nom002_distkg_ibfk_1` FOREIGN KEY (`nom002ID`) REFERENCES `nom002` (`nom002ID`);

--
-- Filtros para la tabla `nom002_tanquesgas`
--
ALTER TABLE `nom002_tanquesgas`
  ADD CONSTRAINT `nom002_tanquesgas_ibfk_1` FOREIGN KEY (`nom002ID`) REFERENCES `nom002` (`nom002ID`);

--
-- Filtros para la tabla `nom006`
--
ALTER TABLE `nom006`
  ADD CONSTRAINT `nom006_ibfk_1` FOREIGN KEY (`ejercicioID`) REFERENCES `ejercicios` (`ejercicioID`),
  ADD CONSTRAINT `nom006_ibfk_2` FOREIGN KEY (`sucursalID`) REFERENCES `sucursales` (`sucursalID`);

--
-- Filtros para la tabla `nom022`
--
ALTER TABLE `nom022`
  ADD CONSTRAINT `nom022_ibfk_1` FOREIGN KEY (`ejercicioID`) REFERENCES `ejercicios` (`ejercicioID`),
  ADD CONSTRAINT `nom022_ibfk_2` FOREIGN KEY (`sucursalID`) REFERENCES `sucursales` (`sucursalID`);

--
-- Filtros para la tabla `nom022_pararrayos`
--
ALTER TABLE `nom022_pararrayos`
  ADD CONSTRAINT `nom022_pararrayos_ibfk_1` FOREIGN KEY (`nom022ID`) REFERENCES `nom022` (`nom022ID`);

--
-- Filtros para la tabla `nom022_resistencias`
--
ALTER TABLE `nom022_resistencias`
  ADD CONSTRAINT `nom022_resistencias_ibfk_1` FOREIGN KEY (`nom022ID`) REFERENCES `nom022` (`nom022ID`);

--
-- Filtros para la tabla `nom029`
--
ALTER TABLE `nom029`
  ADD CONSTRAINT `nom029_ibfk_1` FOREIGN KEY (`ejercicioID`) REFERENCES `ejercicios` (`ejercicioID`),
  ADD CONSTRAINT `nom029_ibfk_2` FOREIGN KEY (`sucursalID`) REFERENCES `sucursales` (`sucursalID`);

--
-- Filtros para la tabla `nom029_caracequipelectricos`
--
ALTER TABLE `nom029_caracequipelectricos`
  ADD CONSTRAINT `nom029_caracequipelectricos_ibfk_1` FOREIGN KEY (`nom029ID`) REFERENCES `nom029` (`nom029ID`);

--
-- Filtros para la tabla `nom029_equipomantenimiento`
--
ALTER TABLE `nom029_equipomantenimiento`
  ADD CONSTRAINT `nom029_equipomantenimiento_ibfk_1` FOREIGN KEY (`nom029ID`) REFERENCES `nom029` (`nom029ID`);

--
-- Filtros para la tabla `nom029_listadoequipos`
--
ALTER TABLE `nom029_listadoequipos`
  ADD CONSTRAINT `nom029_listadoequipos_ibfk_1` FOREIGN KEY (`nom029ID`) REFERENCES `nom029` (`nom029ID`);

--
-- Filtros para la tabla `nom029_seguridadequipos`
--
ALTER TABLE `nom029_seguridadequipos`
  ADD CONSTRAINT `nom029_seguridadequipos_ibfk_1` FOREIGN KEY (`nom029ID`) REFERENCES `nom029` (`nom029ID`);

--
-- Filtros para la tabla `nom030`
--
ALTER TABLE `nom030`
  ADD CONSTRAINT `nom030_ibfk_1` FOREIGN KEY (`ejercicioID`) REFERENCES `ejercicios` (`ejercicioID`),
  ADD CONSTRAINT `nom030_ibfk_2` FOREIGN KEY (`sucursalID`) REFERENCES `sucursales` (`sucursalID`);

--
-- Filtros para la tabla `nom033`
--
ALTER TABLE `nom033`
  ADD CONSTRAINT `nom033_ibfk_1` FOREIGN KEY (`ejercicioID`) REFERENCES `ejercicios` (`ejercicioID`),
  ADD CONSTRAINT `nom033_ibfk_2` FOREIGN KEY (`sucursalID`) REFERENCES `sucursales` (`sucursalID`);

--
-- Filtros para la tabla `sucursales`
--
ALTER TABLE `sucursales`
  ADD CONSTRAINT `sucursales_ibfk_1` FOREIGN KEY (`zonaID`) REFERENCES `zonas` (`zonaID`),
  ADD CONSTRAINT `sucursales_ibfk_2` FOREIGN KEY (`tipoSucursalID`) REFERENCES `tipossucursal` (`tipoSucursalID`);

--
-- Filtros para la tabla `sucursalesgenerales`
--
ALTER TABLE `sucursalesgenerales`
  ADD CONSTRAINT `sucursalesgenerales_ibfk_1` FOREIGN KEY (`ejercicioID`) REFERENCES `ejercicios` (`ejercicioID`),
  ADD CONSTRAINT `sucursalesgenerales_ibfk_2` FOREIGN KEY (`sucursalID`) REFERENCES `sucursales` (`sucursalID`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`rolID`) REFERENCES `roles` (`rolID`),
  ADD CONSTRAINT `usuarios_ibfk_2` FOREIGN KEY (`sucursalID`) REFERENCES `sucursales` (`sucursalID`);

--
-- Filtros para la tabla `zonas`
--
ALTER TABLE `zonas`
  ADD CONSTRAINT `zonas_ibfk_1` FOREIGN KEY (`regionID`) REFERENCES `regiones` (`regionID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
