<?php
include('autentificacion.php');
setlocale(LC_ALL, "es_MX");
date_default_timezone_set("America/Mexico_City");

if (isset($_GET["sucursalID"])) {
    require_once('config.php');
    require_once('functionsDB.php');
    include_once('nom-helpers.php');

    $ejercicio = getEjercicioAbierto($link);
    $userID = $_SESSION["id"];

    $table = "nom030"; // nombre de la tabla principal de la norma
    $tableID = $table."ID"; // nombre de la columna ID de la norma, debrá seguir el estandar <nombreTabla><ID>
    $sucursalID = $_GET["sucursalID"];

    $sql = mysqli_query($link, "SELECT S.sucursal,Z.zona,R.region, A.* FROM asignaciones AS A INNER JOIN sucursales AS S ON A.sucursalID = S.sucursalID INNER JOIN zonas AS Z ON S.zonaID = Z.zonaID INNER JOIN regiones AS R ON Z.regionID = R.regionID WHERE ejercicioID = $ejercicio AND usuarioID = $userID AND A.sucursalID = $sucursalID");
    if (mysqli_num_rows($sql) == 0) {
        header("location: index.php");
    } else {
        $row = mysqli_fetch_assoc($sql);

        $sucursal = $row["sucursal"];
        $zona = $row["zona"];
        $region = $row["region"];
        $fecha = $row["fecha"];
        $estatus = $row["estatus"];

        $exist = mysqli_query($link, "SELECT $tableID, sucursalID FROM $table WHERE ejercicioID = '$ejercicio' AND sucursalID = $sucursalID");

        if (mysqli_num_rows($exist) == 0) {
            // FALTA OBTENER DATOS DEL AÑO PASADO EN TABLA PRINCIPAL Y SECUNDARIAS
            // SI ES EL PRIMER AÑO SE INSERTA UN REGISTRO SOLO CON LOS CAMPOS PRINCIPALES
            $result = mysqli_query($link, "INSERT INTO $table (ejercicioID, sucursalID) VALUES ('$ejercicio',$sucursalID)");
            $nomID = mysqli_insert_id($link);
        }else{
            $row = mysqli_fetch_assoc($exist);
            $nomID = $row[$tableID];
        }
        $where = "WHERE $tableID = $nomID";
    }
}else{
    header("location: index.php");
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('head.php'); ?>
</head>

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <?php include('sidebar.php'); ?>
        <!-- Page Content  -->
        <div id="content">
            <?php include('navbar.php'); ?>
            <div class="content">
                <h3><span class="fa fa-gavel" aria-hidden="true"></span> NOM-017-STPS-2008</h3>
                <h4>Equipo de protección personal</h4>
                <?php require('nom-cards.php'); ?>
                <div class="line"></div>
                <div class="accordion" id="accordionExample">
                    <?php
                    // Procesamos todas las secciones
                    $d1 = proccessFormSection("1");

                    // Obtenemos banderas de captura de secciones
                    $c1 = sectionCompleted("1");

                    // obtenemos datos almacenados en BD
                    $data = getDataFromDB();
                    //print("<pre>".print_r($data,true)."</pre>");
                    
                    initSection("accordionExample", "1", "Actividades de Salud y Seguridad en el trabajo", $d1, $c1);
   
                    endSection($sucursalID, "1");
                    ?>
                </div>
            </div>
        </div>
    </div>

    <script src="vendor/bootstrap/jquery-3.4.1.min.js"></script>
    <script src="vendor/bootstrap/popper.min.js"></script>
    <script src="vendor/bootstrap/bootstrap.min.js"></script>

    <script src="js/autentificacionAjax.js"></script>
    <script src="js/sidebarCollapse.js"></script>
    <?php require('nom-helpers-script.php') ?>
</body>

</html>