<?php include('autentificacion.php');
setlocale(LC_ALL, "es_MX");
date_default_timezone_set("America/Mexico_City");

require_once('config.php');
require_once('functionsDB.php');
if (isset($_POST['asignacionID'])) {
    $asignacionID = $_POST["asignacionID"];
    $sql = mysqli_query(
        $link,
        "SELECT c.*, 1 AS asignado FROM asignacionescertificados
        INNER JOIN certificados AS c USING(certificadoID)
        WHERE asignacionID = {$asignacionID}
        UNION
        SELECT c.*, 0 AS asignado FROM asignacionescertificados
        RIGHT JOIN certificados AS c USING(certificadoID)
        WHERE asignacionID IS NULL OR asignacionID <> {$asignacionID}"
    );
    if (mysqli_num_rows($sql) !== 0) {
        $myArray = array();
        while ($row = mysqli_fetch_assoc($sql)) {
            $myArray[] = $row;
        }
        echo json_encode($myArray);
    }
}
?>